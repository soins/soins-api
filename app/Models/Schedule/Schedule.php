<?php


namespace App\Models\Schedule;

use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Customer;
use App\Models\Masters\Types;
use App\Models\Masters\User;
use App\Models\Tickets\Ticket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Schedule extends Model
{

    protected $table = "trschedule";
    protected $primaryKey = "scheduleid";

    protected $fillable = [
        'bpid',
        'subjectid',
        'scheduletitle',
        'scheduledatefrom',
        'scheduledateto',
        'actualdatefrom',
        'actualdateto',
        'statusid',
        'towardtypeid',
        'assigntoid',
        'customerid',
        'refid',
        'createdby',
        'updatedby',
        'isactive',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = [
        'scheduletitle',
        'scheduledatefrom',
        'scheduledateto',
    ];

    /**
     * @param Relation $query
     * @return Relation|Schedule
     */
    static public function foreignSelect($query)
    {
        $schedule = new Schedule();
        return $schedule->_withJoin($query, $schedule->defaultSelects);
    }

    /**
     * @param Relation|Schedule $query
     * @param array $selects
     * @return Relation|Schedule
     */
    public function _withJoin($query, $selects = array())
    {
        return $query->with([
            'subject' => function($query) {
                Types::foreignSelect($query);
            },
            'status' => function($query) {
                Types::foreignSelect($query);
            },
            'towardtype' => function($query) {
                Types::foreignSelect($query);
            },
            'assignto' => function($query) {
                User::foreignSelect($query);
            },
            'customer' => function($query) {
                Customer::foreignSelect($query);
            },
        ])->select('scheduleid', 'subjectid', 'statusid', 'towardtypeid','assigntoid', 'customerid', 'refid')->addSelect($selects);
    }

    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function subject()
    {
        return $this->hasOne(Types::class, 'typeid', 'subjectid');
    }

    public function status()
    {
        return $this->hasOne(Types::class, 'typeid', 'statusid');
    }

    public function towardtype()
    {
        return $this->hasOne(Types::class, 'typeid', 'towardtypeid');
    }

    public function toward()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'towardid');
    }

    public function assignto()
    {
        return $this->hasOne(User::class, 'userid', 'assigntoid');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'customerid', 'customerid');
    }

    public function ticket()
    {
        return $this->hasOne(Ticket::class, 'ticketid', 'refid');
    }

    public function datacreated()
    {
        return $this->hasOne(User::class, 'userid', 'createdby');
    }

    public function dataupdated()
    {
        return $this->hasOne(User::class, 'userid', 'updatedby');
    }
}

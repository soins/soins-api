<?php


namespace App\Models\Tickets;


use App\Models\Masters\Assets;
use App\Models\Masters\Files;
use App\Models\Masters\Types;
use App\Models\Masters\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class TicketProgress extends Model
{

    protected $table = "trticketprogress";
    protected $primaryKey = "progressid";

    protected $fillable = [
        "ticketid",
        "ticketdtid",
        "description",
        "progresstypeid",
        "assetid",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        'description'
    );

    public $historySelects = array(
        'createdby',
        'createddate',
        'updatedby',
        'updateddate',
        'isactive',
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $user = new TicketProgress();
        return $user->withJoin(is_null($selects) ? $user->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param int $bpid
     * @return Relation
     * */
    static public function history($query, $bpid)
    {
        $ticketProgress = new TicketProgress();
        return $ticketProgress->withJoin($ticketProgress->historySelects, $query);
    }

    /**
     * @param Relation|TicketProgress $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'progresstype' => function($query) {
                Types::foreignSelect($query);
            },
            'asset' => function($query) {
                Assets::foreignSelect($query);
            }
        ])->select('progressid', 'progresstypeid', 'assetid', 'ticketid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|TicketProgress
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function datacreated()
    {
        return $this->hasOne(User::class, 'userid', 'createdby');
    }

    public function dataupdated()
    {
        return $this->hasOne(User::class, 'userid', 'updatedby');
    }

    public function progresstype()
    {
        return $this->hasOne(Types::class, 'typeid', 'progresstypeid');
    }

    public function asset()
    {
        return $this->hasOne(Assets::class, 'assetid', 'assetid');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticketid', 'ticketid');
    }

    public function files()
    {
        return $this->hasMany(Files::class, 'refid', 'progressid');
    }
}

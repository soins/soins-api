<?php


namespace App\Models\Tickets;


use App\Models\Masters\Asset;
use App\Models\Masters\Types;
use App\Models\Masters\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class TicketAsset extends Model
{

    protected $table = "trticketasset";
    protected $primaryKey = "ticketassetid";

    protected $fillable = [
        "ticketid",
        "assetid",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        'ticketid',
        'assetid',
        "createdby",
        "createddate"
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $user = new TicketAsset();
        return $user->withJoin(is_null($selects) ? $user->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|TicketAsset $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'asset' => function($query) {
                Asset::foreignSelect($query);
            }
        ])->select('ticketid', 'assetid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|TicketAsset
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function datacreated()
    {
        return $this->hasOne(User::class, 'userid', 'createdby');
    }

    public function dataupdated()
    {
        return $this->hasOne(User::class, 'userid', 'updatedby');
    }

    public function asset()
    {
        return $this->hasOne(Asset::class, 'assetid', 'assetid');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticketid', 'ticketid');
    }
}

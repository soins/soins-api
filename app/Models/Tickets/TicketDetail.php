<?php


namespace App\Models\Tickets;


use App\Models\Masters\Files;
use App\Models\Masters\Types;
use App\Models\Masters\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class TicketDetail extends Model
{

    protected $table = "trticketdt";
    protected $primaryKey = "ticketdtid";

    protected $fillable = [
        "ticketid",
        "statusid",
        "description",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "description",
    );

    public $historySelects = array(
        "createdby",
        "createddate",
        "updatedby",
        "updateddate",
        "isactive",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $ticketdt = new TicketDetail();
        return $ticketdt->withJoin(is_null($selects) ? $ticketdt->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|TicketDetail $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'status' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('ticketdtid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|TicketDetail
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function datacreated()
    {
        return $this->hasOne(User::class, 'userid', 'createdby');
    }

    public function dataupdated()
    {
        return $this->hasOne(User::class, 'userid', 'updatedby');
    }

    public function status()
    {
        return $this->hasOne(Types::class, 'typeid', 'statusid');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticketid', 'ticketid');
    }

    public function files()
    {
        return $this->hasMany(Files::class, 'refid', 'ticketdtid');
    }
}

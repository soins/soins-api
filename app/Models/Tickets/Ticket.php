<?php


namespace App\Models\Tickets;


use App\Constants\DBTypes;
use App\Helpers\Helpers;
use App\Models\Masters\Branch;
use App\Models\Masters\Customer;
use App\Models\Masters\Departement;
use App\Models\Masters\Files;
use App\Models\Masters\Types;
use App\Models\Masters\User;
use App\Models\Schedule\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Ticket extends Model
{

    protected $table = "trticket";
    protected $primaryKey = "ticketid";

    protected $fillable = [
        "ticketid",
        "priorityid",
        "duedate",
        "customerid",
        "ticketcd",
        "tickettypeid",
        "tickettitle",
        "description",
        "statusid",
        "bpid",
        "deptfromid",
        "depttoid",
        "branchfromid",
        "branchtoid",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "ticketcd",
        "customerid",
        "tickettitle",
        "description",
    );

    public $history = array(
        "createdby",
        "createddate",
        "updatedby",
        "updateddate",
        "isactive",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $ticket = new Ticket();
        return $ticket->withJoin(is_null($selects) ? $ticket->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Ticket $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'customer' => function($query) {
                Customer::foreignSelect($query);
            },
            'branchfrom' => function($query) {
                Branch::foreignSelect($query);
            },
            'branchto' => function($query) {
                Branch::foreignSelect($query);
            },
            'departementfrom' => function($query) {
                Departement::foreignSelect($query);
            },
            'departementto' => function($query) {
                Departement::foreignSelect($query);
            },
            'tickettype' => function($query) {
                Types::foreignSelect($query);
            },
            'status' => function($query) {
                Types::foreignSelect($query);
            },
            'priority' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('ticketid', 'branchfromid', 'branchtoid', 'deptfromid', 'depttoid', 'customerid', 'tickettypeid', 'statusid','priorityid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Ticket
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function datacreated()
    {
        return $this->hasOne(User::class, 'userid', 'createdby');
    }

    public function dataupdated()
    {
        return $this->hasOne(User::class, 'userid', 'updatedby');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'customerid', 'customerid');
    }

    public function priority()
    {
        return $this->hasOne(Types::class, 'typeid', 'priorityid');
    }

    public function status()
    {
        return $this->hasOne(Types::class, 'typeid', 'statusid')
            ->orderBy('typeseq');
    }

    public function departementfrom()
    {
        return $this->hasOne(Departement::class, 'deptid', 'deptfromid');
    }

    public function departementto()
    {
        return $this->hasOne(Departement::class, 'deptid', 'depttoid');
    }

    public function branchfrom()
    {
        return $this->hasOne(Branch::class, 'branchid', 'branchfromid');
    }

    public function branchto()
    {
        return $this->hasOne(Branch::class, 'branchid', 'branchtoid');
    }

    public function tickettype()
    {
        return $this->hasOne(Types::class, 'typeid', 'tickettypeid');
    }

    public function details()
    {
        return $this->hasMany(TicketDetail::class, 'ticketid', 'ticketid');
    }

    public function ticketasset()
    {
        return $this->hasMany(TicketAsset::class, 'ticketid', 'ticketid');
    }

    public function progresses()
    {
        return $this->hasMany(TicketProgress::class, 'ticketid', 'ticketid');
    }

    public function files()
    {
        return $this->hasMany(Files::class, 'refid', 'ticketid');
    }

    public function getCode()
    {
        $string = Helpers::randomStr(10, true);
        $check = $this->with([
        ])->select('ticketid', 'ticketcd')
        ->where('ticketcd', $string);

        if($check->count() > 0)
            return $this->getCode();

        return $string;
    }

    public function schedule()
    {
        return $this->hasOne(Schedule::class, 'refid', 'ticketid')
            ->whereHas('subject', function($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('typecd', DBTypes::typescheduleservice);
            });
    }
}

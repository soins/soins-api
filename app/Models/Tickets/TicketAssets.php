<?php


namespace App\Models\Tickets;


use App\Models\Masters\Assets;
use App\Models\Masters\Types;
use App\Models\Masters\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class TicketAssets extends Model
{
    protected $table = "trticketasset";
    protected $primaryKey = "ticketassetid";

    protected $fillable = [
        "ticketid",
        "assetid",
        "statusid",
        "createdby",
        "updatedby",
        "isactive"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $ticketasset = new TicketAssets();
        return $ticketasset->withJoin(is_null($selects) ? $ticketasset->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|TicketAssets $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'asset' => function($query) {
                Assets::foreignSelect($query);
            },
            'status' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('ticketassetid', 'statusid', 'assetid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|TicketAssets
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function datacreated()
    {
        return $this->hasOne(User::class, 'userid', 'createdby');
    }

    public function dataupdated()
    {
        return $this->hasOne(User::class, 'userid', 'updatedby');
    }

    public function status()
    {
        return $this->hasOne(Types::class, 'typeid', 'statusid');
    }

    public function asset()
    {
        return $this->hasOne(Assets::class, 'assetid', 'assetid');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticketid', 'ticketid');
    }

}

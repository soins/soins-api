<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Location extends Model
{

    protected $table = "mslocation";
    protected $primaryKey = "locationid";

    protected $fillable = [
        "locationname",
        "bpid",
        "branchid",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "locationname",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $location = new Location();
        return $location->withJoin(is_null($selects) ? $location->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Location $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('locationid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Location
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'branchid', 'branchid');
    }
}

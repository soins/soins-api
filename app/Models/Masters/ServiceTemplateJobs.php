<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class ServiceTemplateJobs extends Model
{

    protected $table = "mstemplateservicedt";
    protected $primaryKey = "templatesrvcdtid";

    protected $fillable = [
        'templateserviceid',
        'jobid',
    ];

    public $defaultSelects = [];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new ServiceTemplateJobs();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|ServiceTemplateJobs $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'jobs' => function($query) {
                Jobs::foreignSelect($query);
            }
        ])->select($this->getKeyName(), 'jobid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|ServiceTemplateJobs
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function jobs()
    {
        return $this->hasOne(Jobs::class, 'jobid', 'jobid');
    }

    public function spare_parts()
    {
        return $this->hasMany(JobsSparePart::class, 'templatesrvcdtid', 'templatesrvcdtid');
    }
}

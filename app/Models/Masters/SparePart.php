<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class SparePart extends Model
{

    protected $table = "mssparepart";
    protected $primaryKey = "sparepartid";

    protected $fillable = [
        "brandid",
        "bpid",
        "branchid",
        "deptid",
        "sparepartcategoryid",
        "sparepartsubcategoryid",
        "spareparttypeid",
        "sparepartname",
        "description",
        "createdby",
        "updatedby",
        "isactive",
    ];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    public $defaultSelects = [
        "sparepartname",
        "description"
    ];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new SparePart();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|SparePart $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('sparepartid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|SparePart
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function spareparttype()
    {
        return $this->hasOne(Types::class, 'typeid', 'spareparttypeid');
    }

    public function sparepartcategory()
    {
        return $this->hasOne(Types::class, 'typeid', 'sparepartcategoryid');
    }

    public function sparepartsubcategory()
    {
        return $this->hasOne(Types::class, 'typeid', 'sparepartsubcategoryid');
    }

    public function brand()
    {
        return $this->hasOne(Types::class, 'typeid', 'brandid');
    }

    public function department()
    {
        return $this->hasOne(Departement::class, 'deptid', 'deptid');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'branchid', 'branchid');
    }

    public function queryData()
    {
        return $this->withJoin($this->defaultSelects)
            ->with([
                'brand' => function($query) {
                    Types::foreignSelect($query);
                },
                'sparepartcategory' => function($query) {
                    Types::foreignSelect($query);
                },
                'sparepartsubcategory' => function($query) {
                    Types::foreignSelect($query);
                },
                'spareparttype' => function($query) {
                    Types::foreignSelect($query);
                },
                'branch' => function($query) {
                    Branch::foreignSelect($query);
                },
                'department' => function($query) {
                    Departement::foreignSelect($query);
                }
            ])
            ->addSelect('brandid', 'sparepartcategoryid', 'sparepartsubcategoryid', 'spareparttypeid', 'branchid', 'deptid');
    }
}

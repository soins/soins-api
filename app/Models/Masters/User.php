<?php


namespace App\Models\Masters;

use App\Constants\DBTypes;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{

    use Authorizable, Authenticatable;

    protected $table = "msuser";
    protected $primaryKey = "userid";

    protected $fillable = [
        'username',
        'userpassword',
        'userfullname',
        'useremail',
        'userphone',
        'userdevice',
        'createdby',
        'updatedby',
        'isactive',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "username",
        "userpassword",
        "userfullname",
        "useremail",
        "userphone",
        "userdeviceid",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $user = new User();
        return $user->withJoin(is_null($selects) ? $user->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param int $bpid
     * @return Relation
     * */
    static public function history($query, $bpid)
    {
        $user = new User();
        return $user->withJoin(array('userid', 'userfullname'), $query)
            ->with([
                'userdetail' => function($query) use ($bpid) {
                    UserDetail::history($query, $bpid)
                        ->addSelect('userid')
                        ->where('bpid', $bpid);
                }
            ]);
    }

    /**
     * @param Relation|User $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('userid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|User
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function profile() {
        return $this->hasOne(Files::class, 'refid', 'userid')
            ->whereHas('transtype', function($query) {
                /* @var Relation $query */
                $query->where('typecd', DBTypes::filesProfile);
            });
    }

    public function userdetail()
    {
        return $this->hasOne(UserDetail::class, 'userid', 'userid');
    }

    public function userdetails()
    {
        return $this->hasMany(UserDetail::class, 'userid', 'userid');
    }

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword()
    {
        return $this->userpassword;
    }

    public function file() {
        return $this->hasOne(Files::class, 'refid', 'userid')
            ->whereHas('transtype', function($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereIn("typecd", array(
                    DBTypes::filesProfile,
                ));
            });
    }
}

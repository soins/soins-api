<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class JobsSparePart extends Model
{

    protected $table = "msjobssp";
    protected $primaryKey = "jobsspid";

    protected $fillable = [
        'templatesrvcdtid',
        'sptypeid',
        'sparepartid',
        'createdby',
        'updatedby',
        'isactive',
    ];

    public $defaultSelects = [];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new JobsSparePart();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|JobsSparePart $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'spareparttype' => function($query) {
                Types::foreignSelect($query);
            },
            'sparepart' => function($query) {
                SparePart::foreignSelect($query);
            }
        ])->select($this->getKeyName(), 'sptypeid', 'sparepartid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|JobsSparePart
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function spareparttype()
    {
        return $this->hasOne(Types::class, 'typeid', 'sptypeid');
    }

    public function sparepart()
    {
        return $this->hasOne(SparePart::class, 'sparepartid', 'sparepartid');
    }
}

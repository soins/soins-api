<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class RecurrentTemplate extends Model
{

    protected $table = "strecurrenttemplate";
    protected $primaryKey = 'recurrenttemplateid';

    protected $fillable = [
        'assettypeid',
        'bpid',
        'schedulepattern',
        'createdby',
        'updatedby',
        'isactive'
    ];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    public $defaultSelects = [
        'schedulepattern'
    ];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new RecurrentTemplate();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|RecurrentTemplate $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'assettype' => function($query) {
                Types::foreignSelect($query);
            },
        ])->select('recurrenttemplateid', 'assettypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|RecurrentTemplate
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function assettype()
    {
        return $this->hasOne(Types::class, 'typeid', 'assettypeid');
    }
}

<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Files extends Model
{

    const IMAGE_SIZE_DATATABLES = 'datatables';
    const IMAGE_SIZE_THUMBNAIL_POTRAIT = 'thumnail-potrait';
    const IMAGE_SIZE_LARGE = 'large';
    const IMAGE_SIZE_MEDIUM = 'medium';
    const IMAGE_SIZE_FULL = 'fullsize';
    const IMAGE_SIZE_MEDIUM_THUMNAIL = 'medium-thumbnail';

    const IMAGE_ALIAS_URL = 'imageURL';
    const IMAGE_ALIAS_THUMBNAIL = 'imageThumbnail';
    const IMAGE_ALIAS_LARGE = 'imageLarge';

    protected $table = "msfiles";
    protected $primaryKey = "fileid";

    protected $fillable = [
        'transtypeid',
        'refid',
        'directories',
        'filename',
        'mimetype',
        'filesize',
        'createdby',
        'updatedby',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        'refid',
        'directories',
        'filename',
        'mimetype',
        'filesize',
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $file = new Files;
        return $file->withJoin(is_null($selects) ? $file->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function file($query, $selects = array())
    {
        return $query->select('directories', 'filename', 'refid')
            ->addSelect($selects);
    }

    /**
     * @param Relation|Files $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'transtype' => function($query) {
                /* @var Relation $query */
                $query->select('typeid', 'typecd', 'typename', 'typeseq');
            }
        ])->select('fileid', 'transtypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Files
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function transtype()
    {
        return $this->hasOne(Types::class, 'typeid', 'transtypeid');
    }

}

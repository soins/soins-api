<?php


namespace App\Models\Masters;


use App\Models\Masters\Types;
use App\Models\Masters\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Notification extends Model
{

    protected $table = "trnotification";
    protected $primaryKey = "notificationid";

    protected $fillable = [
        'refid',
        'actualid',
        'notificationtypeid',
        'notificationtoid',
        'notificationtitle',
        'description',
        'createdby',
        'updatedby',
        'isactive',
        'isviewed',
        'ishandled',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    /**
     * @param Relation|Notification $query
     * @param array $select
     * @return Relation
     */
    public function withQueryJoin($query, $select = array())
    {
        return $query->with([
            'notificationtype' => function($query) {
                Types::foreignSelect($query);
            },
            'notificationto' => function($query) {
                User::foreignSelect($query);
            }
        ])->select('notificationid', 'notificationtypeid', 'notificationtoid')->addSelect($select);
    }

    public function withJoin($select = array())
    {
        return $this->withQueryJoin($this, is_array($select) ? $select : func_get_args());
    }

    public function notificationtype()
    {
        return $this->hasOne(Types::class, 'typeid', 'notificationtypeid');
    }

    public function notificationto()
    {
        return $this->hasOne(User::class, 'userid', 'notificationtoid');
    }

    public function notifications($userid)
    {
        return $this->withJoin('notificationtitle', 'description', 'isviewed')
            ->where('notificationtoid', $userid)
            ->orderBy('createddate', 'desc');
    }
}

<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class SparePartType extends Model
{

    protected $table = "msspareparttype";
    protected $primaryKey = "spareparttypeid";

    protected $fillable = [
        "brandid",
        "bpid",
        "deptid",
        "typename",
        "description",
        "createdby",
        "updatedby",
    ];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    public $defaultSelects = [
        'typename',
        'description'
    ];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($row) {
            /* @var SparePartType $row */
            $row->vendors()->delete();
        });
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new SparePartType();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|SparePartType $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('spareparttypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|SparePartType
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function vendors()
    {
        return $this->hasMany(SparePartVendor::class, 'spareparttypeid', 'spareparttypeid');
    }

    public function brand()
    {
        return $this->hasOne(Types::class, 'typeid', 'brandid');
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }

    public function department()
    {
        return $this->hasOne(Departement::class, 'deptid', 'deptid');
    }

    public function defaultQuery()
    {
        return $this->withJoin($this->defaultSelects)
            ->with([
                'brand' => function($query) {
                    Types::foreignSelect($query);
                },
                'department' => function($query) {
                    Departement::foreignSelect($query)
                        ->with([
                            'branch' => function($query) {
                                Branch::foreignSelect($query);
                            }
                        ])
                        ->addSelect('branchid');
                },
                'vendors' => function($query) {
                    SparePartVendor::foreignSelect($query)
                        ->addSelect('spareparttypeid');
                }
            ])
            ->addSelect('brandid', 'deptid');
    }
}

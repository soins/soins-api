<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class UserDetail extends  Model
{

    protected $table = "msuserdt";
    protected $primaryKey = "userdtid";

    protected $fillable = [
        "userid",
        "usertypeid",
        "bpid",
        "branchid",
        "deptid",
        "referalcode",
        "relationid",
        "createdby",
        "updatedby",
        "isactive"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array();

    public $history = array(
        'userid',
        'createdby',
        'createddate',
        'updatedby',
        'updateddate',
        'isactive',
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $userdt = new UserDetail();
        return $userdt->withJoin(is_null($selects) ? $userdt->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param int $bpid
     * @return Relation
     * */
    static public function history($query, $bpid)
    {
        $userdt = new UserDetail();
        return $userdt->withJoin(array('userid', 'relationid', 'bpid'), $query)
            ->with([
                'customer' => function($query) {
                    Customer::foreignSelect($query, array('customerprefix', 'customername'));
                },
                'businesspartner' => function($query) {
                    BusinessPartner::foreignSelect($query);
                }
            ])
            ->where('bpid', $bpid);
    }


    /**
     * @param Relation|UserDetail $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'usertype' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('userdtid', 'usertypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|UserDetail
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }

    public function usertype()
    {
        return $this->hasOne(Types::class, 'typeid', 'usertypeid');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'customerid', 'relationid');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'branchid', 'branchid');
    }

    public function departement()
    {
        return $this->hasOne(Departement::class, 'deptid', 'deptid');
    }
}

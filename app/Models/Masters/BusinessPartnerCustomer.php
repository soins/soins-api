<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class BusinessPartnerCustomer extends Model
{

    protected $table = "stbpcustomer";
    protected $primaryKey = "bpcustomerid";

    protected $fillable = [
        'bpid',
        'customerid',
        'customername',
        'customerphone',
        'customeraddress',
        'customerpic',
        'createdby',
        'updatedby',
        'isactive',
    ];


    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "customername",
        "customerpic",
        "customerphone",
        "customeraddress",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $bpcustomer = new BusinessPartnerCustomer();
        return $bpcustomer->withJoin(is_null($selects) ? $bpcustomer->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|BusinessPartnerCustomer $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('bpcustomerid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|BusinessPartnerCustomer
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'customerid', 'customerid');
    }
}

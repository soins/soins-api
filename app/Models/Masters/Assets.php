<?php


namespace App\Models\Masters;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Assets extends Model
{

    protected $table = "msassets";
    protected $primaryKey = "assetid";

    protected $fillable = [
        "assetno",
        "bpid",
        "assettypeid",
        "assetcategoryid",
        "assetsubcategoryid",
        "branchid",
        "merkid",
        "assettypeid",
        "purchasedate",
        "purchaseprice",
        "warrantyexpdate",
        "purchasetypeid",
        "depreciationtypeid",
        "branchid",
        "deptid",
        "locationid",
        "assettypeid",
        "assetname",
        "usedbranchid",
        "usedbyid",
        "useddeptid",
        "vendorid",
        "schedulepattern",
        "recurrentpattern",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "assetno",
        "assetname",
        "purchasedate",
        "purchaseprice",
        "warrantyexpdate",
        "schedulepattern",
        "recurrentpattern",
        "deptid",
        "branchid",
        "useddeptid",
        "usedbranchid",
    );

    public $historySelects = array(
        'createdby',
        'createddate',
        'updatedby',
        'updateddate',
    );

    public function getCreateddateAttribute($value)
    {
        return Carbon::parse($value, 'Asia/Jakarta');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->timezone('Asia/Jakarta')
            ->format('Y-m-d H:i:s');
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $assets = new Assets();
        return $assets->withJoin(is_null($selects) ? $assets->defaultSelects : $selects, $query);
    }

    static public function relationHistory() {
        return [
            'datacreated' => function($query) {
                /* @var Relation $query */
                $query->select('userid', 'userfullname');
            },
            'dataupdated' => function($query) {
                /* @var Relation $query */
                $query->select('userid', 'userfullname');
            }
        ];
    }

    /**
     * @param Relation|Assets $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('assetid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Assets
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function datacreated()
    {
        return $this->hasOne(User::class, 'userid', 'createdby');
    }

    public function dataupdated()
    {
        return $this->hasOne(User::class, 'userid', 'updatedby');
    }

    public function usedby()
    {
        return $this->hasOne(User::class, 'userid', 'usedbyid');
    }

    public function usedbranch()
    {
        return $this->hasOne(Branch::class, 'branchid', 'usedbranchid');
    }

    public function useddept()
    {
        return $this->hasOne(Departement::class, 'deptid', 'useddeptid');
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'branchid', 'branchid');
    }

    public function departement()
    {
        return $this->hasOne(Departement::class, 'deptid', 'deptid');
    }

    public function location()
    {
        return $this->hasOne(Location::class, 'locationid', 'locationid');
    }

    public function assetcategory()
    {
        return $this->hasOne(Types::class, 'typeid', 'assetcategoryid');
    }

    public function assetsubcategory()
    {
        return $this->hasOne(Types::class, 'typeid', 'assetsubcategoryid');
    }

    public function merk()
    {
        return $this->hasOne(Types::class, 'typeid', 'merkid');
    }

    public function assettype()
    {
        return $this->hasOne(Types::class, 'typeid', 'assettypeid');
    }

    public function purchasetype()
    {
        return $this->hasOne(Types::class, 'typeid', 'purchasetypeid');
    }

    public function depreciationtype()
    {
        return $this->hasOne(Types::class, 'typeid', 'depreciationtypeid');
    }

    public function vendor()
    {
        return $this->hasOne(User::class, 'userid', 'vendorid');
    }

    public function template_services()
    {
        return $this->hasMany(ServiceTemplate::class, 'assettypeid', 'assettypeid');
    }

    public function all_information()
    {
        return $this->hasMany(AssetsDetail::class, 'assetid', 'assetid');
    }

    public function services()
    {
        return $this->hasMany(AssetsService::class, 'assetid', 'assetid');
    }

    public function queryData()
    {
        return $this->withJoin($this->defaultSelects)
            ->with([
                'branch' => function($query) {
                    Branch::foreignSelect($query);
                },
                'departement' => function($query) {
                    Departement::foreignSelect($query);
                },
                'location' => function($query) {
                    Location::foreignSelect($query);
                },
                'assetcategory' => function($query) {
                    Types::foreignSelect($query);
                },
                'assetsubcategory' => function($query) {
                    Types::foreignSelect($query);
                },
                'merk' => function($query) {
                    Types::foreignSelect($query);
                },
                'assettype' => function($query) {
                    Types::foreignSelect($query);
                },
                'usedby' => function($query) {
                    User::foreignSelect($query);
                },
                'usedbranch' => function($query) {
                    Branch::foreignSelect($query);
                },
                'useddept' => function($query) {
                    Departement::foreignSelect($query);
                },
                'purchasetype' => function($query) {
                    Types::foreignSelect($query);
                },
                'depreciationtype' => function($query) {
                    Types::foreignSelect($query);
                },
                'vendor' => function($query) {
                    User::foreignSelect($query);
                },
                'all_information' => function($query) {
                    AssetsDetail::foreignSelect($query)
                        ->addSelect('assetid');
                }
            ])
            ->addSelect('branchid', 'deptid', 'locationid', 'assettypeid', 'usedbyid', 'useddeptid', 'usedbranchid', 'assetcategoryid', 'assetsubcategoryid', 'merkid', 'purchasetypeid', 'depreciationtypeid', 'vendorid');
    }
}

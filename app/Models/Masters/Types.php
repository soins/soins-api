<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Types extends Model
{

    protected $table = "mstype";
    protected $primaryKey = "typeid";

    protected $fillable = [
        "typecd",
        "typename",
        "typeseq",
        "masterid",
        "descriptions",
        "createdby",
        "updatedby"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "typecd",
        "typename",
        "typeseq",
        "descriptions",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $type = new Types();
        return $type->withJoin(is_null($selects) ? $type->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Types $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('typeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Types
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function parent()
    {
        return $this->hasOne(Types::class, 'typeid', 'masterid');
    }

    public function vendors()
    {
        return $this->hasMany(Vendor::class, 'vendorid', 'vendorid');
    }
}

<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Departement extends Model
{

    protected $table = "msdepartement";
    protected $primaryKey = "deptid";

    protected $fillable = [
        "deptname",
        "bpid",
        "branchid",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "deptname",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $departement = new Departement();
        return $departement->withJoin(is_null($selects) ? $departement->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Departement $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('deptid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Departement
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'branchid', 'branchid');
    }
}

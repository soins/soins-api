<?php


namespace App\Models\Masters;


use App\Helpers\Helpers;
use App\Models\Addresses\Village;
use Illuminate\Database\Eloquent\Collection;
use App\Constants\DBTypes;
use App\Models\Masters\Types;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Customer extends Model
{

    protected $table = "mscustomer";
    protected $primaryKey = "customerid";

    protected $fillable = [
        'customerprefix',
        'customername',
        'customerphone',
        'customeraddress',
        'customertypeid',
        'customerproviceid',
        'customercityid',
        'customersubdistrictid',
        'customeruvid',
        'customerpostalcode',
        'customerlatitude',
        'customerlongitude',
        'referalcode',
        'createdby',
        'updatedby',
        'isactive',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        'customerprefix',
        'customername',
        'customerphone',
        'customeraddress',
        'customerlatitude',
        'customerlongitude',
        'customerpostalcode',
        'referalcode'
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $customer = new Customer();
        return $customer->withJoin(is_null($selects) ? $customer->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Customer $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'customertype' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('customerid', 'customertypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Customer
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function businesspartner()
    {
        return $this->belongsTo(BusinessPartnerCustomer::class, 'customerid', 'customerid');
    }

    public function customertype()
    {
        return $this->hasOne(Types::class, 'typeid', 'customertypeid');
    }

    public function village()
    {
        return $this->hasOne(Village::class, 'villageid', 'customeruvid');
    }

    public function bpcustomer()
    {
        return $this->hasMany(BusinessPartnerCustomer::class, 'customerid', 'customerid');
    }

    public function randomStr()
    {
        $code = Helpers::randomStr(6, true);

        /* @var Relation $this */
        $query = $this->where('referalcode', $code)
            ->get();

        if($query->count() > 0)
            return $this->randomStr();

        return $code;
    }

    /**
     * @param string $referalcode
     * @return Collection|static[]
     * */
    public function findReferalCode($referalcode)
    {
        return $this->withJoin($this->defaultSelects)
            ->with([
                'businesspartner' => function($query) {
                    BusinessPartnerCustomer::foreignSelect($query)
                        ->addSelect('customerid', 'bpid');
                }
            ])
            ->where('referalcode', $referalcode)->get();
    }

}

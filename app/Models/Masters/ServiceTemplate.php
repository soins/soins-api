<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class ServiceTemplate extends Model
{

    protected $table = "mstemplateservice";
    protected $primaryKey = "templateserviceid";

    protected $fillable = [
        "assetcategoryid",
        "assetsubcategoryid",
        "assetbrandid",
        "assettypeid",
        "servicenm",
        "payload",
        "createdby",
        "updatedby",
    ];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    public $defaultSelects = [
        "servicenm",
        "payload"
    ];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new ServiceTemplate();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|ServiceTemplate $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select($this->getKeyName())->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|ServiceTemplate
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function assetcategory()
    {
        return $this->hasOne(Types::class, 'typeid', 'assetcategoryid');
    }

    public function assetsubcategory()
    {
        return $this->hasOne(Types::class, 'typeid', 'assetsubcategoryid');
    }

    public function assetbrand()
    {
        return $this->hasOne(Types::class, 'typeid', 'assetbrandid');
    }

    public function assettype()
    {
        return $this->hasOne(Types::class, "typeid", "assettypeid");
    }

    public function jobs()
    {
        return $this->hasMany(ServiceTemplateJobs::class, 'templateserviceid', 'templateserviceid');
    }

    public function defaultQuery()
    {
        return $this->withJoin($this->defaultSelects)
            ->with([
                'assettype' => function($query) {
                    Types::foreignSelect($query);
                },
                'assetcategory' => function($query) {
                    Types::foreignSelect($query);
                },
                'assetsubcategory' => function($query) {
                    Types::foreignSelect($query);
                },
                'assetbrand' => function($query) {
                    Types::foreignSelect($query);
                },
            ])->addSelect('assettypeid', 'assetcategoryid', 'assetsubcategoryid', 'assetbrandid');
    }
}

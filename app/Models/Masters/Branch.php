<?php


namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Branch extends Model
{

    protected $table = "msbranch";
    protected $primaryKey = "branchid";

    protected $fillable = [
        "branchname",
        "branchaddress",
        "branchphone",
        "branchpic",
        "bpid",
        "createdby",
        "updatedby",
        "isactive"
    ];

    const UPDATED_AT = "updateddate";
    const CREATED_AT = "createddate";

    public $defaultSelects = [
        "branchname",
        "branchaddress",
        "branchphone",
        "branchpic"
    ];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $user = new Branch();
        return $user->withJoin(is_null($selects) ? $user->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Branch $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('branchid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Branch
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }
}

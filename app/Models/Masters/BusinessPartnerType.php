<?php


namespace App\Models\Masters;


use App\Models\Settings\MappingType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class BusinessPartnerType extends  Model
{
    protected $table = "stbptype";
    protected $primaryKey = "bptypeid";

    protected $fillable = [
        'bpid',
        'typeid',
        'typevalue',
        'typeremark',
        'typeseq',
        'createdby',
        'updatedby',
        'isactive',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        'typeremark',
        'stbptype.typeseq',
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $bpType = new BusinessPartnerType();
        return $bpType->withJoin(is_null($selects) ? $bpType->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|BusinessPartnerType $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'type' => function($query) {
                Types::foreignSelect($query);
            },
            'typeval' => functioN($query) {
                Types::foreignSelect($query)
                    ->addSelect(DB::raw("(SELECT count(child.typeid) FROM mstype child WHERE child.masterid = typeid) as countchild"));
            }
        ])->select('bptypeid', 'stbptype.typeid', 'typevalue')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|BusinessPartnerType
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'typeid', 'typeid');
    }

    public function typeval()
    {
        return $this->hasOne(Types::class, 'typeid', 'typevalue');
    }

    public function mappingtype()
    {
        return $this->hasOne(MappingType::class, 'mappingvalueid', 'typevalue');
    }
}

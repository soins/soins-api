<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class AssetsDetail extends Model
{

    protected $table = "msassetsdt";
    protected $primaryKey = "assetdtid";

    protected $fillable = [
        "assetid",
        "typeid",
        "typevalue",
        "createdby",
        "updatedby",
        "isactive"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        'typevalue',
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $assetdt = new AssetsDetail();
        return $assetdt->withJoin(is_null($selects) ? $assetdt->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|AssetsDetail $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'type' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('assetdtid', 'typeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|AssetsDetail
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function asset()
    {
        return $this->hasOne(Assets::class, 'assetid', 'assetid');
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'typeid', 'typeid');
    }
}

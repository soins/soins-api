<?php


namespace App\Models\Masters;


use App\Constants\DBTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Menu extends  Model
{

    protected $table = "msmenu";
    protected $primaryKey = "menuid";

    protected $fillable = [
        "masterid",
        "menutypeid",
        "menunm",
        "icon",
        "color",
        "route",
        "seq",
        "createdby",
        "updatedby",
        "isactive"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "menunm",
        "route",
        "icon",
        "color",
        "seq",
    );

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($data) {
            /* @var Menu $data */
            $data->accesses()->delete();
        });
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $menu = new Menu();
        return $menu->withJoin(is_null($selects) ? $menu->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Menu $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'menutype' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('menuid', 'menutypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Menu
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function menutype()
    {
        return $this->hasOne(Types::class, 'typeid', 'menutypeid');
    }

    public function parent()
    {
        return $this->hasOne(Menu::class, 'menuid', 'masterid');
    }

    public function childrens()
    {
        return $this->hasMany(Menu::class, 'masterid','menuid');
    }

    public function accesses()
    {
        return $this->hasMany(Role::class, 'menuid', 'menuid');
    }

    public function getAccessMenu($usertypeid, $bpid, $menutype = DBTypes::menuWeb)
    {
        return $this->withJoin($this->defaultSelects)
            ->with([
                'accesses' => function($query) use ($usertypeid, $bpid) {
                    Role::foreignSelect($query)
                        ->addSelect('menuid')
                        ->where('usertypeid', $usertypeid)
                        ->where('bpid', $bpid);
                }
            ])
            ->whereHas('accesses', function($query) use ($usertypeid, $bpid) {
                /* @var Relation $query */
                $query->where('usertypeid', $usertypeid)
                    ->where('bpid', $bpid);
            })
            ->whereHas('menutype', function($query) use ($menutype) {
                /* @var Relation $query */
                $query->where('typecd', $menutype);
            })
            ->addSelect('masterid')
            ->orderBy("seq")
            ->get();
    }
}

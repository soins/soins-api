<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class BusinessPartner extends Model
{

    protected $table = "msbusinesspartner";
    protected $primaryKey = "bpid";

    protected $fillable = [
        "bpname",
        "bptypeid",
        "bppicname",
        "bpemail",
        "bpphone",
        "createdby",
        "updatedby",
        "isactive"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "bpname",
        "bppicname",
        "bpemail",
        "bpphone",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $userdt = new BusinessPartner();
        return $userdt->withJoin(is_null($selects) ? $userdt->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|BusinessPartner $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'bptype' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('bpid', 'bptypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|BusinessPartner
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function bptype()
    {
        return $this->hasOne(Types::class, 'typeid', 'bptypeid');
    }
}

<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class BusinessPartnerVendor extends Model
{

    protected $table = "stbpvendor";
    protected $primaryKey = "bpvendorid";

    protected $fillable = [
        'bpid',
        'vendorid',
        'createdby',
        'updatedby',
    ];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    public $defaultSelects = [];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new BusinessPartnerVendor();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|BusinessPartnerVendor $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'vendor' => function($query) {
                Vendor::foreignSelect($query);
            }
        ])->select('bpvendorid', 'vendorid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|BusinessPartnerVendor
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function vendor()
    {
        return $this->hasOne(Vendor::class, 'vendorid', 'vendorid');
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }
}

<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class SparePartMapping extends Model
{

    protected $table = "stmappingsparepart";
    protected $primaryKey = "mappingsparepartid";

    protected $fillable = [
        "assettypeid",
        "sparepartid",
        "createdby",
        "updatedby",
        "isactive",
    ];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    public $defaultSelects = [];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new SparePartMapping();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|SparePartMapping $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'assettype' => function($query) {
                Types::foreignSelect($query);
            },
            'sparepart' => function($query) {
                SparePart::foreignSelect($query);
            }
        ])->select('mappingsparepartid', 'assettypeid', 'sparepartid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|SparePartMapping
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function assettype()
    {
        return $this->hasOne(Types::class, 'typeid', 'assettypeid');
    }

    public function sparepart()
    {
        return $this->hasOne(SparePart::class, 'sparepartid', 'sparepartid');
    }
}

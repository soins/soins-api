<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class AssetsService extends Model
{

    protected $table = "msassetsservice";
    protected $primaryKey = "assetserviceid";

    protected $fillable = [
        "assetid",
        "templateserviceid",
        "lastservice",
        "payload",
        "createdby",
        "updatedby",
        "isactive",
    ];

    public const CREATED_AT = "createddate";
    public const UPDATED_AT = "updateddate";

    public $defaultSelects = [
        "lastservice",
        "payload",
    ];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $model = new AssetsService();
        return $model->withJoin(is_null($selects) ? $model->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|AssetsService $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select($this->getKeyName())->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|AssetsService
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function templateservice()
    {
        return $this->hasOne(ServiceTemplate::class, 'templateserviceid', 'templateserviceid');
    }
}

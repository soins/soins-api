<?php


namespace App\Models\Addresses;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Province extends Model
{

    protected $table = "msprovince";
    protected $primaryKey = "provinceid";

    protected $fillable = [
        "countryid",
        "provincename",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "provincename",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $type = new Province();
        return $type->withJoin(is_null($selects) ? $type->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function nested($query, $selects = null)
    {
        $type = new Province();
        return $type->withJoin(is_null($selects) ? $type->defaultSelects : $selects, $query)
            ->with([
                'country' => function($query) {
                    Country::foreignSelect($query);
                }
            ])
            ->addSelect('countryid');
    }

    /**
     * @param Relation|Province $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('provinceid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Province
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'countryid', 'countryid');
    }
}

<?php


namespace App\Models\Addresses;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Subdistrict extends Model
{

    protected $table = "mssubdistrict";
    protected $primaryKey = "subdistrictid";

    protected $fillable = [
        "subdistrictname",
        "cityid"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "subdistrictname"
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $subdistrict = new Subdistrict();
        return $subdistrict->withJoin(is_null($selects) ? $subdistrict->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function nested($query, $selects = null)
    {
        $subdistrict = new Subdistrict();
        return $subdistrict->withJoin(is_null($selects) ? $subdistrict->defaultSelects : $selects, $query)
            ->with([
                'city' => function($query) {
                    City::nested($query);
                }
            ])
            ->addSelect('cityid');
    }

    /**
     * @param Relation|Subdistrict $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('subdistrictid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Subdistrict
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function city()
    {
        return $this->hasOne(City::class, 'cityid', 'cityid');
    }
}

<?php

namespace App\Models\Addresses;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Country extends Model
{

    protected $table = "mscountry";
    protected $primaryKey = "countryid";

    protected $fillable = [
        "countryname",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "countryname",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $type = new Country();
        return $type->withJoin(is_null($selects) ? $type->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Country $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('countryid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Country
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }
}

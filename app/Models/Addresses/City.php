<?php


namespace App\Models\Addresses;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class City extends Model
{

    protected $table = "mscity";
    protected $primaryKey = "cityid";

    protected $fillable = [
        'provinceid',
        'cityname'
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        'cityname',
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $city = new City();
        return $city->withJoin(is_null($selects) ? $city->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function nested($query, $selects = null)
    {
        $city = new City();
        return $city->withJoin(is_null($selects) ? $city->defaultSelects : $selects, $query)
            ->with([
                'province' => function($query) {
                    Province::nested($query);
                }
            ])
            ->addSelect('provinceid');
    }

    /**
     * @param Relation|City $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('cityid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|City
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'countryid', 'countryid');
    }

    public function province()
    {
        return $this->hasOne(Province::class, 'provinceid', 'provinceid');
    }
}

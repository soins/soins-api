<?php


namespace App\Models\Addresses;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Village extends Model
{

    protected $table = "msvillage";
    protected $primaryKey = "villageid";

    protected $fillable = [
        "villagename",
        "subdistrictid",
        "createdby",
        "updatedby",
        "isactive",
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array(
        "villagename",
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $subdistrict = new Village();
        return $subdistrict->withJoin(is_null($selects) ? $subdistrict->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function nested($query, $selects = null)
    {
        $subdistrict = new Village();
        return $subdistrict->withJoin(is_null($selects) ? $subdistrict->defaultSelects : $selects, $query)
            ->with([
                'subdistrict' => function($query) {
                    Subdistrict::nested($query);
                }
            ])
            ->addSelect('subdistrictid');
    }

    /**
     * @param Relation|Village $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select('villageid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Village
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function subdistrict()
    {
        return $this->hasOne(Subdistrict::class, 'subdistrictid', 'subdistrictid');
    }
}

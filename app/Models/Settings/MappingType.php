<?php

namespace App\Models\Settings;

use App\Models\Masters\Types;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class MappingType extends Model
{

    protected $table = "stmappingtype";
    protected $primaryKey = "mappingid";

    protected $fillable = [
        'mappingtypeid',
        'mappingkeyid',
        'mappingparentid',
        'mappingvalueid',
        'createdby',
        'updatedby',
        'isactive',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = [];

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $user = new MappingType();
        return $user->withJoin(is_null($selects) ? $user->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|MappingType $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
        ])->select($this->getKeyName())->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|MappingType
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function mappingtype()
    {
        return $this->hasOne(Types::class, 'typeid', 'mappingtypeid');
    }

    public function mappingparent()
    {
        return $this->hasOne(Types::class, 'typeid', 'mappingtypeid');
    }

    public function mappingvalue()
    {
        return $this->hasOne(Types::class, 'typeid', 'mappingvalueid');
    }
}

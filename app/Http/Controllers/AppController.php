<?php


namespace App\Http\Controllers;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Mail\TicketAll;
use App\Models\Masters\Branch;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Customer;
use App\Models\Masters\Departement;
use App\Models\Masters\Menu;
use App\Models\Masters\Notification;
use App\Models\Masters\Types;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use App\Models\Tickets\Ticket;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use ReflectionClass;

class AppController extends  Controller
{

    /* @var Menu|Relation */
    protected $menu;

    /* @var Menu|Relation */
    protected $user;

    /* @var Ticket|Relation */
    protected $ticket;

    /* @var Notification|Relation */
    protected $notifications;

    public function __construct()
    {
        $this->menu = new Menu();
        $this->user = new User();
        $this->ticket = new Ticket();
        $this->notifications = new Notification();
    }

    public function fetch()
    {
        try {

            $types = new ReflectionClass(DBTypes::class);
            return $this->jsonData($types->getConstants());
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'fetch');
        }
    }

    public function update(Request $req)
    {
        try {

            $userid = $req->input('userid');
            $bpid = $req->input('bpid');
            $usertypeid = $req->input('usertypeid');

            $user = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'userdetails' => function($query) {
                        UserDetail::foreignSelect($query)
                            ->with([
                                'businesspartner' => function($query) {
                                    BusinessPartner::foreignSelect($query);
                                },
                                'customer' => function($query) {
                                    Customer::foreignSelect($query);
                                },
                                'branch' => function($query) {
                                    Branch::foreignSelect($query);
                                },
                                'departement' => function($query) {
                                    Departement::foreignSelect($query);
                                }
                            ])
                            ->addSelect('userid', 'bpid', 'relationid', 'branchid', 'deptid');
                    }
                ])
                ->find($userid);

            if(is_null($user))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $userdetail = null;
            foreach($user->userdetails as $usrdetail) {
                if($usrdetail->bpid == $bpid && $usrdetail->usertypeid == $usertypeid) {
                    $userdetail = $usrdetail;
                }
            }

            if(is_null($userdetail))
                $userdetail = $user->userdetail[0];

            return $this->jsonData(array(
                'user' => array(
                    'userid' => $user->userid,
                    'userfullname' => $user->userfullname,
                    'userdetail' => $userdetail,
                    'userdetails' => $user->userdetails,
                    'token' => str_replace('Bearer ', '', $req->header('Authorization')),
                    'expired' => date('Y-m-d H:i:s', strtotime('+'.env('JWT_TTL').' seconds')),
                ),
                'accessmenus' => $this->menu->getAccessMenu($usertypeid, $bpid),
            ));
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function sendEmail(Request $req)
    {
        try {
            Mail::to('kholifanalfon99@gmail.com')->send(new TicketAll($req));

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function dashboardTicket(Request $req)
    {
        try {
            $query = $this->ticket->select('ticketid', 'depttoid', 'statusid')
                ->with([
                    'departementto' => function($query) {
                        Departement::foreignSelect($query);
                    },
                    'status' => function($query) {
                        Types::foreignSelect($query);
                    }
                ])
                ->where('bpid', $req->get('bpid'));

            if($req->has('depttoid') && !empty($req->get('depttoid')))
                $query->where('depttoid', $req->get('depttoid'));

            $tickets = $query->get();

            $statuses = TypesFinder::find()->byCode([
                DBTypes::statusTicketWaiting,
                DBTypes::statusTicketOpen,
                DBTypes::statusTicketWaitingTechnician,
                DBTypes::statusTicketInProgress,
                DBTypes::statusTicketPending,
                DBTypes::statusTicketResolved,
                DBTypes::statusTicketClose
            ]);

            $countTickets = [];
//            foreach($tickets as $ticket) {
//                if(!array_key_exists($ticket->depttoid, $countTickets))
//                    $countTickets[$ticket->depttoid] = (object) [
//                        'department' => $ticket->departementto,
//                        'statuses' => [
//                            $statuses->get(DBTypes::statusTicketWaiting)->getId() => (object) [
//                                'typename' => $statuses->get(DBTypes::statusTicketWaiting)->getName(),
//                                'count' => 0
//                            ],
//                            $statuses->get(DBTypes::statusTicketOpen)->getId() => (object) [
//                                'typename' => $statuses->get(DBTypes::statusTicketOpen)->getName(),
//                                'count' => 0
//                            ],
//                            $statuses->get(DBTypes::statusTicketWaitingTechnician)->getId() => (object) [
//                                'typename' => $statuses->get(DBTypes::statusTicketWaitingTechnician)->getName(),
//                                'count' => 0
//                            ],
//                            $statuses->get(DBTypes::statusTicketInProgress)->getId() => (object) [
//                                'typename' => $statuses->get(DBTypes::statusTicketInProgress)->getName(),
//                                'count' => 0
//                            ],
//                            $statuses->get(DBTypes::statusTicketPending)->getId() => (object) [
//                                'typename' => $statuses->get(DBTypes::statusTicketPending)->getName(),
//                                'count' => 0
//                            ],
//                            $statuses->get(DBTypes::statusTicketResolved)->getId() => (object) [
//                                'typename' => $statuses->get(DBTypes::statusTicketResolved)->getName(),
//                                'count' => 0
//                            ],
//                            $statuses->get(DBTypes::statusTicketClose)->getId() => (object) [
//                                'typename' => $statuses->get(DBTypes::statusTicketClose)->getName(),
//                                'count' => 0
//                            ]
//                        ],
//                    ];
//
//                $countTickets[$ticket->depttoid]->statuses[$ticket->statusid]->count++;
//            }


            $response = [];
//            foreach($countTickets as $countTicket) {
//
//                $statuses = [];
//                foreach($countTicket->statuses as $status)
//                    $statuses[] = $status;
//
//                $response[] = [
//                    'department' => $countTicket->department,
//                    'statuses' => $statuses,
//                ];
//            }

            return $this->jsonData($response);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function notifications(Request $req)
    {
        try {

            $query = $this->notifications->withJoin(['refid', 'actualid', 'notificationtitle', 'description', 'isviewed', 'ishandled'])
                ->where('notificationtoid', $req->get('userid'))
                ->orderBy('createddate', 'desc');

            $notifications = $query->get();

            $read = $this->notifications->where('notificationtoid', $req->get('userid'))
                ->where(function($query) {
                    /* @var Relation $query */
                    $query->where('isviewed', false)
                        ->orWhere('ishandled', false);
                })
                ->where('notificationtoid', $req->get('userid'))
                ->count();

            return $this->jsonData([
                'countRead' => $read,
                'notifications' => $notifications
            ]);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function readNotification(Request $req)
    {
        try {
            $notification = $this->notifications->find($req->get('notificationid'));

            if(is_null($notification))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $notification->update([
                'isviewed' => true,
                'updatedby' => $req->get('userid'),
            ]);

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

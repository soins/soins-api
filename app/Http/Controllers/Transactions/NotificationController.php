<?php

namespace App\Http\Controllers\Transactions;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Notifications\PushNotification;
use App\Helpers\Types\TypesFinder;
use App\Helpers\Users\UsersFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\Assets;
use App\Models\Masters\Notification;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use App\Models\Schedule\Schedule;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketAsset;
use App\Models\Tickets\TicketProgress;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{

    /* @var Assets|Relation */
    protected $asset;

    /* @var User|Relation */
    protected $user;

    /* @var Notification|Relation */
    protected $notification;

    /* @var Schedule|Relation */
    protected $schedule;

    /* @var Ticket|Relation */
    protected $ticket;

    /* @var TicketAsset|Relation */
    protected $ticketAsset;

    /* @var TicketProgress|Relation */
    protected $ticketProgress;

    public function __construct()
    {
        $this->asset = new Assets();
        $this->user = new User();
        $this->notification = new Notification();
        $this->schedule = new Schedule();
        $this->ticket = new Ticket();
        $this->ticketAsset = new TicketAsset();
        $this->ticketProgress = new TicketProgress();
    }

    public function createSchedule(Request $req)
    {
        try {

            DB::beginTransaction();

            $types = TypesFinder::find()->byCode([
                DBTypes::ticketTypeService,
                DBTypes::statusTicketWaitingTechnician,
                DBTypes::scheduleTypeService,
                DBTypes::statusScheduleCreate,
                DBTypes::progressTicketWaitingTechnician,
                DBTypes::notificationTypeScheduling
            ]);

            $user = UsersFinder::init()->byId($req->get('userid'));

            $assets = $this->asset->select($this->asset->defaultSelects)
                ->find($req->get('assetid'));

            if(is_null($assets))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $period = explode(" - ", $req->get('period'));

            $start = isset($period[0]) ? date('Y-m-d', strtotime($period[0])) : null;
            $end = isset($period[1]) ? date('Y-m-d', strtotime($period[1])) : null;

            $insertTicket = collect($req->only($this->ticket->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'duedate' => $end,
                    'ticketcd' => $this->ticket->getCode(),
                    'tickettitle' => $req->get('scheduletitle'),
                    'tickettypeid' => $types->get(DBTypes::ticketTypeService)->getId(),
                    'deptfromid' => $assets->useddeptid,
                    'depttoid' => $assets->deptid,
                    'branchfromid' => $assets->usedbranchid,
                    'branchtoid' => $assets->branchid,
                    'statusid' => $types->get(DBTypes::statusTicketWaitingTechnician)->getId(),
                    'createdby' => $user->getId(),
                    'updatedby' => $user->getId(),
                ]);

            $ticket = $this->ticket->create($insertTicket->toArray());

            $this->ticketAsset->create([
                'ticketid' => $ticket->ticketid,
                'assetid' => $req->get('assetid'),
                'createdby' => $user->getId(),
                'updatedby' => $user->getId(),
            ]);

            $this->ticketProgress->create([
                'ticketid' => $ticket->ticketid,
                'progresstypeid' => $types->get(DBTypes::progressTicketWaitingTechnician)->getId(),
                'description' => $types->get(DBTypes::progressTicketWaitingTechnician)->getDescFormated([
                    '{userfullname}' => $user->getFullname(),
                ]),
                'createdby' => $user->getId(),
                'updatedby' => $user->getId(),
            ]);

            $insertSchedule = collect($req->only($this->schedule->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'refid' => $ticket->ticketid,
                    'subjectid' => $types->get(DBTypes::scheduleTypeService)->getId(),
                    'statusid' => $types->get(DBTypes::statusScheduleCreate)->getId(),
                    'scheduledatefrom' => $start,
                    'scheduledateto' => $end,
                    'createdby' => $user->getId(),
                    'updatedby' => $user->getId(),
                ]);

            $schedule = $this->schedule->create($insertSchedule->toArray());

            $inQuery = "SELECT usr.userid
                    FROM msuser usr
                    JOIN msuserdt usrdt ON usrdt.userid = usr.userid
                    JOIN mstype usrtype ON usrtype.typeid = usrdt.usertypeid
                    WHERE usrdt.bpid = ".$req->get('bpid')."
                    AND (usrdt.deptid = {$ticket->depttoid} OR usrdt.deptid IS NULL)
                    AND usrtype.typecd IN ('".DBTypes::roleSuperuser."', '".DBTypes::roleAdministrator."')
                ";

            $this->notification->where('refid', $req->get('assetid'))
                ->where('notificationtypeid', $req->get('notificationtypeid'))
                ->whereRaw("notificationtoid IN ($inQuery)")
                ->update([
                    'ishandled' => true,
                    'actualid' => $schedule->scheduleid
                ]);

            if(!is_null($ticket->deptfromid)) {
                $deptid = $ticket->deptfromid;
                $userDepartment = $this->user->withJoin($this->user->defaultSelects)
                    ->with([
                        'userdetail' => function($query) {
                            UserDetail::foreignSelect($query)
                                ->addSelect('userid');
                        }
                    ])
                    ->whereHas('userdetail', function($query) use ($deptid) {
                        /* @var Relation $query */
                        $query->where('deptid', $deptid);
                    })
                    ->get();

                $pushTo = [];
                foreach($userDepartment as $userDept)
                    if(!in_array($userDept->userid, $pushTo))
                        $pushTo[] = $userDept->userid;

                $typeScheduling = $types->get(DBTypes::notificationTypeScheduling);

                $createStart = date_create($start);
                $createEnd = date_create($end);
                $diff = date_diff($createStart, $createEnd);

                $dateSchedule = sprintf("%s - %s", date('d/m/Y', strtotime($start)), date('d/m/Y', strtotime($end)));
                if($diff->days <= 1)
                    $dateSchedule = date('d/m/Y', strtotime($start));

                $notification = new PushNotification($user->getId());
                $notification->setNotification($typeScheduling->getName(), $typeScheduling->getDescFormated([
                    '{assetname}' => $assets->assetname,
                    '{date}' => $dateSchedule
                ]));
                $notification->setNotificationType(DBTypes::notificationTypeScheduling, $schedule->scheduleid);
                $notification->pushTo($pushTo, true);
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }
}

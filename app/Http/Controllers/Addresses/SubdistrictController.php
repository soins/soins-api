<?php


namespace App\Http\Controllers\Addresses;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Addresses\City;
use App\Models\Addresses\Country;
use App\Models\Addresses\Province;
use App\Models\Addresses\Subdistrict;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class SubdistrictController extends Controller
{

    /* @var Subdistrict|Relation */
    protected $subdistrict;

    public function __construct()
    {
        $this->subdistrict = new Subdistrict();
    }

    public function select(Request $req)
    {
        try {
            $query = $this->subdistrict->withJoin($this->subdistrict->defaultSelects)
                ->where('cityid', $req->get('cityid'))
                ->orderBy("subdistrictname")
                ->get();

            $json = array();
            foreach($query->all() as $db)
                $json[] = ['value' => $db->subdistrictid, 'text' => $db->subdistrictname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {
            $query = $this->subdistrict->withJoin($this->subdistrict->defaultSelects)
                ->with([
                    'city' => function($query) {
                        City::nested($query);
                    }
                ])
                ->addSelect('cityid')
                ->orderBy('subdistrictname');

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            $this->subdistrict->create($req->only($this->subdistrict->getFillable()));

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->subdistrict->withJoin($this->subdistrict->defaultSelects)
                ->with([
                    'city' => function($query) {
                        City::foreignSelect($query)
                            ->with([
                                'province' => function($query) {
                                    Province::foreignSelect($query)
                                        ->with([
                                            'country' => function($query) {
                                                Country::foreignSelect($query);
                                            }
                                        ])
                                        ->addSelect('countryid');
                                }
                            ])
                            ->addSelect('provinceid');
                    }
                ])
                ->addSelect('cityid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->subdistrict->find($id, ['subdistrictid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->subdistrict->find($id, ['subdistrictid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

<?php


namespace App\Http\Controllers\Addresses;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Addresses\City;
use App\Models\Addresses\Country;
use App\Models\Addresses\Province;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class CityController extends Controller
{

    /* @var City|Relation */
    protected $city;

    public function __construct()
    {
        $this->city = new City();
    }

    public function select()
    {
        try {
            $query = $this->city->withJoin($this->city->defaultSelects)
                ->orderBy("cityname")
                ->get();

            $json = array();
            foreach($query->all() as $db)
                $json[] = ['value' => $db->cityid, 'text' => $db->cityname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {
            $query = $this->city->withJoin($this->city->defaultSelects)
                ->with([
                    'province' => function($query) {
                        Province::nested($query);
                    }
                ])
                ->addSelect('provinceid');

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            $this->city->create($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->city->withJoin($this->city->defaultSelects)
                ->with([
                    'province' => function($query) {
                        Province::foreignSelect($query)
                            ->with([
                                'country' => function($query) {
                                    Country::foreignSelect($query);
                                }
                            ])
                            ->addSelect('countryid');
                    }
                ])
                ->addSelect('provinceid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->city->find($id, ['cityid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->city->find($id, ['cityid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

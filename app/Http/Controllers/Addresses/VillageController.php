<?php


namespace App\Http\Controllers\Addresses;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Addresses\City;
use App\Models\Addresses\Country;
use App\Models\Addresses\Province;
use App\Models\Addresses\Subdistrict;
use App\Models\Addresses\Village;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class VillageController extends Controller
{

    /* @var Village|Relation */
    protected $village;

    public function __construct()
    {
        $this->village = new Village();
    }

    public function select(Request $req)
    {
        try {
            $query = $this->village->withJoin($this->village->defaultSelects)
                ->where('subdistrictid', $req->get('subdistrictid'))
                ->orderBy("villagename")
                ->get();

            $json = array();
            foreach($query->all() as $db)
                $json[] = ['value' => $db->villageid, 'text' => $db->villagename];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {
            $query = $this->village->withJoin($this->village->defaultSelects)
                ->with([
                    'subdistrict' => function($query) {
                        Subdistrict::nested($query);
                    }
                ])
                ->addSelect('subdistrictid')
                ->orderBy('villagename');

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            $this->village->create($req->only($this->village->getFillable()));

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->village->withJoin($this->village->defaultSelects)
                ->with([
                    'subdistrict' => function($query) {
                        Subdistrict::foreignSelect($query)
                            ->with([
                                'city' => function($query) {
                                    City::foreignSelect($query)
                                        ->with([
                                            'province' => function($query) {
                                                Province::foreignSelect($query)
                                                    ->with([
                                                        'country' => function($query) {
                                                            Country::foreignSelect($query);
                                                        }
                                                    ])
                                                    ->addSelect('countryid');
                                            }
                                        ])
                                        ->addSelect('provinceid');
                                }
                            ])
                            ->addSelect('cityid');
                    }
                ])
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->village->find($id, ['villageid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->village->find($id, ['villageid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

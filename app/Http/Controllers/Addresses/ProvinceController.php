<?php


namespace App\Http\Controllers\Addresses;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Addresses\Country;
use App\Models\Addresses\Province;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class ProvinceController extends  Controller
{

    /* @var Province|Relation */
    protected $province;

    public function __construct()
    {
        $this->province = new Province();
    }

    public function select(Request $req)
    {
        try {
            $query = $this->province->withJoin($this->province->defaultSelects)
                ->where('countryid', $req->get('countryid'))
                ->orderBy("provincename")
                ->get();

            $json = array();
            foreach($query->all() as $db)
                $json[] = ['value' => $db->provinceid, 'text' => $db->provincename];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {
            $query = $this->province->withJoin($this->province->defaultSelects)
                ->with([
                    'country' => function($query) {
                        Country::foreignSelect($query);
                    }
                ])
            ->addSelect('countryid');

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            $this->province->create($req->only($this->province->getFillable()));

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->province->withJoin($this->province->defaultSelects)
                ->with([
                    'country' => function($query) {
                        Country::foreignSelect($query);
                    }
                ])
                ->addSelect('countryid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->province->find($id, ['provinceid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->province->find($id, ['provinceid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

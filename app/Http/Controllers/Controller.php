<?php

namespace App\Http\Controllers;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use Exception;
use Illuminate\Support\Facades\Validator;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @throws Exception
     */
    public function customValidate(array $data, array $rules, array $messages = array())
    {

        $customRules = array();
        $customAttribtues = array();
        foreach($rules as $attribute => $rule) {
            if(strpos($attribute, ':') !== FALSE) {
                list($attributeRule, $attributeName) = explode(':', $attribute);
                $customRules[$attributeRule] = $rule;
                $customAttribtues[$attributeRule] = $attributeName;
            }

            else {
                $customRules[$attribute] = $rule;
            }
        }

        $validator = Validator::make($data,  $customRules, array_merge([
            'required' => ':attribute tidak boleh kosong',
            'numeric' => ':attribute harus angka'
        ], $messages));

        if($validator->fails()) {
            $strmessage = '';
            foreach($validator->errors()->messages() as $attribtue => $arrmessage) {
                foreach ($arrmessage as $message) {
                    if (array_key_exists($attribtue, $customAttribtues)) {
                        $strmessage .= '- ' . str_replace($attribtue, $customAttribtues[$attribtue], $message) . "\n";
                    }

                    else {
                        $strmessage .= '- ' . str_replace($attribtue, ucfirst($attribtue), $message) . "\n";
                    }
                }
            }

            throw new Exception($strmessage);
        }
    }

    public function jsonError(Exception $e, $classname = null, $function = null)
    {

        $code = intval($e->getCode());
        $message = DBMessage::API_SERVER_ERROR_MESSAGE;

        if($code == DBCode::AUTHORIZED_ERROR)
            $message = $e->getMessage();

        return response()->json([
            'status' => $code,
            'result' => false,
            'message' => $message,
            'code' => $code,
            'reporting' => array(
                'type' => 'API Server',
                'filename' => $e->getFile(),
                'classname' => $classname,
                'function' => $function,
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            )
        ], 200);
    }

    public function jsonSuccess($message, $data = array())
    {
        $json['result'] = true;
        $json['status'] = 200;
        $json['message'] = $message;
        $json['data'] = $data;

        return response()->json($json);
    }

    public function jsonData($data = array())
    {
        $json['result'] = true;
        $json['status'] = 200;
        $json['message'] = '';
        $json['data'] = $data;

        return response()->json($json);
    }

    public function jsonWarning($message, $data = array())
    {
        $json['result'] = true;
        $json['status'] = 201;
        $json['message'] = $message;
        $json['data'] = $data;

        return response()->json($json);
    }

    public function jsonCount($message, $count)
    {
        $json['result'] = true;
        $json['status'] = 200;
        $json['message'] = $message;
        $json['count'] = $count;

        return response()->json($json);
    }
}

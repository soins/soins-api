<?php

namespace App\Http\Controllers\Security;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Files\FileUpload;
use App\Helpers\Helpers;
use App\Helpers\Types\TypesFinder;
use App\Helpers\Users\UserField;
use App\Helpers\Users\UsersFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\Files;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    /* @var User|Relation */
    protected $user;

    /* @var UserDetail|Relation */
    protected $userdt;

    public function __construct()
    {
        $this->user = new User();
        $this->userdt = new UserDetail();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->get('searchValue')));
            $query = $this->user->withJoin('userfullname')
                ->where(function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(userfullname))'), 'like', "%{$searchValue}%");
                })
                ->orderBy('userfullname');

            if($req->has('usertype')) {
                $usertype = $req->get('usertype');
                $query->whereHas('userdetails', function($query) use ($usertype) {
                   /* @var Relation $query */
                   $query->whereHas('usertype', function($query) use ($usertype) {
                       /* @var Relation $query */
                       $query->where('typecd', $usertype);
                   });
                });
            }

            if($req->has('branchid') || $req->has('deptid') || $req->has('bpid')) {
                $bpid = $req->get('bpid');
                $branchid = $req->get('branchid');
                $deptid = $req->get('deptid');
                $query->whereHas('userdetails', function($query) use ($branchid, $deptid, $bpid) {
                    /* @var Relation $query */

                    if($branchid > 0)
                        $query->where('branchid', $branchid);

                    if($bpid > 0)
                        $query->where('bpid', $bpid);

                    if($deptid > 0)
                        $query->where('deptid', $deptid);
                });
            }

            $json = array();
            foreach($query->get() as $db)
                $json[] = ['value' => $db->userid, 'text' => $db->userfullname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {

            $query = $this->user->withJoin($this->user->defaultSelects)
                ->where('userid', '!=', $req->get('userid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $users = collect($req->only($this->user->getFillable()))
                ->put('userpassword', Hash::make($req->get('userpassword')));

            $user = $this->user->create($users->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD, array(
                'userid' => $user->userid,
            ));
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'profile' => function($query) {
                        Files::file($query, [
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_URL, Files::IMAGE_SIZE_MEDIUM))
                        ]);
                    }
                ])
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            DB::beginTransaction();

            $row = $this->user->find($id, ['userid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $users = collect($req->only($this->user->getFillable()))
                ->except(['userpassword', 'createdby']);

            if($req->has('userpassword') && !empty($req->get('userpassword')))
                $users->put('userpassword', Hash::make($req->get('userpassword')));

            $row->update($users->toArray());

            DB::commit();

            return $this->jsonData($row);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->user->find($id, ['userid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $this->userdt->where('userid', $row->userid)
                ->delete();

            $row->delete();

            return $this->jsonData($row);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function check($field, Request $req)
    {
        try {
            $userfield = UserField::username;
            if($field == 'email')
                $userfield = UserField::email;

            UsersFinder::init()->check($req->input('value'), $userfield);

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function changePassword(Request $req)
    {
        try {

            $row = $this->user->find($req->get('userid'), ['userid', 'userpassword']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            if(!Hash::check($req->get('current'), $row->userpassword))
                throw new Exception(DBMessage::AUTH_WRONG_PASSWORD, DBCode::AUTHORIZED_ERROR);

            $updates = collect([
                'userpassword' => Hash::make($req->get('new')),
                'updatedby' => $row->userid,
            ]);
            $row->update($updates->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function changeProfile(Request $req)
    {
        try {
            $fileTicket = TypesFinder::find()->byCode(DBTypes::filesProfile);
            $row = $this->user->find($req->get('userid'), ['userid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            if($req->hasFile('profile')) {
                $fileUpload = new FileUpload($row->userid, $fileTicket->get(), $row->userid);

                try {
                    $fileUpload->uploadFileTo($req->file('profile'), sprintf($fileTicket->get()->getJsonDesc('path')));
                    $fileUpload->singleUpdate(function($file) {
                        /* @var UploadedFile $file */
                        return (object) array(
                            'filename' => sprintf("%s.%s", time(), $file->getClientOriginalExtension()),
                        );
                    });
                } catch (Exception $e) {
                    $fileUpload->rollback();
                    throw new Exception($e->getMessage(), DBCode::AUTHORIZED_ERROR);
                }
            }

            $profile = $row->profile;

            return $this->jsonSuccess(DBMessage::SUCCESS, Helpers::imageUrl($profile->directories, $profile->filename, Files::IMAGE_SIZE_MEDIUM));
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

<?php


namespace App\Http\Controllers\Security;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Branch;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Customer;
use App\Models\Masters\Departement;
use App\Models\Masters\UserDetail;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class UserDetailController extends Controller
{

    /* @var UserDetail|Relation */
    protected $userdt;

    public function __construct()
    {
        $this->userdt = new UserDetail();
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->userdt->withJoin($this->userdt->defaultSelects)
                ->with([
                    'businesspartner' => function($query) {
                        BusinessPartner::foreignSelect($query);
                    },
                    'customer' => function($query) {
                        Customer::foreignSelect($query);
                    },
                    'branch' => function($query) {
                        Branch::foreignSelect($query);
                    },
                    'departement' => function($query) {
                        Departement::foreignSelect($query);
                    }
                ])
                ->addSelect('bpid', 'relationid', 'branchid', 'deptid')
                ->where('userid', $req->get('userid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $users = collect($req->only($this->userdt->getFillable()))->filter(function($data) {
                return $data != '';
            });

            if(empty($req->get('deptid')))
                $users->put('deptid', null);

            $this->userdt->create($users->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->userdt->withJoin($this->userdt->defaultSelects)
                ->with([
                    'businesspartner' => function($query) {
                        BusinessPartner::foreignSelect($query);
                    },
                    'customer' => function($query) {
                        Customer::foreignSelect($query);
                    },
                    'branch' => function($query) {
                        Branch::foreignSelect($query);
                    },
                    'departement' => function($query) {
                        Departement::foreignSelect($query);
                    }
                ])
                ->addSelect('bpid', 'relationid', 'branchid', 'deptid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->userdt->find($id, ['userdtid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $users = collect($req->only($this->userdt->getFillable()))
                ->except('createdby')
                ->filter(function($data) { return $data != ''; });

            if(empty($req->get('deptid')))
                $users->put('deptid', null);

            $row->update($users->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->userdt->find($id, ['userdtid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

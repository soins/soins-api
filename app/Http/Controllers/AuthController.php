<?php


namespace App\Http\Controllers;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Helpers;
use App\Helpers\Types\TypesFinder;
use App\Helpers\Users\UserField;
use App\Helpers\Users\UsersFinder;
use App\Models\Masters\Branch;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Customer;
use App\Models\Masters\Departement;
use App\Models\Masters\Files;
use App\Models\Masters\Menu;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    /* @var User|Relation */
    protected $user;

    /* @var UserDetail|Relation */
    protected $userdt;

    /* @var Menu|Relation*/
    protected $menu;

    /* @var Customer|Relation*/
    protected $customer;

    public function __construct()
    {
        $this->user = new User();
        $this->userdt = new UserDetail();

        $this->menu = new Menu();
        $this->customer = new Customer();
    }

    public function login(Request $req)
    {
        try {

            $this->customValidate($req->all(), array(
                'username:Nama pengguna' => 'required|string',
                'password:Kata sandi' => 'required|string',
            ));

            $credentials = $req->only(['username', 'password']);

            if (! $token = Auth::attempt($credentials, true))
                throw new Exception(DBMessage::AUTH_LOGIN_FAILED, DBCode::AUTHORIZED_ERROR);

            /* @var User $user*/
            $user = \auth()->user();

            $datauser = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'userdetails' => function($query) {
                        UserDetail::foreignSelect($query)
                            ->with([
                                'businesspartner' => function($query) {
                                    BusinessPartner::foreignSelect($query);
                                },
                                'customer' => function($query) {
                                    Customer::foreignSelect($query);
                                },
                                'branch' => function($query) {
                                    Branch::foreignSelect($query);
                                },
                                'departement' => function($query) {
                                    Departement::foreignSelect($query);
                                }
                            ])
                            ->addSelect('userid', 'bpid', 'relationid', 'branchid', 'deptid');
                    },
                    'profile' => function($query) {
                        Files::file($query, [
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_URL, Files::IMAGE_SIZE_MEDIUM)),
                        ]);
                    }
                ])
                ->find($user->userid);

            if(collect($datauser->userdetails)->count() == 0)
                throw new Exception(DBMessage::AUTH_UNAUTHORIZED, DBCode::AUTHORIZED_ERROR);

            $userdetails = $datauser->userdetails;

            $response['userid'] = $datauser->userid;
            $response['username'] = $datauser->username;
            $response['userfullname'] = $datauser->userfullname;
            $response['profile'] = $datauser->profile;
            $response['useremail'] = $datauser->useremail;
            $response['userphone'] = $datauser->userphone;
            $response['userdetail'] = $userdetails[0];
            $response['userdetails'] = $userdetails;
            $response['token'] = $token;
            $response['expired'] = date('Y-m-d H:i:s', strtotime('+'.env('JWT_TTL').' seconds'));

            return $this->jsonSuccess(null, $response);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function check()
    {
        try {
            return $this->jsonData(\auth()->user());
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'check');
        }
    }

    public function register(Request $req)
    {
        try {
            DB::beginTransaction();

            UsersFinder::init()->check($req->input('username'), UserField::username);
            UsersFinder::init()->check($req->input('email'), UserField::email);

            $branchnm = $req->get('branchnm');

            $users = collect($req->only($this->user->getFillable()));
            $users->put('userpassword', Hash::make($req->input('password')));

            $user = $this->user->create($users->toArray());

            if($req->has('referalcode')) {
                $usertype = TypesFinder::find()->byCode(DBTypes::roleCustomer);
                if(!$usertype->valid())
                    throw new Exception(sprintf(DBMessage::UNDEFINED_DATA_TYPE, DBTypes::roleCustomer), DBCode::AUTHORIZED_ERROR);

                $rawReferal = $this->customer->findReferalCode($req->input('referalcode'));
                if($rawReferal->count() == 0)
                    throw new Exception(sprintf(DBMessage::AUTH_REFERAL_INVALID, $req->input('referalcode')), DBCode::AUTHORIZED_ERROR);

                $dataReferal = $rawReferal->first();

                $userdt['userid'] = $user->userid;
                $userdt['referalcode'] = $dataReferal->referalcode;
                $userdt['relationid'] = $dataReferal->customerid;
                $userdt['usertypeid'] = $usertype->get()->getId();
                $userdt['branchnm'] = $branchnm;
                $userdt['createdby'] = $user->userid;
                $userdt['updatedby'] = $user->userid;

                if(!is_null($dataReferal->businesspartner))
                    $userdt['bpid'] = $dataReferal->businesspartner->bpid;

                $this->userdt->create($userdt);
            }

            $row = $this->user->find($user->userid);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update([
                'createdby' => $user->userid,
                'updatedby' => $user->userid,
            ]);

            DB::commit();

            return $this->jsonSuccess(DBMessage::AUTH_REGISTER_SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function checkData($field, Request $req)
    {
        try {
            $userfield = UserField::username;
            if($field == 'email')
                $userfield = UserField::email;

            UsersFinder::init()->check($req->input('value'), $userfield);

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function loginTechnician(Request $req)
    {
        try {

            $this->customValidate($req->all(), array(
                'username:Nama pengguna' => 'required|string',
                'password:Kata sandi' => 'required|string',
            ));

            $credentials = $req->only(['username', 'password']);

            if (! $token = Auth::attempt($credentials, true))
                throw new Exception(DBMessage::AUTH_LOGIN_FAILED, DBCode::AUTHORIZED_ERROR);

            /* @var User $user*/
            $user = \auth()->user();

            $datauser = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'userdetails' => function($query) {
                        UserDetail::foreignSelect($query)
                            ->with([
                                'businesspartner' => function($query) {
                                    BusinessPartner::foreignSelect($query);
                                },
                                'customer' => function($query) {
                                    Customer::foreignSelect($query);
                                },
                                'branch' => function($query) {
                                    Branch::foreignSelect($query);
                                },
                                'departement' => function($query) {
                                    Departement::foreignSelect($query);
                                }
                            ])
                            ->addSelect('userid', 'bpid', 'relationid', 'branchid', 'deptid');
                    },
                ])
                ->find($user->userid);

            $userdetails = $datauser->userdetails;

            $usertype = TypesFinder::find()->byCode(DBTypes::roleTechnition);

            if (count($userdetails) < 0 || $userdetails[0]->usertypeid != $usertype->get()->getId())
                throw new Exception(DBMessage::AUTH_LOGIN_FAILED, DBCode::AUTHORIZED_ERROR);

            $response['userid'] = $datauser->userid;
            $response['username'] = $datauser->username;
            $response['userfullname'] = $datauser->userfullname;
            $response['useremail'] = $datauser->useremail;
            $response['userphone'] = $datauser->userphone;
            $response['userdetail'] = $userdetails[0];
            $response['userdetails'] = $userdetails;
            $response['token'] = $token;
            $response['expired'] = date('Y-m-d H:i:s', strtotime('+'.env('JWT_TTL').' seconds'));

            return $this->jsonSuccess(null, $response);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

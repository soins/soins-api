<?php

namespace App\Http\Controllers\Schedules;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Http\Controllers\Controller;
use App\Models\Schedule\Schedule;
use App\Models\Tickets\Ticket;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScheduleController extends Controller
{

    /* @var Schedule|Relation */
    protected $schedule;

    /* @var Ticket|Relation */
    protected $ticket;

    public function __construct()
    {
        $this->schedule = new Schedule();
        $this->ticket = new Ticket();
    }

    public function show($id)
    {
        try {

            $schedule = $this->schedule->withJoin($this->schedule->defaultSelects)
                ->find($id);

            if(is_null($schedule))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            if(in_array($schedule->subject->typecd, [DBTypes::scheduleTypeService, DBTypes::scheduleTypeMaintenance])) {
                $schedule->ticket = $this->ticket->withJoin($this->ticket->defaultSelects)
                    ->find($schedule->refid);
            }

            return $this->jsonData($schedule);
        } catch (\Exception $e){
            return $this->jsonError($e);
        }
    }
}

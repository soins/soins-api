<?php


namespace App\Http\Controllers\Tickets;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Http\Controllers\Controller;
use App\Models\Masters\Assets;
use App\Models\Masters\JobsSparePart;
use App\Models\Masters\ServiceTemplate;
use App\Models\Masters\ServiceTemplateJobs;
use App\Models\Masters\SparePart;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketAssets;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TicketAssetController extends Controller
{

    /* @var Ticket|Relation */
    protected $ticket;

    /* @var TicketAssets|Relation */
    protected $ticketassets;

    public function __construct()
    {
        $this->ticket = new Ticket();
        $this->ticketassets = new TicketAssets();
    }

    public function select(Request $req)
    {
        try {
            $ticketcd = $req->get('ticketcd');
            $searchValue = trim(strtolower($req->get('searchValue')));

            $query = $this->ticketassets->withJoin($this->ticketassets->defaultSelects)
                ->whereHas('ticket', function($query) use ($ticketcd) {
                    /* @var Relation $query */
                    $query->where('ticketcd', $ticketcd);
                })
                ->whereHas('asset', function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(assetname))'), 'like', "%$searchValue%")
                        ->orWhere(DB::raw('TRIM(LOWER(assetno))'), 'like', "%$searchValue%");
                });

            $json = array();
            foreach($query->get() as $db)
                $json[] = $db->asset;

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {
            $ticketcd = $req->get('ticketcd');
            $query = $this->ticketassets->withJoin($this->ticketassets->defaultSelects)
                ->whereHas('ticket', function($query) use ($ticketcd) {
                    /* @var Relation $query */
                    $query->where('ticketcd', $ticketcd);
                });

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $inserts = $req->only($this->ticketassets->getFillable());
            $this->ticketassets->create($inserts);

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->ticketassets->find($id, ['ticketassetid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e){
            return $this->jsonError($e);
        }
    }

    public function sparePart(Request $req)
    {
        try {

            $ticketcd = $req->get('ticketcd');
            $assets = $this->ticketassets->withJoin($this->ticketassets->defaultSelects)
                ->with([
                    'asset' => function($query) {
                        Assets::foreignSelect($query, ['assettypeid', 'assetname'])
                            ->with([
                                'template_services' => function($query) {
                                    ServiceTemplate::foreignSelect($query, ['templateserviceid', 'assettypeid', 'servicenm'])
                                        ->with([
                                            'jobs' => function($query) {
                                                ServiceTemplateJobs::foreignSelect($query)
                                                    ->with([
                                                        'spare_parts' => function($query) {
                                                            JobsSparePart::foreignSelect($query)
                                                                ->addSelect('templatesrvcdtid');
                                                        }
                                                    ])
                                                    ->addSelect('templateserviceid');
                                            }
                                        ]);
                                }
                            ]);
                    }
                ])
                ->whereHas('ticket', function($query) use ($ticketcd) {
                    /* @var Relation $query */
                    $query->where('ticketcd', $ticketcd);
                });

            return $this->jsonData($assets->get());
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

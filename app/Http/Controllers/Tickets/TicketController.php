<?php


namespace App\Http\Controllers\Tickets;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Files\FileUpload;
use App\Helpers\Helpers;
use App\Helpers\Types\TypesFinder;
use App\Helpers\Users\UsersFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\Files;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use App\Models\Schedule\Schedule;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketAssets;
use App\Models\Tickets\TicketDetail;
use App\Models\Tickets\TicketProgress;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{

    /* @var Ticket|Relation */
    protected $ticket;

    /* @var TicketDetail|Relation */
    protected $ticketdetail;

    /* @var TicketProgress|Relation */
    protected $ticketprogress;

    /* @var TicketAssets|Relation */
    protected $ticketassets;

    /* @var Schedule|Relation*/
    protected $schedule;

    public function __construct()
    {
        $this->ticket = new Ticket();
        $this->ticketdetail = new TicketDetail();
        $this->ticketprogress = new TicketProgress();
        $this->ticketassets = new TicketAssets();
        $this->schedule = new Schedule();
    }

    public function datatables(Request $req)
    {
        try {

            $bpid = $req->get('bpid');
            $query = $this->ticket->withJoin($this->ticket->defaultSelects)
                ->with([
                    'datacreated' => function($query) use ($bpid) {
                        User::foreignSelect($query, ['userfullname'])
                            ->with([
                                'userdetail' => function($query) use ($bpid) {
                                    UserDetail::foreignSelect($query, ['userid'])
                                        ->where('bpid', $bpid);
                                }
                            ]);
                    }
                ])
                ->addSelect('trticket.createdby', 'trticket.createddate', 'trticket.updateddate')
                ->orderBy('trticket.createddate', 'desc')
                ->where('bpid', $bpid);

            if($req->has('status') && !empty($req->get('status'))) {
                $statuscd = $req->get('status');
                $query->whereHas('status', function($query) use ($statuscd) {
                   /* @var Relation $query */
                   $query->where('typecd', $statuscd);
                });
            }

            else {
                $query->whereHas('status', function($query) {
                    /* @var Relation $query */
                    $query->where('typecd', '!=', DBTypes::statusTicketClose);
                });
            }

            if($req->has('customerid') && !empty($req->input('customerid'))) {
                $customerid = $req->input('customerid');
                $query->where('customerid', $customerid);
            }

            if($req->has('depttoid') && !empty($req->get('depttoid'))) {
                $query->where('depttoid', $req->get('depttoid'));
            }

            if($req->has('deptfromid') && !empty($req->get('deptfromid'))) {
                $query->where('deptfromid', $req->get('deptfromid'));
            }

            $filter = collect($req->get('filter'));
            if(!empty($filter->get('customerid')))
                $query->where('customerid', $filter->get('customerid'));

            if(!empty($filter->get('statusid')))
                $query->where('statusid', $filter->get('statusid'));

            if(!empty($filter->get('startdate')) && !empty($filter->get('enddate')))
                $query->whereBetween(DB::raw("TO_CHAR(createddate, 'YYYY-MM-DD')"), array($filter->get('startdate'), $filter->get('enddate')));

            else if(!empty($filter->get('startdate')) && empty($filter->get('enddate')))
                $query->whereDate('createddate', $filter->get('startdate'));

            return $this->jsonData(datatables()->eloquent($query)
                ->editColumn('createddate', function($data) {
                    return date('Y-m-d H:i:s', strtotime($data->createddate));
                })
                ->editColumn('updateddate', function($data) {
                    return date('Y-m-d H:i:s', strtotime($data->updateddate));
                })
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $user = UsersFinder::init()->byId($req->input('userid'));

            $types = TypesFinder::find()->byCode(DBTypes::statusTicketWaiting, DBTypes::progressTicketCreated, DBTypes::filesTicket);
            if(!$types->valid())
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $insertTickets = collect($req->only($this->ticket->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'statusid' => $types->get(DBTypes::statusTicketWaiting)->getId(),
                    'ticketcd' => $this->ticket->getCode(),
                ]);

            $ticket = $this->ticket->create($insertTickets->toArray());

            $assets = explode(",", $req->get('assets'));
            foreach($assets as $assetid) {

                $this->ticketassets->create([
                    'ticketid' => $ticket->ticketid,
                    'assetid' => $assetid,
                    'statusid' => 0,
                    'createdby' => $user->getId(),
                    'updatedby' => $user->getId(),
                ]);

                $progress = $types->get(DBTypes::progressTicketCreated);

                $notusDocument = array(
                    ['insert' => $progress->getDescFormated(array(
                        '{userfullname}' => $user->getFullname(),
                    ))."\n"]
                );
            }

            $this->ticketprogress->create([
                'ticketid' => $ticket->ticketid,
                'ticketdtid' => null,
                'assetid' => null,
                'description' => json_encode($notusDocument),
                'progresstypeid' => $progress->getId(),
                'createdby' => $user->getId(),
                'updatedby' => $user->getId(),
            ]);

            if($req->hasFile('files')) {
                $fileTicket = $types->get(DBTypes::filesTicket);
                $fileUpload = new FileUpload($user->getId(), $fileTicket, $ticket->ticketid);

                try {
                    $fileUpload->uploadFileTo($req->file('files'), sprintf($fileTicket->getJsonDesc('path')));
                    $fileUpload->save(function($file) {
                        /* @var UploadedFile $file */
                        return (object) array(
                            'filename' => sprintf("%s.%s", time(), $file->getClientOriginalExtension()),
                        );
                    });
                } catch (Exception $e) {
                    throw new Exception($e->getMessage(), DBCode::AUTHORIZED_ERROR);
                }
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD, $ticket);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($ticketcd, Request $req)
    {
        try {
            $bpid = $req->get('bpid');
            $row = $this->ticket->withJoin($this->ticket->defaultSelects)
                ->with([
                    'datacreated' => function($query) use ($bpid) { User::history($query, $bpid); },
                    'dataupdated' => function($query) use ($bpid) { User::history($query, $bpid); },
                    'files' => function($query) {
                        Files::foreignSelect($query)->addSelect(array(
                            DB::raw(Helpers::imageUrlField()),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_MEDIUM_THUMNAIL)),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                        ))
                        ->whereHas('transtype', function($query) {
                            /* @var Relation $query */
                            $query->where('typecd', DBTypes::filesTicket);
                        });
                    }
                ])
                ->addSelect($this->ticket->history)
                ->where('ticketcd', $ticketcd)
                ->first();

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->ticket->find($id, ['ticketid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updateTicket = collect($req->only($this->ticket->getFillable()))
                ->filter(function($data) { return $data != ''; });
            $row->update($updateTicket->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->ticket->find($id, ['ticketid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }

    public function loadDetails(Request $req)
    {
        try {
            $bpid = $req->get('bpid');
            $ticketcd = $req->get('ticketcd');

            $datas = $this->ticketdetail->withJoin($this->ticketdetail->defaultSelects)
                ->with([
                    'ticket' => function($query) {
                        /* @var Relation $query */
                        $query->select('ticketid', 'ticketcd');
                    },
                    'datacreated' => function($query) use ($bpid) { User::history($query, $bpid); },
                    'dataupdated' => function($query) use ($bpid) { User::history($query, $bpid); },
                    'files' => function($query) {
                        Files::foreignSelect($query)->addSelect(array(
                            DB::raw(Helpers::imageUrlField()),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_MEDIUM_THUMNAIL)),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                        ))
                        ->whereHas('transtype', function($query) {
                            /* @var Relation $query */
                            $query->where('typecd', DBTypes::filesTicketReply);
                        });
                    }
                ])
                ->addSelect('ticketid')->addSelect($this->ticketdetail->historySelects)
                ->whereHas('ticket', function($query) use ($ticketcd) {
                    /* @var Relation $query */
                    $query->where('ticketcd', $ticketcd);
                })
                ->orderBy('createddate', 'desc');

            return $this->jsonData($datas->get());
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function reply(Request $req)
    {
        try {
            $userid = $req->input('userid');

            DB::beginTransaction();

            $types = TypesFinder::find()->byCode(DBTypes::filesTicketReply);
            $user = UsersFinder::init()->byId($userid);

            $ticket = $this->ticket->select('ticketid', 'tickettitle')
                ->where('ticketcd', $req->get('ticketcd'))
                ->first();

            if(is_null($ticket))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $ticketdt = $this->ticketdetail->create([
                'ticketid' => $ticket->ticketid,
                'statusid' => 0,
                'description' => $req->get('description'),
                'createdby' => $userid,
                'updatedby' => $userid
            ]);

            if($req->hasFile('files')) {
                $fileTicket = $types->get(DBTypes::filesTicketReply);
                $fileUpload = new FileUpload($userid, $fileTicket, $ticketdt->ticketdtid);

                try {
                    $fileUpload->uploadFileTo($req->file('files'), sprintf($fileTicket->getJsonDesc('path')));
                    $fileUpload->save(function($file) {
                        /* @var UploadedFile $file */
                        return (object) array(
                            'filename' => sprintf("%s.%s", time(), $file->getClientOriginalExtension()),
                        );
                    });
                } catch (Exception $e) {
                    throw new Exception($e->getMessage(), DBCode::AUTHORIZED_ERROR);
                }
            }

//            $mailable = new Mailable();
//            $mailable->from(env('MAIL_USERNAME'), 'PT. Access');
//            $mailable->to($user->getEmail(), $user->getFullname());
//            $mailable->subject($ticket->tickettitle);
//            $mailable->view('<p>'.$req->get('description').'</p>');
//
//            Mail::send($mailable);

            $update = $this->ticket->find($ticket->ticketid);
            $update->update(array(
               'updatedby' => $userid
            ));

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function schedule(Request $req)
    {
        try {

            DB::beginTransaction();

            $user = UsersFinder::init()->byId($req->get('userid'));
            $assignTo = UsersFinder::init()->byId($req->get('assigntoid'));
            $types = TypesFinder::find()->byCode($req->get('subjectcd'), DBTypes::statusTicketWaitingTechnician, DBTypes::progressTicketWaitingTechnician, DBTypes::statusScheduleCreate);

            if($types->hasError())
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $ticket = $this->ticket->where('ticketcd', $req->get('ticketcd'))
                ->first();

            if(is_null($ticket))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $scheduledatefrom = dbDate($req->get('scheduledatefrom'));
            $scheduledateto = !empty($req->get('scheduledateto')) ? dbDate($req->get('scheduledateto')) : $scheduledatefrom;

            $schedules = collect($req->only($this->schedule->getFillable()))
                ->merge([
                    'refid' => $ticket->ticketid,
                    'subjectid' => $types->get($req->get('subjectcd'))->getId(),
                    'customerid' => $ticket->customerid,
                    'statusid' => $types->get(DBTypes::statusScheduleCreate)->getId(),
                    'scheduledatefrom' => $scheduledatefrom,
                    'scheduledateto' => $scheduledateto
                ])->toArray();

            $this->schedule->create($schedules);

            $this->ticketprogress->create([
                'ticketid' => $ticket->ticketid,
                'description' => json_encode(array(
                    ['insert' => $types->get(DBTypes::progressTicketWaitingTechnician)->getDescFormated([
                        '{technician}' => $assignTo->getFullname()
                    ]) . "\n"],
                )),
                'progresstypeid' => $types->get(DBTypes::progressTicketWaitingTechnician)->getId(),
                'createdby' => $user->getId(),
                'updatedby' => $user->getId(),
            ]);

            $this->ticket->update([
                'statusid' => $types->get(DBTypes::statusTicketWaitingTechnician)->getId(),
                'updatedby' => $user->getId(),
            ]);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function confirm(Request $req)
    {
        try {

            DB::beginTransaction();

            $user = UsersFinder::init()->byId($req->get('userid'));

            $ticket = $this->ticket->find($req->get('ticketid'));

            if(is_null($ticket))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $types = TypesFinder::find()->byCode(DBTypes::statusTicketOpen, DBTypes::progressTicketConfirmed);

            $ticket->update([
                'statusid' => $types->get(DBTypes::statusTicketOpen)->getId(),
                'updatedby' => $user->getId(),
            ]);

            $this->ticketprogress->create([
                'ticketid' => $ticket->ticketid,
                'description' => json_encode(array(
                    ['insert' => $types->get(DBTypes::progressTicketConfirmed)->getDescFormated(array(
                        '{userfullname}' => $user->getFullname()
                    )) . "\n"],
                )),
                'progresstypeid' => $types->get(DBTypes::progressTicketConfirmed)->getId(),
                'createdby' => $user->getId(),
                'updatedby' => $user->getId(),
            ]);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }
}

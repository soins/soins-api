<?php


namespace App\Http\Controllers\Tickets;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\Assets;
use App\Models\Masters\User;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketAssets;
use App\Models\Tickets\TicketProgress;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TicketProgressController extends Controller
{

    /* @var Ticket|Relation */
    protected $ticket;

    /* @var TicketProgress|Relation */
    protected $ticketprogress;

    /* @var TicketAssets|Relation */
    protected $ticketassets;

    public function __construct()
    {
        $this->ticket = new Ticket();
        $this->ticketprogress = new TicketProgress();
        $this->ticketassets = new TicketAssets();
    }

    public function load(Request $req)
    {
        try {
            $ticketcd = $req->get('ticketcd');

            $datas = $this->ticketprogress->withJoin($this->ticketprogress->defaultSelects)
                ->with([
                    'datacreated' => function($query) { User::foreignSelect($query); },
                    'dataupdated' => function($query) { User::foreignSelect($query); },
                    'asset' => function($query) {
                        Assets::foreignSelect($query);
                    }
                ])
                ->addSelect('assetid')
                ->addSelect($this->ticketprogress->historySelects)
                ->whereHas('ticket', function($query) use ($ticketcd) {
                    /* @var Relation $query */
                    $query->where('ticketcd', $ticketcd);
                })
                ->orderBy('createddate', 'desc');

            return $this->jsonData($datas->get());
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {
            $ticketcd = $req->get('ticketcd');
            $userid = $req->get('userid');

            DB::beginTransaction();

            $progresstypeid = $req->get('progresstypeid');

            /* Cari status close sudah ada tau belum */
            $types = TypesFinder::find()->byCode(DBTypes::statusTicketClose);
            if($types->hasError())
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            /* Proses ambil ticketid dari ticketcd */
            $ticket = $this->ticket->select('ticketid')
                ->where('ticketcd', $ticketcd)
                ->first();

            if(is_null($ticket))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $dataprogress = collect($req->only($this->ticketprogress->getFillable()))
                ->filter(function($data) { return !empty($data) && $data != 'null'; })
                ->put('ticketid', $ticket->ticketid);

            /* Proses insert data ke progress */
            $this->ticketprogress->create($dataprogress->toArray());

            /* Ketika memilih asset maka update status berdasarkan asset yang dipilih */
            if($req->has('assetid') && !empty($req->get('assetid')) && $req->get('assetid') != 'null') {
                $this->ticketassets->where('assetid', $req->get('assetid'))
                    ->where('ticketid', $ticket->ticketid)
                    ->update(['statusid' => $progresstypeid]);
            }

            /* Ketika tidak mimilih asset maka update semua asset*/
            else {
                $this->ticketassets->where('ticketid', $ticket->ticketid)
                    ->update(['statusid' => $progresstypeid]);
            }

            $typeids = TypesFinder::find()->byId($progresstypeid);
            if($typeids->hasError())
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updates['updatedby'] = $userid;

            /* Jika tipe progress terpilih adalah selesai maka check semua status assets */
            if($typeids->get($progresstypeid, 'typeid')->getCode() == DBTypes::progressTicketComplete) {
                $allassets = $this->ticketassets->withJoin($this->ticketassets->defaultSelects)
                    ->where('ticketid', $ticket->ticketid);

                /* Check semua status assets */
                $allcomplete = true;
                foreach($allassets->get() as $db) {
                    if(!is_null($db->status)) {
                        if($db->status->typecd != DBTypes::progressTicketComplete) {
                            $allassets = false;
                            break;
                        }
                    }
                }

                /* Jika sudah selesai maka update status header menjadi close */
                if($allcomplete)
                    $updates['statusid'] = $types->get(DBTypes::statusTicketClose)->getId();
            }

            $update = $this->ticket->find($ticket->ticketid);
            $update->update($updates);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS, array(
                'redirect' => $typeids->get($progresstypeid, 'typeid')->getCode() == DBTypes::progressTicketComplete,
            ));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }
}

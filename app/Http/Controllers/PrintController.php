<?php


namespace App\Http\Controllers;


use App\Documents\PDF\PrintOutAllTicket;
use App\Models\Masters\User;
use App\Models\Tickets\Ticket;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrintController extends Controller
{

    /* @var Ticket|Relation */
    protected $ticket;

    public function __construct()
    {
        $this->ticket = new Ticket();
    }

    public function ticketAll(Request $req)
    {
        try {

            $bpid = $req->get('bpid');

            $tickets = $this->ticket->withJoin($this->ticket->defaultSelects)
                ->with([
                    'datacreated' => function($query) use ($bpid) {
                        User::history($query, $bpid);
                    }
                ])
                ->addSelect($this->ticket->history)
                ->where('bpid', $bpid);

            if($req->has('customerid'))
                $tickets->where('customerid', $req->get('customerid'));

            if($req->has('statusid'))
                $tickets->where('statusid', $req->get('statusid'));

            if($req->has('startdate') && !$req->has('enddate'))
                $tickets->where(DB::raw("TO_CHAR(createddate, 'YYYY-MM-DD')"), $req->get('startdate'));

            else if($req->has('startdate') && $req->has('enddate'))
                $tickets->whereBetween(DB::raw("TO_CHAR(createddate, 'YYYY-MM-DD')"), array($req->get('startdate'), $req->get('enddate')));

            $document = new PrintOutAllTicket();
            $document->setDatas($tickets->get());
            $document->build();

            return $document->response();
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

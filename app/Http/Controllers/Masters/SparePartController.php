<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\SparePart;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SparePartController extends Controller
{

    /* @var SparePart|Relation */
    protected $sparePart;

    public function __construct()
    {
        $this->sparePart = new SparePart();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->get('searchValue')));

            $query = $this->sparePart->withJoin($this->sparePart->defaultSelects)
                ->where('bpid', $req->get('bpid'))
                ->where(function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(sparepartname))'), 'like', "%$searchValue%");
                });

            return $this->jsonData($query->get());
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {

            $query = $this->sparePart->queryData()
                ->where('bpid', $req->get('bpid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $userid = $req->get('userid');

            $insertSparePart = collect($req->only($this->sparePart->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

            $this->sparePart->create($insertSparePart->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->sparePart->queryData()
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $userid = $req->get('userid');
            $row = $this->sparePart->queryData()
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updateSparePart = collect($req->only($this->sparePart->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'updatedby' => $userid,
                ]);
            $row->update($updateSparePart->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->sparePart->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

}

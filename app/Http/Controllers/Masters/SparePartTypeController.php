<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartnerType;
use App\Models\Masters\SparePartType;
use App\Models\Masters\SparePartVendor;
use App\Models\Masters\Types;
use App\Models\Settings\MappingType;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SparePartTypeController extends Controller
{

    /* @var Types|Relation */
    protected $type;

    /* @var BusinessPartnerType|Relation */
    protected $bpType;

    /* @var MappingType|Relation */
    protected $deptType;

    /* @var SparePartVendor|Relation */
    protected $vendors;

    public function __construct()
    {
        $this->type = new Types();
        $this->bpType = new BusinessPartnerType();
        $this->deptType = new MappingType();
        $this->vendors = new SparePartVendor();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->get('searchValue')));

            $query = $this->type->withJoin($this->type->defaultSelects)
                ->where(function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(typename))'), 'like', "%$searchValue%");
                });

            return $this->jsonData($query->get());
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {

            $query = $this->type->withJoin($this->type->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Types::foreignSelect($query)->with([
                            'parent' => function($query) {
                                Types::foreignSelect($query);
                            }
                        ])->addSelect('masterid');
                    }
                ])
                ->addSelect('masterid')
                ->whereHas('parent', function($query) {
                    /* @var Relation $query */
                    $query->whereHas('parent', function($query) {
                        /* @var Relation $query */
                        $query->whereHas('parent', function($query) {
                            /* @var Relation $query */
                            $query->where('typecd', DBTypes::sparePartCategory);
                        });
                    });
                });

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $types = TypesFinder::find()->byCode([DBTypes::sparePartCategory, DBTypes::mappingTypeDepartment]);

            $bpid = $req->get('bpid');
            $deptid = $req->get('deptid');
            $userid = $req->get('userid');
            $categoryId = $req->get('categoryid');
            if($categoryId == '') {
                $category = $this->type->create([
                    'masterid' => $types->get(DBTypes::sparePartCategory)->getId(),
                    'typename' => $req->get('categoryname'),
                    'description' => $req->get('categorydesc'),
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                $this->bpType->create([
                    'bpid' => $bpid,
                    'typeid' => $category->masterid,
                    'typevalue' => $category->typeid,
                    'typeremark' => $category->typename,
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                if(!empty($deptid)) {
                    $this->deptType->create([
                        'mappingtypeid' => $types->get(DBTypes::mappingTypeDepartment)->getId(),
                        'mappingkeyid' => $deptid,
                        'mappingparentid' => $category->masterid,
                        'mappingvalueid' => $category->typeid,
                        'createdby' => $userid,
                        'updatedby' => $userid,
                    ]);
                }

                $categoryId = $category->typeid;
            }

            $subCategoryId = $req->get('subcategoryid');
            if($subCategoryId == '') {
                $subCategory = $this->type->create([
                    'masterid' => $categoryId,
                    'typename' => $req->get('subcategoryname'),
                    'description' => $req->get('subcategorydesc'),
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                $this->bpType->create([
                    'bpid' => $bpid,
                    'typeid' => $subCategory->masterid,
                    'typevalue' => $subCategory->typeid,
                    'typeremark' => $subCategory->typename,
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                if(!empty($deptid)) {
                    $this->deptType->create([
                        'mappingtypeid' => $types->get(DBTypes::mappingTypeDepartment)->getId(),
                        'mappingkeyid' => $deptid,
                        'mappingparentid' => $subCategory->masterid,
                        'mappingvalueid' => $subCategory->typeid,
                        'createdby' => $userid,
                        'updatedby' => $userid,
                    ]);
                }

                $subCategoryId = $subCategory->typeid;
            }

            $type = $this->type->create([
                'masterid' => $subCategoryId,
                'typename' => $req->get('typename'),
                'description' => $req->get('typedesc'),
                'createdby' => $userid,
                'updatedby' => $userid,
            ]);

            $this->bpType->create([
                'bpid' => $bpid,
                'typeid' => $type->masterid,
                'typevalue' => $type->typeid,
                'typeremark' => $type->typename,
                'createdby' => $userid,
                'updatedby' => $userid,
            ]);

            if(!empty($deptid)) {
                $this->deptType->create([
                    'mappingtypeid' => $types->get(DBTypes::mappingTypeDepartment)->getId(),
                    'mappingkeyid' => $deptid,
                    'mappingparentid' => $type->masterid,
                    'mappingvalueid' => $type->typeid,
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->type->withJoin($this->type->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Types::foreignSelect($query)
                            ->with([
                                'parent' => function($query) {
                                    Types::foreignSelect($query);
                                }
                            ])->addSelect('masterid');
                    }
                ])
                ->addSelect('masterid')
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            DB::beginTransaction();

            $types = TypesFinder::find()->byCode([DBTypes::sparePartCategory]);

            $bpid = $req->get('bpid');
            $deptid = $req->get('deptid');
            $userid = $req->get('userid');
            $categoryId = $req->get('categoryid');
            if($categoryId == '') {
                $category = $this->type->create([
                    'masterid' => $types->get(DBTypes::sparePartCategory)->getId(),
                    'typename' => $req->get('categoryname'),
                    'description' => $req->get('categorydesc'),
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                $this->bpType->create([
                    'bpid' => $bpid,
                    'typeid' => $category->masterid,
                    'typevalue' => $category->typeid,
                    'typeremark' => $category->typename,
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                if(!empty($deptid)) {
                    $this->deptType->create([
                        'mappingtypeid' => $types->get(DBTypes::mappingTypeDepartment)->getId(),
                        'mappingkeyid' => $deptid,
                        'mappingparentid' => $category->masterid,
                        'mappingvalueid' => $category->typeid,
                        'createdby' => $userid,
                        'updatedby' => $userid,
                    ]);
                }

                $categoryId = $category->typeid;
            }

            $subCategoryId = $req->get('subcategoryid');
            if($subCategoryId == '') {
                $subCategory = $this->type->create([
                    'masterid' => $categoryId,
                    'typename' => $req->get('subcategoryname'),
                    'description' => $req->get('subcategorydesc'),
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                $this->bpType->create([
                    'bpid' => $bpid,
                    'typeid' => $subCategory->masterid,
                    'typevalue' => $subCategory->typeid,
                    'typeremark' => $subCategory->typename,
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

                if(!empty($deptid)) {
                    $this->deptType->create([
                        'mappingtypeid' => $types->get(DBTypes::mappingTypeDepartment)->getId(),
                        'mappingkeyid' => $deptid,
                        'mappingparentid' => $subCategory->masterid,
                        'mappingvalueid' => $subCategory->typeid,
                        'createdby' => $userid,
                        'updatedby' => $userid,
                    ]);
                }

                $subCategoryId = $subCategory->typeid;
            }

            $type = $this->type->find($id);

            if(is_null($type))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $type->update([
                'masterid' => $subCategoryId,
                'typename' => $req->get('typename'),
                'description' => $req->get('typedesc'),
                'updatedby' => $userid,
            ]);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function destroy(Request $req, $id)
    {
        try {

            $bpid = $req->get('bpid');
            $deptid = $req->get('deptid');

            /* @var SparePartType $row */
            $row = $this->type->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $this->bpType->where('bpid', $bpid)
                ->where('typevalue', $id)
                ->delete();

            $this->deptType->where('mappingkeyid', $deptid)
                ->where('mappingvalueid', $id)
                ->whereHas('mappingtype', function($query) {
                    /* @var Relation $query */
                    $query->where('typecd', DBTypes::mappingTypeDepartment);
                })
                ->delete();

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

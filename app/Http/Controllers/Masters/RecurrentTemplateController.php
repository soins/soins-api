<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\RecurrentTemplate;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class RecurrentTemplateController extends Controller
{

    /* @var RecurrentTemplate|Relation */
    protected $template;

    public function __construct()
    {
        $this->template = new RecurrentTemplate();
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->template->withJoin($this->template->defaultSelects)
                ->where('bpid', $req->get('bpid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $userid = $req->get('userid');

            $insertTemplate = collect($req->only($this->template->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'updatedby' => $userid,
                    'createdby' => $userid,
                ]);

            $this->template->create($insertTemplate->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->template->withJoin($this->template->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->template->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updateTemplate = collect($req->only($this->template->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'updatedby' => $req->get('userid'),
                ]);

            $row->update($updateTemplate->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->template->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

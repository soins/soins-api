<?php


namespace app\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Branch;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BranchController extends Controller
{

    /* @var Branch|Relation */
    protected $branch;

    public function __construct()
    {
        $this->branch = new Branch();
    }

    public function select(Request $req)
    {
        try {
            $searcValue = trim(strtolower($req->get('searchValue')));

            $query = $this->branch->select('branchid', 'branchname')
                ->where(function($query) use ($searcValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(branchname))'), 'like', "%{$searcValue}%");
                })
                ->where('bpid', $req->get('bpid'))
                ->get();

            $json = array();
            foreach($query as $db)
                $json[] = ['value' => $db->branchid, 'text' => $db->branchname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->branch->withJoin($this->branch->defaultSelects)
                ->where('bpid', $req->get('bpid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $inserts = $req->only($this->branch->getFillable());
            $this->branch->create($inserts);

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->branch->withJoin($this->branch->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->branch->find($id, ['branchid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updates = collect($req->only($this->branch->getFillable()))
                ->except('createdby');

            $row->update($updates->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT, DBCode::AUTHORIZED_ERROR);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->branch->find($id, ['branchid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

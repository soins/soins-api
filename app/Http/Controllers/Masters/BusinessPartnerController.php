<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartner;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessPartnerController extends  Controller
{

    /* @var BusinessPartner|Relation */
    protected $businesspartner;

    public function __construct()
    {
        $this->businesspartner = new BusinessPartner();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            $query = $this->businesspartner->withJoin($this->businesspartner->defaultSelects)
                ->where(DB::raw('TRIM(LOWER(bpname))'), 'like', "%$searchValue%");

            $json = array();
            foreach($query->get() as $db)
                $json[] = ['value' => $db->bpid, 'text' => $db->bpname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'select');
        }
    }

    public function datatables()
    {
        try {
            $query = $this->businesspartner->withJoin($this->businesspartner->defaultSelects);

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {
            $this->businesspartner->create($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->businesspartner->withJoin($this->businesspartner->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {
            $row = $this->businesspartner->find($id, ['bpid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->businesspartner->find($id, ['bpid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

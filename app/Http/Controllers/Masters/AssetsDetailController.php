<?php


namespace app\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\AssetsDetail;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class AssetsDetailController extends Controller
{

    /* @var AssetsDetail|Relation */
    protected $assetdt;

    public function __construct()
    {
        $this->assetdt = new AssetsDetail();
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->assetdt->withJoin($this->assetdt->defaultSelects)
                ->where('assetid', $req->get('assetid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $inserts = $req->only($this->assetdt->getFillable());
            $asset = $this->assetdt->create($inserts);

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD, [
                'assetid' => $asset->assetid,
            ]);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {
            $row = $this->assetdt->withJoin($this->assetdt->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {
            $row = $this->assetdt->find($id, ['assetdtid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updates = collect($req->only($this->assetdt->getFillable()))
                ->except('createdby');
            $row->update($updates->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->assetdt->find($id, ['assetdtid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

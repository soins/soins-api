<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Addresses\Village;
use App\Models\Masters\BusinessPartnerCustomer;
use App\Models\Masters\Customer;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BpCustomerController extends Controller
{

    /* @var BusinessPartnerCustomer|Relation */
    protected $bpcustomer;

    /* @var Customer|Relation */
    protected $customer;

    public function __construct()
    {
        $this->customer = new Customer();
        $this->bpcustomer = new BusinessPartnerCustomer();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            $query = $this->bpcustomer->withJoin($this->bpcustomer->defaultSelects)
                ->where(function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(customername))'), 'like', "%$searchValue%");
                    $query->orWhere(DB::raw('TRIM(LOWER(customeraddress))'), 'like', "%$searchValue%");
                })
                ->where('bpid', $req->input('bpid'))
                ->addSelect('customerid');

            return $this->jsonData($query->get());
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'select');
        }
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->bpcustomer->withJoin($this->bpcustomer->defaultSelects)
                ->with([
                    'customer' => function($query) {
                        Customer::foreignSelect($query);
                    }
                ])
                ->addSelect('customerid')
                ->where('bpid', $req->get('bpid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $customers = collect($req->only($this->customer->getFillable()))
                ->put('referalcode', $this->customer->randomStr());

            $customer = $this->customer->create($customers->all());

            $bpcustomers = collect($req->only($this->bpcustomer->getFillable()))
                ->put('customerid', $customer->customerid);

            $this->bpcustomer->create($bpcustomers->all());

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->bpcustomer->withJoin($this->bpcustomer->defaultSelects)
                ->with([
                    'customer' => function($query) {
                        Customer::foreignSelect($query)
                            ->with([
                                'village' => function($query) {
                                    Village::nested($query);
                                }
                            ])
                            ->addSelect('customeruvid');
                    }
                ])
                ->addSelect('customerid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            DB::beginTransaction();

            $userid = $req->get('userid');
            $row = $this->bpcustomer->find($id, ['bpcustomerid', 'customerid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $fillable = collect($this->bpcustomer->getFillable());

            $bpcustomers = collect($req->only($fillable->all()))
                ->except('createdby');
            $row->update($bpcustomers->all());

            $customer = $this->customer->find($row->customerid, ['customerid']);

            if(is_null($customer))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $customers = collect($req->only($this->customer->getFillable()))
                ->except($fillable)
                ->put('updatedby', $userid);
            $customer->update($customers->all());

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->bpcustomer->find($id, ['bpcustomerid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

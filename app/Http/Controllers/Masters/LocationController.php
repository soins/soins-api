<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Branch;
use App\Models\Masters\Location;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{

    /* @var Location|Relation */
    protected $location;

    public function __construct()
    {
        $this->location = new Location();
    }

    public function select(Request $req)
    {
        try {
            $searcValue = trim(strtolower($req->get('searchValue')));

            $query = $this->location->select('locationid', 'locationname')
                ->where(function($query) use ($searcValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(locationname))'), 'like', "%{$searcValue}%");
                })
                ->where('bpid', $req->get('bpid'))
                ->where('branchid', $req->get('branchid'))
                ->get();

            $json = array();
            foreach($query as $db)
                $json[] = ['value' => $db->locationid, 'text' => $db->locationname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->location->withJoin($this->location->defaultSelects)
                ->with([
                    'branch' => function($query) {
                        Branch::foreignSelect($query);
                    }
                ])
                ->addSelect('branchid')
                ->where('bpid', $req->get('bpid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $inserts = $req->only($this->location->getFillable());
            $this->location->create($inserts);

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->location->withJoin($this->location->defaultSelects)
                ->with([
                    'branch' => function($query) {
                        Branch::foreignSelect($query);
                    }
                ])
                ->addSelect('branchid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->location->find($id, ['locationid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updates = collect($req->only($this->location->getFillable()))
                ->except('createdby');

            $row->update($updates->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT, DBCode::AUTHORIZED_ERROR);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->location->find($id, ['locationid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\JobsSparePart;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class JobsSparePartController extends Controller
{

    /* @var Relation|JobsSparePart */
    protected $jobssp;

    public function __construct()
    {
        $this->jobssp = new JobsSparePart();
    }

    public function datatables(Request $req)
    {
        try {
            $templatesrvcdtid = $req->get('templatesrvcdtid');

            $query = $this->jobssp->withJoin($this->jobssp->defaultSelects)
                ->where('templatesrvcdtid', $templatesrvcdtid);

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $userid = $req->get('userid');

            $insertJobsSparePart = collect($req->only($this->jobssp->getFillable()))
                ->merge([
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);
            $this->jobssp->create($insertJobsSparePart->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {
            $row = $this->jobssp->withJoin($this->jobssp->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $userid = $req->get('userid');
            $row = $this->jobssp->withJoin($this->jobssp->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updateJobsSparePart = collect($req->only($this->jobssp->getFillable()))
                ->merge([
                    'updatedby' => $userid,
                ]);
            $row->update($updateJobsSparePart->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->jobssp->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

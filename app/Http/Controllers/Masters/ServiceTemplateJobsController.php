<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\ServiceTemplateJobs;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class ServiceTemplateJobsController extends Controller
{

    /* @var ServiceTemplateJobs|Relation*/
    protected $serviceJobs;

    public function __construct()
    {
        $this->serviceJobs = new ServiceTemplateJobs();
    }

    public function all(Request $req)
    {
        try {
            $templateserviceid = $req->get('templateserviceid');
            $query = $this->serviceJobs->withJoin($this->serviceJobs->defaultSelects)
                ->where('templateserviceid', $templateserviceid);

            return $this->jsonData($query->get());
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $insertJobs = collect($req->only($this->serviceJobs->getFillable()));
            $sJob = $this->serviceJobs->create($insertJobs->toArray());

            $primaryKey = $this->serviceJobs->getKeyName();
            return $this->jsonSuccess(DBMessage::SUCCESS_ADD, [
                 $primaryKey => $sJob->$primaryKey,
            ]);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->serviceJobs->withJoin($this->serviceJobs->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->serviceJobs->withJoin($this->serviceJobs->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updateJobs = collect($req->only($this->serviceJobs->getFillable()));
            $row->update($updateJobs->toArray());

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->serviceJobs->withJoin($this->serviceJobs->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Http\Controllers\Controller;
use App\Models\Masters\Branch;
use App\Models\Masters\Customer;
use App\Models\Masters\Departement;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BpUserController extends Controller
{

    /* @var UserDetail|Relation */
    protected $user;

    /* @var UserDetail|Relation */
    protected $userdetail;

    public function __construct()
    {
        $this->user = new User();
        $this->userdetail = new UserDetail();
    }

    public function datatables(Request $req)
    {
        try {
            $userid = $req->get('userid');
            $bpid = $req->get('bpid');
            $query = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'userdetails' => function($query) use ($bpid) {
                        UserDetail::foreignSelect($query)
                            ->with([
                                'customer' => function($query) {
                                    Customer::foreignSelect($query);
                                },
                                'branch' => function($query) {
                                    Branch::foreignSelect($query);
                                },
                                'departement' => function($query) {
                                    Departement::foreignSelect($query);
                                }
                            ])
                            ->addSelect('userid', 'relationid', 'branchid', 'deptid')
                            ->where('bpid', $bpid);
                    }
                ])
                ->whereHas('userdetails', function($query) use ($bpid) {
                    /* @var Relation $query */
                    $query->where('bpid', $bpid);
                })
                ->where('userid', '!=', $userid)
                ->orderBy('userfullname');

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $bpid = $req->get('bpid');
            $userid = $req->get('userid');

            $users = collect($req->only($this->user->getFillable()))
                ->put('userpassword', Hash::make($req->get('password')))
                ->put('createdby', $userid)
                ->put('updatedby', $userid);

            $user = $this->user->create($users->all());

            $userdetails = json_decode($req->userdetails);

            $inserts = array();
            foreach($userdetails as $userdetail) {
                $inserts[] = array(
                    'userid' => $user->userid,
                    'usertypeid' => $userdetail->usertype->typeid,
                    'bpid' => $bpid,
                    'relationid' => !empty($userdetail->relationid) ? $userdetail->relationid : 1,
                    'branchid' => $userdetail->branchid,
                    'deptid' => !empty($userdetail->deptid) ? $userdetail->deptid : null,
                    'createdby' => $userid,
                    'createddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $userid,
                    'updateddate' => date('Y-m-d H:i:s'),
                );
            }

            $this->userdetail->insert($inserts);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show(Request $req, $id)
    {
        try {
            $bpid = $req->bpid;
            $row = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'userdetails' => function($query) use ($bpid) {
                        UserDetail::foreignSelect($query)
                            ->with([
                                'customer' => function($query) {
                                    Customer::foreignSelect($query);
                                },
                                'branch' => function($query) {
                                    Branch::foreignSelect($query);
                                },
                                'departement' => function($query) {
                                    Departement::foreignSelect($query);
                                }
                            ])
                            ->addSelect('userid', 'relationid', 'bpid', 'branchid', 'deptid')
                            ->where('bpid', $bpid);
                    }
                ])
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            DB::beginTransaction();;

            $row = $this->user->find($id, ['userid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $users = collect($req->only($this->user->getFillable()))
                ->put('updatedby', $row->userid)
                ->except('createdby');

            if($req->has('password') && !empty($req->input('password')))
                $users->put('userpassword', Hash::make($req->input('password')));

            $row->update($users->toArray());

            $userdetails = json_decode($req->input('userdetails'));


            $userdtids = array();
            $updates = array();
            $inserts = array();
            $deletes = array();

            $usertypeids = explode(",", $req->input('usertypeid'));
            foreach($userdetails as $userdetail) {
                foreach($usertypeids as $usertypeid) {
                    if($userdetail->usertypeid == $usertypeid) {

                        $values = collect($userdetail)->only($this->userdetail->getFillable());

                        if($userdetail->usertype->typecd != DBTypes::roleCustomer)
                            $values->put('relationid', 1);

                        if(!empty($userdetail->userdtid)) {
                            $userdtids[] = $userdetail->userdtid;

                            if(empty($userdetail->deptid))
                                $values->put('deptid', null);

                            $values->put('updatedby', $row->userid);
                            $updates[$userdetail->userdtid] = $values->toArray();
                        }

                        else {
                            $values->put('createdby', $row->userid);
                            $values->put('updatedby', $row->userid);

                            $inserts[] = $values->toArray();
                        }
                    }

                    else {
                        if(!empty($userdetail->userdtid))
                            $deletes[] = $userdetail->userdtid;
                    }
                }
            }

            $userdts = $this->userdetail->whereIn($this->userdetail->getKeyName(), $userdtids)
                ->get();
            foreach($userdts as $userdt) {
                foreach($updates[$userdt->userdtid] as $field => $value)
                    $userdt->$field = $value;

                $userdt->save();
            }

            $this->userdetail->whereIn($this->userdetail->getKeyName(), $deletes)
                ->delete();

            $this->userdetail->insert($inserts);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function destroy(Request $req, $id)
    {
        try {
            $bpid = $req->get('bpid');

            DB::beginTransaction();

            $row = $this->user->find($id, ['userid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $userdeleted = $this->userdetail->where('userid', $id)
                ->where('bpid', $bpid);

            $userdeleted->delete();

            $userdetails = $this->userdetail->where('userid', $id);

            if($userdetails->count() == 0)
                $row->delete();

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

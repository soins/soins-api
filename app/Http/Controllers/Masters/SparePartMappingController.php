<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\SparePartMapping;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SparePartMappingController extends Controller
{

    /* @var SparePartMapping|Relation */
    protected $mapping;

    public function __construct()
    {
        $this->mapping = new SparePartMapping();
    }

    public function datatables(Request $req)
    {
        try {

            $query = $this->mapping->withJoin($this->mapping->defaultSelects)
                ->where('assettypeid', $req->get('assettypeid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $userid = $req->get('userid');
            $spareparts = explode(",", $req->get('spareparts'));

            DB::beginTransaction();

            $insertMappings = [];
            foreach($spareparts as $sparepart) {
                $insertMappings[] = collect($req->only($this->mapping->getFillable()))
                    ->filter(function($data) { return $data != ''; })
                    ->put('sparepartid', $sparepart)
                    ->merge([
                        'createdby' => $userid,
                        'createddate' => date('Y-m-d H:i:s'),
                        'updatedby' => $userid,
                        'updateddate' => date('Y-m-d H:i:s'),
                    ])
                    ->toArray();
            }

            $this->mapping->insert($insertMappings);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->mapping->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

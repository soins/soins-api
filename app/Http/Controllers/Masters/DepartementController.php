<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Branch;
use App\Models\Masters\Departement;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartementController extends Controller
{

    /* @var Departement|Relation */
    protected $departement;

    public function __construct()
    {
        $this->departement = new Departement();
    }

    public function select(Request $req)
    {
        try {
            $searcValue = trim(strtolower($req->get('searchValue')));

            $query = $this->departement->select('deptid', 'deptname')
                ->where(function($query) use ($searcValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(deptname))'), 'like', "%{$searcValue}%");
                })
                ->where('bpid', $req->get('bpid'));

            if($req->has('branchid'))
                $query->where('branchid', $req->get('branchid'));

            $json = array();
            foreach($query->get() as $db)
                $json[] = ['value' => $db->deptid, 'text' => $db->deptname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->departement->withJoin($this->departement->defaultSelects)
                ->with([
                    'branch' => function($query) {
                        Branch::foreignSelect($query);
                    }
                ])
                ->addSelect('branchid')
                ->where('bpid', $req->get('bpid'));

            if($req->has('branchid') && $req->get('branchid') > 0)
                $query->where('branchid', $req->get('branchid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $inserts = $req->only($this->departement->getFillable());
            $this->departement->create($inserts);

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->departement->withJoin($this->departement->defaultSelects)
                ->with([
                    'branch' => function($query) {
                        Branch::foreignSelect($query);
                    }
                ])
                ->addSelect('branchid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->departement->find($id, ['deptid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updates = collect($req->only($this->departement->getFillable()))
                ->except('createdby');

            $row->update($updates->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT, DBCode::AUTHORIZED_ERROR);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->departement->find($id, ['deptid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

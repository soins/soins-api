<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Assets;
use App\Models\Masters\AssetsDetail;
use App\Models\Masters\AssetsService;
use App\Models\Masters\JobsSparePart;
use App\Models\Masters\ServiceTemplate;
use App\Models\Masters\ServiceTemplateJobs;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssetsController extends Controller
{

    /* @var Assets|Relation */
    protected $assets;

    /* @var AssetsDetail|Relation */
    protected $assetsdt;

    /* @var AssetsService|Relation */
    protected $assetsservice;

    /* @var ServiceTemplate|Relation */
    protected $templateservice;

    public function __construct()
    {
        $this->assets = new Assets();
        $this->assetsdt = new AssetsDetail();
        $this->assetsservice = new AssetsService();
        $this->templateservice = new ServiceTemplate();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            DB::enableQueryLog();
            $query = $this->assets->withJoin($this->assets->defaultSelects)
                ->where(function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(assetname))'), 'like', "%$searchValue%");
                })
                ->where('bpid', $req->get('bpid'));

            if($req->has('deptid') && $req->get('deptid'))
                $query->where('deptid', $req->get('deptid'));

            if($req->has('ticketid'))
                $query->whereRaw("assetid NOT IN (SELECT assetid FROM trticketasset WHERE ticketid = {$req->get('ticketid')})");

            if($req->has('currentdeptid') && $req->get('currentdeptid') > 0)
                $query->where('useddeptid', $req->get('currentdeptid'));

            return $this->jsonData($query->get());
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'selectApi');
        }
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->assets->queryData()
                ->where('bpid', $req->get('bpid'));

            if($req->has('branchid') && $req->get('branchid') > 0)
                $query->where('branchid', $req->get('branchid'));

            if($req->has('deptid') && $req->get('deptid') > 0)
                $query->where('deptid', $req->get('deptid'));

            if($req->has('categoryid'))
                $query->where('assetcategoryid', $req->get('categoryid'));

            if($req->has('brandid'))
                $query->where('merkid', $req->get('brandid'));

            if($req->has('typeid'))
                $query->where('assettypeid', $req->get('typeid'));

            if($req->has('picbranchid'))
                $query->where('branchid', $req->get('picbranchid'));

            if($req->has('picdepartmentid'))
                $query->where('deptid', $req->get('picdepartmentid'));

            if($req->has('usedbranchid'))
                $query->where('usedbranchid', $req->get('usedbranchid'));

            if($req->has('useddeptid'))
                $query->where('useddeptid', $req->get('useddeptid'));

            if($req->has('locationid'))
                $query->where('locationid', $req->get('locationid'));

            if($req->has('usedbyid'))
                $query->where('usedbyid', $req->get('usedbyid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $inserts = collect($req->only($this->assets->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'schedulepattern' => '{"period":"PatternPeriod.daily","pattern":"PatternPeriodDaily.everyDay","range":"1"}',
                    'recurrentpattern' => '{"pattern":"PatternRangeRecurrent.forever","start":"","lastModified":null}',
                ]);

            if($req->has('purchasedate'))
                $inserts->put('purchasedate', dbDate($req->get('purchasedate')));

            if($req->has('warrantyexpdate'))
                $inserts->put('warrantyexpdate', dbDate($req->get('warrantyexpdate')));

            if($req->has('purchaseprice'))
                $inserts->put('purchaseprice', dbIDR($req->get('purchaseprice')));

            $asset = $this->assets->create($inserts->toArray());

            $assetdetail = $this->assetsdt->whereHas('asset', function($query) use ($asset) {
                /* @var Relation $query */
                $query->where('bpid', $asset->bpid)
                    ->where('branchid', $asset->branchid)
                    ->where('deptid', $asset->deptid)
                    ->where('assettypeid', $asset->assettypeid);
            })->get();

            $insertDetail = [];
            foreach($assetdetail as $assetdt) {
                $insertDetail[] = [
                    'assetid' => $asset->assetid,
                    'typeid' => $assetdt->typeid,
                    'typevalue' => $assetdt->typevalue,
                    'createdby' => $req->get('createdby'),
                    'createddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $req->get('updatedby'),
                    'updateddate' => date('Y-m-d H:i:s'),
                ];
            }

            $this->assetsdt->insert($insertDetail);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD, [
                'assetid' => $asset->assetid,
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {
            $row = $this->assets->queryData()
                ->with(Assets::relationHistory())
                ->with([
                    'template_services' => function($query) {
                        ServiceTemplate::foreignSelect($query)
                            ->with([
                                'jobs' => function($query) {
                                    ServiceTemplateJobs::foreignSelect($query)
                                        ->with([
                                            'spare_parts' => function($query) {
                                                JobsSparePart::foreignSelect($query)
                                                    ->addSelect('templatesrvcdtid');
                                            }
                                        ])
                                        ->addSelect('templateserviceid');
                                }
                            ])
                            ->addSelect('assettypeid');
                    }
                ])
                ->addSelect($this->assets->historySelects)
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {
            $row = $this->assets->find($id, ['assetid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updates = collect($req->only($this->assets->getFillable()))
                ->except('createdby')
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'purchasedate' => dbDate($req->get('purchasedate')),
                    'warrantyexpdate' => dbDate($req->get('warrantyexpdate')),
                    'purchaseprice' => dbIDR($req->get('purchaseprice')),
                ]);

            if($req->has('purchasedate'))
                $updates->put('purchasedate', dbDate($req->get('purchasedate')));

            if($req->has('warrantyexpdate'))
                $updates->put('warrantyexpdate', dbDate($req->get('warrantyexpdate')));

            if($req->has('purchaseprice'))
                $updates->put('purchaseprice', dbIDR($req->get('purchaseprice')));


            $row->update($updates->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->assets->find($id, ['assetid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
    public function check($field, Request $req)
    {
        try {
            $result = $this->assets->where($field, $req->get('value'));

            if($result->count() > 0)
                throw new Exception(sprintf(DBMessage::DATA_EXIST, 'No. Asset', $req->get('value')), DBCode::AUTHORIZED_ERROR);

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function showService(Request $req)
    {
        try {
            $row = $this->assetsservice->where('assetid', $req->get('assetid'))
                ->where('templateserviceid', $req->get('templateserviceid'))
                ->with([
                    'templateservice' => function($query) {
                        ServiceTemplate::foreignSelect($query);
                    }
                ])
                ->first();

            if(is_null($row)) {
                $templateService = $this->templateservice->select('templateserviceid', 'servicenm', 'payload')
                    ->find($req->get('templateserviceid'));

                $row = (object) [
                    'templateserviceid' => $req->get('templateserviceid'),
                    'templateservice' => $templateService
                ];
            }

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function saveService(Request $req)
    {
        try {

            $userid = $req->get('userid');

            $assetService = $this->assetsservice->where('assetid', $req->get('assetid'))
                ->where('templateserviceid', $req->get('templateserviceid'))
                ->first();

            if(is_null($assetService)) {
                $insertService = collect($req->only($this->assetsservice->getFillable()))
                    ->merge([
                        'createdby' => $userid,
                        'updatedby' => $userid,
                    ]);

                $this->assetsservice->create($insertService->toArray());
            } else {
                $this->assetsservice->where('assetid', $req->get('assetid'))
                    ->where('templateserviceid', $req->get('templateserviceid'))
                    ->update([
                        'payload' => $req->get('payload')
                    ]);
            }

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

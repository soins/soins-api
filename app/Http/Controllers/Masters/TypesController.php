<?php


namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\Types;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TypesController extends Controller
{

    /* @var Types|Relation */
    protected $types;

    public function __construct()
    {
        $this->types = new Types();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            $query = $this->types->withJoin($this->types->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Types::foreignSelect($query);
                    }
                ])
                ->where(function ($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(typecd))'), 'like', "%$searchValue%");
                    $query->orWhere(DB::raw('TRIM(LOWER(typename))'), 'like', "%$searchValue%");
                });

            if ($req->has('typeid') && !empty($req->typeid)) {
                $typeid = $req->post('typeid');
                $query->where('id', '!=', $typeid);
                $query->where('parentid', '!=', $typeid);
            }

            if ($req->has('typecd')) {
                $typecd = $req->post('typecd');
                $query->whereHas('parent', function ($query) use ($typecd) {
                    /* @var Relation $query */
                    $query->where('typecd', $typecd);
                });
            }

            if ($req->has('masterid')) {
                $masterid = $req->post('masterid');
                $query->where('masterid', $masterid);
            }

            return $this->jsonData($query->get());
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {
            $query = $this->types->withJoin($this->types->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Types::foreignSelect($query);
                    }
                ])
                ->addSelect('masterid');

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {
            if(!empty($req->get('typecd')))
                TypesFinder::find()->check($req->input('typecd'));

            $this->types->create($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function find(Request $req, $value)
    {
        try {
            $field = $req->get('field', 'typecd');

            $row = $this->types->withJoin($this->types->defaultSelects)
                ->where($field, strtoupper($value))
                ->first();

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {
            $row = $this->types->withJoin($this->types->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Types::foreignSelect($query);
                    }
                ])
                ->addSelect('masterid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {
            if(!empty($req->get('typecd')))
                TypesFinder::find()->check($req->input('typecd'), $id);

            $row = $this->types->find($id, ['typeid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->types->find($id, ['typeid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

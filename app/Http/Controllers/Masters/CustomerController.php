<?php


namespace app\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Addresses\Village;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Customer;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{

    /* @var Customer|Relation */
    protected $customer;

    public function __construct()
    {
        $this->customer = new Customer();
    }

    public function search(Request $req)
    {
        try {
            $bpid = $req->get('bpid');
            $customername = trim(strtolower($req->get('customername')));
            if(empty($customername))
                $customername = '---------------';

            $query = $this->customer->select('customerid')
                ->addSelect($this->customer->defaultSelects)
                ->with([
                    'bpcustomer' => function($query) use ($customername, $bpid) {
                        $words = collect(explode(' ', $customername));
                        /* @var Relation $query */
                        $query->with([
                            'businesspartner' => function($query) {
                                BusinessPartner::foreignSelect($query, ['bpid', 'bpname']);
                            }
                        ])
                        ->select('bpcustomerid', 'customerid', 'customername', 'customeraddress', 'bpid')
                        ->where(function($query) use ($words) {
                            foreach($words->all() as $i => $word) {
                                /* @var Relation $query */
                                $i == 0 ? $query->where(DB::raw('TRIM(LOWER(customername))'), 'like', "%{$word}%")
                                    : $query->orWhere(DB::raw('TRIM(LOWER(customername))'), 'like', "%{$word}%");
                            }
                        });

                        if(!empty($bpid))
                            $query->where('bpid', $bpid);
                    }
                ])
                ->where(function($query) use ($customername, $bpid) {
                    $words = collect(explode(' ', $customername));

                    /* @var Relation $query */
                    $query->where(function($query) use ($words) {
                        foreach($words->all() as $i => $word) {
                            /* @var Relation $query */
                            $i == 0 ? $query->where(DB::raw('TRIM(LOWER(customername))'), 'like', "%{$word}%")
                                : $query->orWhere(DB::raw('TRIM(LOWER(customername))'), 'like', "%{$word}%");
                        }
                    });

                    $query->orWhereHas('bpcustomer', function($query) use ($words, $bpid) {
                        /* @var Relation $query */
                        $query->where(function($query) use ($words) {
                            foreach($words->all() as $i => $word) {
                                /* @var Relation $query */
                                $i == 0 ? $query->where(DB::raw('TRIM(LOWER(customername))'), 'like', "%{$word}%")
                                    : $query->orWhere(DB::raw('TRIM(LOWER(customername))'), 'like', "%{$word}%");
                            }
                        });

                        if(!empty($bpid))
                            $query->where('bpid', $bpid);
                    });
                });
            $result = $query->get();

            return $this->jsonData($result);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {
            $query = $this->customer->withJoin($this->customer->defaultSelects);

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {
            if($req->has('resultcustomerid') && !empty($req->get('resultcustomerid'))) {
                $row = $this->customer->find($req->get('resultcustomerid'), ['customerid']);

                if(is_null($row))
                    throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

                $customers = collect($req->only($this->customer->getFillable()))
                    ->except('createdby');
                $row->update($customers->toArray());
            }

            else {
                $customers = $req->only($this->customer->getFillable());
                $this->customer->create($customers);
            }

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->customer->withJoin($this->customer->defaultSelects)
                ->with([
                    'village' => function($query) {
                        Village::nested($query);
                    }
                ])
                ->addSelect('customeruvid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {
            $row = $this->customer->find($id, ['customerid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $customers = collect($req->only($this->customer->getFillable()))
                ->except('createdby');
            $row->update($customers->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->customer->find($id, ['customerid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Jobs;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobsController extends Controller
{

    /* @var Jobs|Relation */
    protected $jobs;

    public function __construct()
    {
        $this->jobs = new Jobs();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->get('searchValue')));

            $query = $this->jobs->withJoin($this->jobs->defaultSelects)
                ->where(function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(jobname))'), 'like', "%$searchValue%");
                });

            return $this->jsonData($query->get());
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables()
    {
        try {
            $query = $this->jobs->withJoin($this->jobs->defaultSelects);

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $insertJobs = collect($req->only($this->jobs->getFillable()));
            $this->jobs->create($insertJobs->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->jobs->withJoin($this->jobs->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {
            $row = $this->jobs->withJoin($this->jobs->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updateJobs = collect($req->only($this->jobs->getFillable()));
            $row->update($updateJobs->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->jobs->withJoin($this->jobs->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

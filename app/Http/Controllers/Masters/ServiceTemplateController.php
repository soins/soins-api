<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\ServiceTemplate;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class ServiceTemplateController extends Controller
{

    /* @var ServiceTemplate|Relation */
    protected $serviceTemplate;

    public function __construct()
    {
        $this->serviceTemplate = new ServiceTemplate();
    }

    public function datatables()
    {
        try {
            $query = $this->serviceTemplate->defaultQuery();

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {
            $userid = $req->get('userid');

            $insertTemplate = collect($req->only($this->serviceTemplate->getFillable()))
                ->merge([
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);
            $serviceTemplate = $this->serviceTemplate->create($insertTemplate->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD, [
                'templateserviceid' => $serviceTemplate->templateserviceid,
            ]);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {

            $row = $this->serviceTemplate->defaultQuery()
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {

            $row = $this->serviceTemplate->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

<?php

namespace App\Http\Controllers\Masters;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Helpers\Collections\TypeCollection;
use App\Helpers\Collections\VendorCollection;
use App\Helpers\Vendor\VendorFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartnerVendor;
use App\Models\Masters\Vendor;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BpVendorController extends Controller
{

    /* @var Vendor|Relation */
    protected $vendor;

    /* @var BusinessPartnerVendor|Relation */
    protected $bpvendor;

    public function __construct()
    {
        $this->vendor = new Vendor();
        $this->bpvendor = new BusinessPartnerVendor();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->get('searchValue')));

            $query = $this->bpvendor->withJoin()
                ->where('bpid', $req->get('bpid'))
                ->whereHas('vendor', function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw("TRIM(LOWER(vendornm))"), 'like', "%$searchValue%");
                });

            $json = [];
            foreach($query->get() as $db)
                $json[] = $db->vendor;

            return $this->jsonData($json);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->bpvendor->withJoin()
                ->where('bpid', $req->get('bpid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {

            $userid = $req->get('userid');
            $name = $req->get('vendornm');
            $vendor = VendorFinder::find()->search($name);

            DB::beginTransaction();

            $insertVendor = collect($req->only($this->vendor->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge([
                    'createdby' => $userid,
                    'updatedby' => $userid,
                ]);

            if(empty($vendor->getId())) {
                $vendor = VendorCollection::create($insertVendor->toArray());
            }

            $insertBpVendor = collect($insertVendor->only($this->bpvendor->getFillable()))
                ->merge([
                    'vendorid' => $vendor->getId(),
                    'bpid' => $req->get('bpid'),
                ]);

            $this->bpvendor->create($insertBpVendor->toArray());

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {
            $row = $this->bpvendor->withJoin()
                ->find($id);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {
            $userid = $req->get('userid');
            $row = $this->bpvendor->find($id, ['bpvendorid', 'vendorid']);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $updateVendor = collect($req->only($this->vendor->getFillable()))
                ->filter(function($data) { return $data != ''; })
                ->merge(['updatedby' => $userid]);

            $vendor = $this->vendor->find($row->vendorid, ['vendorid']);

            if(is_null($vendor))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $vendor->update($updateVendor->toArray());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->bpvendor->find($id, ['bpvendorid']);

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

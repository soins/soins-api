<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Collections\TypeCollection;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartnerType;
use App\Models\Masters\Types;
use App\Models\Settings\MappingType;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BpTypeController extends Controller
{
    /* @var Types|Relation */
    protected $types;

    /* @var BusinessPartnerType|Relation */
    protected $bptype;

    /* @var MappingType|Relation */
    protected $mappingtype;

    public function __construct()
    {
        $this->types = new Types();
        $this->bptype = new BusinessPartnerType();
        $this->mappingtype = new MappingType();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            $query = $this->bptype->withJoin($this->bptype->defaultSelects)
                ->whereHas('type', function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(typename))'), 'like', "%$searchValue%");
                })
                ->whereHas('typeval', function($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('TRIM(LOWER(typename))'), 'like', "%$searchValue%");
                })
                ->where('bpid', $req->get('bpid'));

            if($req->has('typecd')) {
                $typecd = $req->get('typecd');
                $query->whereHas('type', function($query) use ($typecd) {
                    /* @var Relation $query */
                    $query->where('typecd', $typecd);
                });
            }

            if($req->has('typeid')) {
                $query->where('typeid', $req->get('typeid'));
            }

            if($req->has('deptid') && !empty($req->get('deptid'))) {
                $deptid = $req->get('deptid');
                $query->whereHas('mappingtype', function($query) use ($deptid) {
                    /* @var Relation $query */
                    $query->whereHas('mappingtype', function($query) {
                        /* @var Relation $query */
                        $query->where('typecd', DBTypes::mappingTypeDepartment);
                    })->where('mappingkeyid', $deptid);
                });
//                $query->whereRaw("typevalue IN (
//                    SELECT mapping.mappingvalueid
//                    FROM stmappingtype mapping
//                    JOIN mstype mappingtype ON mappingtype.typeid = mapping.mappingtypeid
//                    WHERE mapping.mappingkeyid = $deptid
//                    AND mappingtype.typecd = '" . DBTypes::mappingTypeDepartment . "'
//                )");
            }

            $json = array();
            foreach($query->get() as $db)
                $json[] = $db->typeval;

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'select');
        }
    }

    public function datatables(Request $req)
    {
        try {

            $query = $this->bptype->withJoin($this->bptype->defaultSelects)
                ->where('stbptype.bpid', $req->bpid);

            if($req->has('deptid') && !empty($req->get('deptid'))) {
                $deptid = $req->get('deptid');
                $query->whereRaw("typevalue IN (
                    SELECT mapping.mappingvalueid
                    FROM stmappingtype mapping
                    JOIN mstype mappingtype ON mappingtype.typeid = mapping.mappingtypeid
                    WHERE mapping.mappingkeyid = $deptid
                    AND mappingtype.typecd = '" . DBTypes::mappingTypeDepartment . "'
                )");
            }

            if($req->has('parentcd')) {
                $parentcd = $req->get('parentcd');
                $query->whereHas('type', function($query) use ($parentcd) {
                    /* @var Relation $query */
                    $query->where('typecd', strtoupper($parentcd));
                });
            }

            if($req->has('childdata')) {
                $childdata = $req->get('childdata');
                if(in_array($childdata, [DBTypes::brand, DBTypes::assetCategory])) {
                    $query->whereHas('typeval', function($query) use ($childdata) {
                        /* @var Relation $query */
                        $query->whereHas('parent', function($query) use ($childdata) {
                            /* @var Relation $query */
                            $query->whereHas('parent', function($query) use ($childdata) {
                                /* @var Relation $query */
                                $query->where('typecd', $childdata);
                            });
                        });
                    });
                }
            }

            $searchName = trim(strtolower($req->get('name')));
            if($req->has('informationid')) {
                $informationid = $req->get('informationid');
                $query->whereHas('all_information', function($query) use ($informationid, $searchName) {
                    /* @var Relation $query */
                    $query->where('typeid', $informationid)
                        ->where(DB::raw('TRIM(LOWER(typevalue))'), 'like', "%$searchName%");
                });
            }

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $bpid = $req->input('bpid');
            $isCreateNew = $req->input('iscreatenew');
            $isAddAll = $req->input('isaddall');

            if($isCreateNew == 'true') {
                if(!empty($req->get('typecd')))
                    TypesFinder::find()->check($req->input('typecd'));

                $type = $this->types->create($req->only($this->types->getFillable()));

                $this->bptype->create([
                    'bpid' => $bpid,
                    'typeid' => $req->input('typeid'),
                    'typevalue' => $type->typeid,
                    'typeseq' => $req->input('typeseq'),
                    'createdby' => $req->input('createdby'),
                    'updatedby' => $req->input('updatedby'),
                    'isactive' => true,
                ]);
            }

            else if($req->has('iscreatenew') && $req->has('isaddall') && $isCreateNew == 'false' && $isAddAll == 'true') {
                $types = $this->types->where('masterid', $req->input('typeid'))
                    ->get();

                foreach($types as $type) {
                    $this->bptype->create([
                        'bpid' => $bpid,
                        'typeid' => $req->input('typeid'),
                        'typevalue' => $type->typeid,
                        'typeseq' => $type->typeseq,
                        'createdby' => $req->input('createdby'),
                        'updatedby' => $req->input('updatedby'),
                        'isactive' => true,
                    ]);
                }
            }

            else if($req->has('iscreatenew') && $req->has('isaddall') && $isCreateNew == 'false' && $isAddAll == 'false') {
                $this->bptype->create($req->all());
            }

            /**
             * Aksi dari menu data master
             * */
            if($req->has('parentcd') || $req->has('parentid')) {
                $types = TypesFinder::find()->byCode([
                    strtoupper($req->get('parentcd')),
                    DBTypes::mappingTypeDepartment,
                ]);
                $parent = $types->get(strtoupper($req->get('parentcd')));
                $parentid = $req->has('parentid') ? $req->get('parentid') : $parent->getId();

                if(!empty($req->get('typecd')))
                    TypesFinder::find()->check($req->input('typecd'));

                /* @var TypeCollection $type */
                $type = TypesFinder::find()->search($req->get('typename'));

                if(is_null($type->getId())) {
                    $inserts = collect($req->only($this->types->getFillable()))
                        ->put('masterid', $parentid);

                    $type = new TypeCollection($this->types->create($inserts->toArray()));
                }

                $bptypes = collect($req->only($this->bptype->getFillable()))
                    ->put('typevalue', $type->getId())
                    ->put('typeid', $parentid)
                    ->put('typeremark', $type->getDesc());

                $this->bptype->create($bptypes->toArray());

                if($req->has('deptid') && !empty($req->get('deptid'))) {
                    $this->mappingtype->create([
                        'mappingtypeid' => $types->get(DBTypes::mappingTypeDepartment)->getId(),
                        'mappingkeyid' => $req->get('deptid'),
                        'mappingparentid' => $parentid,
                        'mappingvalueid' => $type->getId(),
                        'createdby' => $req->get('createdby'),
                        'updatedby' => $req->get('updatedby'),
                    ]);
                }
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->bptype->withJoin($this->bptype->defaultSelects)
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {

            DB::beginTransaction();

            $row = $this->bptype->find($id, ['bptypeid', 'typevalue']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $bptypes = collect($req->only($this->bptype->getFillable()))
                ->except('createdby');

            $row->update($bptypes->toArray());

            if($req->has('parentcd')) {
                $type = $this->types->find($row->typevalue, ['typeid']);

                if(is_null($type))
                    throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

                $updates = collect($req->only($this->types->getFillable()))
                    ->except('createdby');

                $type->update($updates->toArray());
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy(Request $req, $id)
    {
        try {

            DB::beginTransaction();

            $row = $this->bptype->find($id, ['bptypeid', 'typevalue']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            if($req->has('deptid') && !empty($req->get('deptid'))) {
                $this->mappingtype->whereHas('mappingtype', function($query) {
                    /* @var Relation $query */
                    $query->where('typecd', DBTypes::mappingTypeDepartment);
                })->where('deptid', $req->get('deptid'))->where('mappingvalueid', $row->typevalue)
                ->delete();
            }

//            if($req->has('parentcd')) {
//                $type = $this->types->find($row->typevalue, ['typeid']);
//
//                if(is_null($type))
//                    throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);
//
//                $type->delete();
//            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}

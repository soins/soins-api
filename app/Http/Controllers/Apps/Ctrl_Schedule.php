<?php


namespace App\Http\Controllers\Apps;


use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Schedule\Schedule;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketAsset;
use App\Models\Tickets\TicketProgress;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class Ctrl_Schedule extends Controller
{
    public function __construct()
    {
        $this->schedule = new Schedule();
        $this->ticket = new Ticket();
        $this->ticketasset = new TicketAsset();
        $this->ticketprogress = new TicketProgress();
        $this->typeFinder = new TypesFinder();
    }

    public function select(Request $request) {
        try {
            $data = $this->schedule->withJoin()
                ->addSelect("scheduledatefrom", "scheduledateto", "actualdatefrom", "actualdateto", "scheduletitle", "refid")
                ->where("assigntoid", $request->userid)
                ->with([
                    'ticket' => function($query) {
                        Ticket::foreignSelect($query)
                            ->addSelect('ticketid');
                    }
                ])
                ->orderBy("scheduledatefrom", "DESC")
                ->orderBy("scheduleid", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function saveEndSchedule(Request $request) {
        DB::beginTransaction();
        try {
            $statusticket = TypesFinder::find()->byCode($request->typecd);

            $ticket = $this->ticket->find($request->ticketid);
            $ticket->update([
                "statusid" => $statusticket->get()->getId(),
                'updatedby' => $request->userid,
            ]);

            $statuschedule = TypesFinder::find()->byCode(DBTypes::statusScheduleFinished);

            if ($request->scheduleid == "0") {
                $schedule = $this->schedule->find($request->scheduleid);
                $schedule->update([
                    "statusid" => $statuschedule->get()->getId(),
                    "actualdateto" => 'now',
                    'updatedby' => $request->userid,
                ]);
            } else {
                $schedule = $this->schedule->withJoin()
                    ->where('refid', $request->ticketid)
                    ->whereHas('subject', function($query) {
                        /* @var \Illuminate\Database\Eloquent\Builder $query */
                        $query->where('typecd', DBTypes::typescheduleservice);
                    })
                    ->first();
                $schedule->update([
                    "statusid" => $statuschedule->get()->getId(),
                    "actualdatefrom" => 'now',
                    "actualdateto" => 'now',
                    'updatedby' => $request->userid,
                ]);
            }

            $progresstype = TypesFinder::find()->byCode(DBTypes::progressTicketComplete);

            $ticketprogress = $this->ticketprogress->create([
                'ticketid' => $request->ticketid,
                'description' => json_encode(array(
                    ['insert' => "Ticket has been completed\n"]
                )),
                'progresstypeid' => $progresstype->get()->getId(),
                'isactive' => true,
                'createdby' => $request->userid,
                'updatedby' => $request->userid,
            ]);

            DB::commit();
            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch(Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function selectToday(Request $request) {
        try {
            $data = $this->schedule->withJoin()
                ->addSelect("scheduledatefrom", "scheduledateto", "actualdatefrom", "actualdateto", "scheduletitle", "refid")
                ->where("assigntoid", $request->userid)
                ->where(DB::raw("TO_CHAR(scheduledatefrom, 'YYYY-mm-dd')"), "<=", date('Y-m-d'))
                ->where(DB::raw("TO_CHAR(scheduledateto, 'YYYY-mm-dd')"), ">=", date('Y-m-d'))
                ->with([
                    'ticket' => function($query) {
                        Ticket::foreignSelect($query)
                            ->addSelect('ticketid');
                    }
                ])
                ->orderBy("scheduledatefrom", "DESC")
                ->orderBy("scheduleid", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function selectComingNext(Request $request) {
        try {
            $data = $this->schedule->withJoin()
                ->addSelect("scheduledatefrom", "scheduledateto", "actualdatefrom", "actualdateto", "scheduletitle", "refid")
                ->where("assigntoid", $request->userid)
                ->where(DB::raw("TO_CHAR(scheduledatefrom, 'YYYY-mm-dd')"), ">", date('Y-m-d'))
                ->with([
                    'ticket' => function($query) {
                        Ticket::foreignSelect($query)
                            ->addSelect('ticketid');
                    }
                ])
                ->orderBy("scheduledatefrom", "DESC")
                ->orderBy("scheduleid", "DESC")
                ->limit(2)
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function selectLate(Request $request) {
        try {
            $data = $this->schedule->withJoin()
                ->addSelect("scheduledatefrom", "scheduledateto", "actualdatefrom", "actualdateto", "scheduletitle", "refid")
                ->where("assigntoid", $request->userid)
                ->where(DB::raw("TO_CHAR(scheduledateto, 'YYYY-mm-dd')"), "<", date('Y-m-d'))
                ->whereHas("status", function($query) {
                    /* @var Relation $query */
                    $query->where('typecd', DBTypes::statusScheduleCreate);
                })
                ->with([
                    'ticket' => function($query) {
                        Ticket::foreignSelect($query)
                            ->addSelect('ticketid');
                    }
                ])
                ->orderBy("scheduledatefrom", "DESC")
                ->orderBy("scheduleid", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

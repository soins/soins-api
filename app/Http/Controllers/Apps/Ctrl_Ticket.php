<?php

namespace App\Http\Controllers\Apps;

use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\FileUpload;
use App\Helpers\Helpers;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\Files;
use App\Models\Schedule\Schedule;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketAsset;
use App\Models\Tickets\TicketDetail;
use App\Models\Tickets\TicketProgress;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class Ctrl_Ticket extends Controller
{
    public function __construct()
    {
        $this->ticket = new Ticket();
        $this->ticketdetail = new TicketDetail();
        $this->ticketasset = new TicketAsset();
        $this->ticketprogress = new TicketProgress();
        $this->typeFinder = new TypesFinder();
        $this->schedule = new Schedule();
    }

    public function select(Request $request) {
        try {
            $data = $this->ticket->withJoin($this->ticket->defaultSelects)
                ->addSelect('createdby', 'createddate')
                ->where('ticketid', $request->ticketid)
                ->with([
                    'progresses' => function($query) {
                        TicketProgress::foreignSelect($query)->addSelect("createddate")
                        ->with([
                            'files' => function($query) {
                                Files::foreignSelect($query)->addSelect(array(
                                    DB::raw(Helpers::imageUrlField()),
                                    DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_THUMBNAIL_POTRAIT)),
                                    DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                                ))->whereHas('transtype', function($query) {
                                    /* @var Relation $query */
                                    $query->where('typecd', DBTypes::filesTicketProgress);
                                });
                            }
                        ])->orderBy("createddate", "DESC");
                    },
                    'ticketasset' => function($query) {
                        TicketAsset::foreignSelect($query);
                    },
                    'files' => function($query) {
                        Files::foreignSelect($query)->addSelect(array(
                            DB::raw(Helpers::imageUrlField()),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_THUMBNAIL_POTRAIT)),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                        ))->whereHas('transtype', function($query) {
                            /* @var Relation $query */
                            $query->where('typecd', DBTypes::filesTicket);
                        });
                    }
                ])
                ->first();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch(Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function selectTicketAsset(Request $request) {
        try {
            $data = $this->ticketasset->withJoin()
                ->addSelect($this->ticketasset->defaultSelects)
                ->where("ticketid", $request->ticketid)
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch(Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function saveTicketProgress(Request $request) {
        DB::beginTransaction();
        try {
            $progresstype = TypesFinder::find()->byCode(DBTypes::progressTicketOnProgress);

            $ticket = $this->ticketprogress->create([
                'ticketid' => $request->ticketid,
                'assetid' => $request->assetid,
                'description' => $request->description,
                'progresstypeid' => $progresstype->get()->getId(),
                'isactive' => true,
                'createdby' => $request->userid,
                'updatedby' => $request->userid,
            ]);

            $fileUpload = new FileUpload($request->userid, DBTypes::filesTicketProgress, $ticket->progressid);
            try {
                $fileUpload->uploadFileTo($request->file('files'), sprintf('app/ticketprogress'));
                $fileUpload->save(function ($file, $index) {
                    /* @var UploadedFile $file */
                    return (object)array(
                        'extension' => 'jpg',
                        'filename' => sprintf("%s-%03d.jpg", time(), $index),
                    );
                });
            } catch (Exception $e) {
                $fileUpload->rollback();
                throw new Exception($e->getMessage(), $e->getCode());
            }

            DB::commit();
            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch(Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function saveTicketProgressConfirm(Request $request) {
        DB::beginTransaction();
        try {
            $statusticket = TypesFinder::find()->byCode(DBTypes::statusTicketOpen);

            $ticket = $this->ticket->find($request->ticketid);
            $ticket->update([
                "statusid" => $statusticket->get()->getId(),
                'updatedby' => $request->userid,
            ]);

            $progresstype = TypesFinder::find()->byCode(DBTypes::progressTicketConfirmedTech);

            $ticketprogress = $this->ticketprogress->create([
                'ticketid' => $request->ticketid,
                'description' => json_encode(array(
                    ['insert' => "Ticket has been confirmed by technician\n"]
                )),
                'progresstypeid' => $progresstype->get()->getId(),
                'isactive' => true,
                'createdby' => $request->userid,
                'updatedby' => $request->userid,
            ]);

            $statuschedule = TypesFinder::find()->byCode(DBTypes::statusScheduleOnProgress);

            if ($request->scheduleid == "0") {
                $schedule = $this->schedule->find($request->scheduleid);
                $schedule->update([
                    "statusid" => $statuschedule->get()->getId(),
                    "actualdatefrom" => 'now',
                    "actualdateto" => 'now',
                    'updatedby' => $request->userid,
                ]);
            } else {
                $schedule = $this->schedule->withJoin()
                    ->where('refid', $request->ticketid)
                    ->whereHas('subject', function($query) {
                        /* @var \Illuminate\Database\Eloquent\Builder $query */
                        $query->where('typecd', DBTypes::typescheduleservice);
                    })
                    ->first();
                $schedule->update([
                    "statusid" => $statuschedule->get()->getId(),
                    "actualdatefrom" => 'now',
                    "actualdateto" => 'now',
                    'updatedby' => $request->userid,
                ]);
            }

            DB::commit();
            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch(Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function selectTicketDetail(Request $request) {
        try {
            $data = $this->ticketdetail->withJoin()
                ->addSelect($this->ticketdetail->defaultSelects)
                ->addSelect("createdby", "createddate")
                ->where("ticketid", $request->ticketid)
                ->with([
                    'files' => function($query) {
                        Files::foreignSelect($query)->addSelect(array(
                            DB::raw(Helpers::imageUrlField()),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_THUMBNAIL_POTRAIT)),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                        ))->whereHas('transtype', function($query) {
                            /* @var Relation $query */
                            $query->where('typecd', DBTypes::filesTicketReply);
                        });
                    }
                ])
                ->orderBy("createddate", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch(Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function saveTicketDetail(Request $request) {
        DB::beginTransaction();
        try {

            $ticketdt = $this->ticketdetail->create([
                'ticketid' => $request->ticketid,
                'statusid' => 0,
                'description' => $request->description,
                'createdby' => $request->userid,
                'updatedby' => $request->userid
            ]);

            if($request->hasFile('files')) {
                $fileUpload = new FileUpload($request->userid, DBTypes::filesTicketReply, $ticketdt->ticketdtid);
                try {
                    $fileUpload->uploadFileTo($request->file('files'), sprintf('app/ticketdetail'));
                    $fileUpload->save(function ($file, $index) {
                        /* @var UploadedFile $file */
                        return (object)array(
                            'extension' => 'jpg',
                            'filename' => sprintf("%s-%03d.jpg", time(), $index),
                        );
                    });
                } catch (Exception $e) {
                    $fileUpload->rollback();
                    throw new Exception($e->getMessage(), $e->getCode());
                }
            }

//            $notificaiton = new PushNotification($request->userid);
//            $notificaiton->setNotification("Tiket Baru", "Terdapat tiket baru dengan kode " . $ticket->ticketcd);
//            $notificaiton->setNotificationType(DBTypes::notifTicket, $ticket->ticketid);
//
//            $userids = $this->userdetail->withJoin()->addSelect("userid")
//                ->join("mstype", "usertypeid", "=", "mstype.typeid")
//                ->where("mstype.typecd", DBTypes::useradmin)
//                ->get();
//
//            $arrayuser = array();
//            foreach ($userids as $x) {
//                $arrayuser[] = $x->userid;
//            }
//
//            $notificaiton->pushTo($arrayuser, true);

            DB::commit();
            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch(Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function selectByAssign(Request $request) {
        try {
            $data = $this->ticket->withJoin($this->ticket->defaultSelects)
                ->addSelect('createdby', 'createddate')
                ->whereHas('schedule', function($query) use ($request) {
                    $query->where("trschedule.assigntoid", $request->userid);
                })
                ->with([
                    'progresses' => function($query) {
                        TicketProgress::foreignSelect($query)->addSelect("createddate")
                            ->with([
                                'files' => function($query) {
                                    Files::foreignSelect($query)->addSelect(array(
                                        DB::raw(Helpers::imageUrlField()),
                                        DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_THUMBNAIL_POTRAIT)),
                                        DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                                    ))->whereHas('transtype', function($query) {
                                        /* @var Relation $query */
                                        $query->where('typecd', DBTypes::filesTicketProgress);
                                    });
                                }
                            ])->orderBy("createddate", "DESC");
                    },
                    'ticketasset' => function($query) {
                        TicketAsset::foreignSelect($query);
                    },
                    'files' => function($query) {
                        Files::foreignSelect($query)->addSelect(array(
                            DB::raw(Helpers::imageUrlField()),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_THUMBNAIL_POTRAIT)),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                        ))->whereHas('transtype', function($query) {
                            /* @var Relation $query */
                            $query->where('typecd', DBTypes::filesTicket);
                        });
                    }
                ])
                ->orderBy("createddate", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch(Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function selectByDept(Request $request) {
        try {
            $data = $this->ticket->withJoin($this->ticket->defaultSelects)
                ->addSelect('createdby', 'createddate')
                ->where("depttoid", $request->depttoid)
                ->whereHas('schedule', function($query) use ($request) {
                    $query->where("trschedule.assigntoid", "!=", $request->userid);
                })
                ->with([
                    'progresses' => function($query) {
                        TicketProgress::foreignSelect($query)->addSelect("createddate")
                            ->with([
                                'files' => function($query) {
                                    Files::foreignSelect($query)->addSelect(array(
                                        DB::raw(Helpers::imageUrlField()),
                                        DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_THUMBNAIL_POTRAIT)),
                                        DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                                    ))->whereHas('transtype', function($query) {
                                        /* @var Relation $query */
                                        $query->where('typecd', DBTypes::filesTicketProgress);
                                    });
                                }
                            ])->orderBy("createddate", "DESC");
                    },
                    'ticketasset' => function($query) {
                        TicketAsset::foreignSelect($query);
                    },
                    'files' => function($query) {
                        Files::foreignSelect($query)->addSelect(array(
                            DB::raw(Helpers::imageUrlField()),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_THUMBNAIL_POTRAIT)),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                        ))->whereHas('transtype', function($query) {
                            /* @var Relation $query */
                            $query->where('typecd', DBTypes::filesTicket);
                        });
                    }
                ])
                ->orderBy("createddate", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch(Exception $e) {
            return $this->jsonError($e);
        }
    }

}

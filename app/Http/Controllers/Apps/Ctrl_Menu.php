<?php


namespace App\Http\Controllers\Apps;

use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\Customer;
use App\Models\Masters\Menu;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Exception;

class Ctrl_Menu  extends Controller
{
    public function __construct()
    {
        $this->menu = new Menu();
    }

    public function select(Request $request) {
        try {
            $data = $this->menu->getAccessMenu($request->usertypeid, $request->bpid, DBTypes::menuApps);

            return $this->jsonSuccess(DBMessage::SUCCESS, $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

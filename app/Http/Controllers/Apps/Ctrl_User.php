<?php


namespace App\Http\Controllers\Apps;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\FileUpload;
use App\Helpers\Helpers;
use App\Helpers\Types\TypesFinder;
use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Customer;
use App\Models\Masters\Files;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Testing\Fluent\Concerns\Has;

class Ctrl_User extends Controller
{
    public function __construct()
    {
        $this->user = new User();
        $this->customer = new Customer();
        $this->userdetail = new UserDetail();
        $this->file = new Files();
    }

    public function register(Request $request) {
        DB::beginTransaction();
        try {
            $userexist = $this->user->withJoin()
                ->where("username", $request->username)
                ->first();

            if (!is_null($userexist))
                return  $this->jsonWarning(DBMessage::REGISTER_FAILED);

            $user = $this->user
                ->create([
                    'userfullname' => $request->name,
                    'username' => $request->username,
                    'userpassword' => Hash::make($request->password),
                    'useremail' => $request->email,
                    'userphone' => $request->phone,
                    'isactive' => TRUE
                ]);

            $customertype = TypesFinder::find()->byCode(DBTypes::custtypeindividu);

            $customer = $this->customer
                ->create([
                    'customerprefix' => $request->prefix,
                    'customername' => $request->name,
                    'customerphone' => $request->phone,
                    'customertypeid' => $customertype->get()->getId(),
                    'createdby' => $user->userid,
                    'updatedby' => $user->userid,
                    'isactive' => TRUE
                ]);


            $usertype = TypesFinder::find()->byCode(DBTypes::usertypepersonal);

            $this->userdetail
                ->create([
                    'userid' => $user->userid,
                    'usertypeid' => $usertype->get()->getId(),
                    'relationid' => $customer->customerid,
                    'createdby' => $user->userid,
                    'updatedby' => $user->userid,
                    'isactive' => TRUE
                ]);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function registerTechnician(Request $request) {
        DB::beginTransaction();
        try {
            $userexist = $this->user->withJoin()
                ->where("username", $request->username)
                ->first();

            if (!is_null($userexist))
                return  $this->jsonWarning(DBMessage::REGISTER_FAILED);

            $user = $this->user
                ->create([
                    'userfullname' => $request->name,
                    'username' => $request->username,
                    'userpassword' => Hash::make($request->password),
                    'useremail' => $request->email,
                    'userphone' => $request->phone,
                    'isactive' => TRUE
                ]);

            $usertype = TypesFinder::find()->byCode(DBTypes::roleTechnition);

            $this->userdetail
                ->create([
                    'userid' => $user->userid,
                    'usertypeid' => $usertype->get()->getId(),
                    'createdby' => $user->userid,
                    'updatedby' => $user->userid,
                    'isactive' => TRUE
                ]);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    function select(Request $request) {
        try {
            $data = $this->user->withJoin($this->user->defaultSelects)
                ->where("userid", $request->userid)
                ->with([
                    'file' => function($query) {
                        Files::foreignSelect($query)->addSelect(array(
                            DB::raw(Helpers::imageUrlField()),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_THUMBNAIL, Files::IMAGE_SIZE_MEDIUM_THUMNAIL)),
                            DB::raw(Helpers::imageUrlField(Files::IMAGE_ALIAS_LARGE, Files::IMAGE_SIZE_LARGE)),
                        ))->addSelect('refid');
                    },
                    'userdetails' => function($query) {
                        UserDetail::foreignSelect($query)
                            ->with([
                                'businesspartner' => function($query) {
                                    BusinessPartner::foreignSelect($query);
                                },
                                'customer' => function($query) {
                                    Customer::foreignSelect($query);
                                }
                            ])
                            ->addSelect('userid', 'bpid', 'relationid');
                    },
                ])
                ->first();

            return $this->jsonSuccess(DBMessage::SUCCESS, $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function edit(Request $request) {
        DB::beginTransaction();
        try {
            $userexist = $this->user->withJoin($this->user->defaultSelects)
                ->where("userid", $request->userid)
                ->first();

            if (!Hash::check($request->confirmpassword, $userexist->userpassword))
                return $this->jsonWarning(DBMessage::WRONG_PASSWORD);

            $userexist->update([
                'userfullname' => $request->name,
                'useremail' => $request->email,
                'userphone' => $request->phone,
                'updatedby' => $request->userid,
            ]);

            $customerexist = $this->customer->find($request->customerid);

            if ($customerexist != null) {
                $customerexist->update([
                    'customername' => $request->name,
                    'customerphone' => $request->phone,
                    'updatedby' => $request->userid,
                ]);
            }

            if ($request->file("files") != null) {
                $typefile = TypesFinder::find()->byCode(DBTypes::filesProfile);

                $fileexist = $this->file->withjoin()
                    ->where("transtypeid", $typefile->get()->getId())
                    ->where("refid", $request->userid)
                    ->first();

                if(!is_null($fileexist))
                    $fileexist->delete();

                $fileUpload = new FileUpload($request->userid, DBTypes::filesProfile, $request->userid);
                $fileUpload->uploadFileTo($request->file("files"), sprintf('app/user/profile'));
                $fileUpload->save(function ($file) {
                    /* @var UploadedFile $file */
                    return (object)array(
                        'extension' => 'jpg',
                        'filename' => sprintf("%s.jpg", time()),
                    );
                });
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT, $userexist);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function changePassword(Request $request) {
        DB::beginTransaction();
        try {
            $userexist = $this->user->withJoin($this->user->defaultSelects)
                ->where("userid", $request->userid)
                ->first();

            if (!Hash::check($request->oldpassword, $userexist->userpassword))
                return $this->jsonWarning(DBMessage::WRONG_PASSWORD);

            $userexist->update([
                'userpassword' => Hash::make($request->newpassword),
                'updatedby' => $request->userid,
            ]);

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT, $userexist);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e);
        }
    }

    public function updateFCMToken(Request $request)
    {
        try {
            $row = $this->user->find($request->userid);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update([
                'fcmtoken' => $request->fcmtoken,
                'updatedby' => $request->userid,
            ]);

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT, $row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

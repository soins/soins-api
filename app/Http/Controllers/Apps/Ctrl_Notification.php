<?php


namespace App\Http\Controllers\Apps;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Helpers\Notifications\PushNotification;
use App\Http\Controllers\Controller;
use App\Models\Masters\Notification;
use App\Models\Masters\User;
use Exception;
use Illuminate\Http\Request;

class Ctrl_Notification extends Controller
{
    public function __construct()
    {
        $this->notification = new Notification();
        $this->user = new User();
    }

    public function select(Request $req) {
        try {
            $data = $this->notification
                ->withJoin()
                ->addSelect("notificationtitle", "description" , "createddate", "isviewed", 'refid')
                ->where("notificationtoid", $req->userid)
                ->orderBy("createddate", "DESC")
                ->orderBy("notificationid", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS LOAD", $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function selectCount(Request $req) {
        try {
            $data = $this->notification
                ->where("notificationtoid", $req->userid)
                ->where("isviewed", FALSE)
                ->count();

            return $this->jsonCount("SUCCESS", $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function updateRead(Request $req) {
        try {
            $row = $this->notification->find($req->notificationid);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update([
                'isviewed' => TRUE,
                'updatedby' => $req->userid,
            ]);

            $data = $this->notification
                ->withJoin()
                ->addSelect("notificationtitle", "description" ,"createddate", "isviewed")
                ->where("notificationtoid", $req->userid)
                ->orderBy("createddate", "DESC")
                ->orderBy("notificationid", "DESC")
                ->get();

            return $this->jsonSuccess("SUCCESS", $data);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function notificationUpdate(Request $request) {
        try {
            $notificaiton = new PushNotification(1);
            $notificaiton->setNotification("Update Aplikasi Mosewa","Terdapat versi baru aplikasi Mosewa, yaitu versi 1.0.14. Silahkan update aplikasi melalui playstore");
            $notificaiton->setNotificationType(DBTypes::NOTIF_UPDATE, 0);

            $userids = $this->user->withJoin("userid")
                ->get();

            $arrayuser = array();
            foreach ($userids as $x) {
                $arrayuser[] = $x->userid;
            }

            $notificaiton->pushTo($arrayuser, true);

            return $this->jsonSuccess("SUCCESS");
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}

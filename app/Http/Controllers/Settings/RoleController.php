<?php


namespace App\Http\Controllers\Settings;


use App\Constants\DBMessage;
use App\Constants\DBTypes;
use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartnerMenu;
use App\Models\Masters\Menu;
use App\Models\Masters\Role;
use App\Models\Masters\Types;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends  Controller
{

    /* @var Role|Relation */
    protected $role;

    /* @var BusinessPartnerMenu|Relation */
    protected $bpmenu;

    /* @var Types|Relation */
    protected $types;

    public function __construct()
    {
        $this->role = new Role();
        $this->types = new Types();
        $this->bpmenu = new BusinessPartnerMenu();
    }

    public function access(Request $req)
    {
        try {

            $bpid = $req->input('bpid');
            $usertypeid = $req->input('usertypeid');
            $menutypeid = $req->input('menutypeid');

            $accesses = $this->types->withJoin($this->types->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Types::foreignSelect($query);
                    }
                ])
                ->whereHas('parent', function($query) {
                    /* @var Relation $query */
                    $query->where('typecd', DBTypes::menuAccess);
                })
                ->orderBy('typeseq')
                ->get();

            $bpmenus = $this->bpmenu->withJoin($this->bpmenu->defaultSelects)
                ->with([
                    'menu' => function($query) {
                        Menu::foreignSelect($query)
                            ->addSelect('masterid');
                    }
                ])
                ->where('bpid', $bpid)
                ->whereHas('menu', function($query) use ($menutypeid) {
                    /* @var Relation $query */
                    $query->where('menutypeid', $menutypeid);
                })
                ->orderBy('menuid')
                ->get();

            $roles = $this->role->withJoin($this->role->defaultSelects)
                ->where('bpid', $bpid)
                ->where('usertypeid', $usertypeid)
                ->addSelect('menuid')
                ->orderBy('menuid')
                ->get();

            $_roles = array();
            foreach($roles as $role) {
                if(!array_key_exists($role->menuid, $_roles))
                    $_roles[$role->menuid] = array();

                $_roles[$role->menuid][] = $role;
            }

            $previleges = array();
            foreach($bpmenus as $bpmenu) {
                $previlege = (object) array(
                    'menuid' => $bpmenu->menu->menuid,
                    'menunm' => $bpmenu->menu->menunm,
                    'masterid' => $bpmenu->menu->masterid,
                    'seq' => $bpmenu->menu->seq,
                    'accesses' => array()
                );

                foreach($accesses as $access) {
                    $maccess = (object) array(
                        'checked' => false,
                        'roleid' => 0,
                        'menuid' => $bpmenu->menu->menuid,
                        'accessid' => $access->accessid,
                        'access' => $access,
                    );

                    if(isset($_roles[$bpmenu->menu->menuid])) {
                        foreach ($_roles[$bpmenu->menu->menuid] as $role) {
                            if($access->typeid == $role->access->typeid) {
                                $maccess->checked = true;
                                $maccess->roleid = $role->roleid;
                            }
                        }
                    }

                    $previlege->accesses[] = $maccess;
                }

                $previleges[] = $previlege;
            }

            return $this->jsonData(array(
                'previleges' => $previleges,
                'accesses' => $accesses,
            ));
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'access');
        }
    }

    public function save(Request $req)
    {
        try {

            $userid = $req->input('userid');
            $bpid = $req->input('bpid');
            $usertypeid = $req->input('usertypeid');

            DB::beginTransaction();

            $menus = json_decode($req->input('menus'));
            foreach($menus as $menu) {
                foreach($menu->accesses as $access) {
                    if($access->roleid == 0 && $access->checked == 'true') {
                        $this->role->create([
                            'usertypeid' => $usertypeid,
                            'menuid' => $menu->menuid,
                            'bpid' => $bpid,
                            'accessid' => $access->access->typeid,
                            'createdby' => $userid,
                            'updatedby' => $userid,
                            'isactive' => true
                        ]);
                    }

                    else if($access->roleid != 0 && $access->checked == 'false') {
                        $row = $this->role->find($access->roleid, ['roleid']);
                        if(!is_null($row))
                            $row->delete();
                    }
                }
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'save');
        }
    }
}

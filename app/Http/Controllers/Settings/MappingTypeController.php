<?php

namespace App\Http\Controllers\Settings;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Settings\MappingType;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MappingTypeController extends Controller
{

    /* @var MappingType|Relation */
    protected $mappingtype;

    public function __construct()
    {
        $this->mappingtype = new MappingType();
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->mappingtype->withJoin($this->mappingtype->defaultSelects)
                ->where('bpid', $req->get('bpid'));

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function find(Request $req)
    {
        try {
            $value = $req->get('value');
            $field = $req->get('field');

            DB::enableQueryLog();
            $row = $this->mappingtype->whereHas('mappingparent', function($query) use ($field, $value) {
                /* @var Relation $query */
                $query->where(DB::raw("TRIM(LOWER($field))"), trim(strtolower($value)));
            })->first();
            print_r(DB::getQueryLog());

            if(is_null($row))
                throw new \Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
}

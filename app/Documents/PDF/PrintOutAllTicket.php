<?php


namespace App\Documents\PDF;


use App\Helpers\Collections\TicketCollection;
use Illuminate\Database\Eloquent\Collection;

class PrintOutAllTicket extends DocumentBuilder
{

    /* @var TicketCollection[] */
    protected $datas;

    public function __construct()
    {
        parent::__construct('L', 'mm', 'A4');
    }

    /**
     * @param Collection $datas
     * */
    public function setDatas($datas)
    {
        $this->datas = $datas->map(function($data) {
            return new TicketCollection($data);
        })->toArray();
    }

    public function header()
    {
        $PoY = $this->GetY();
        $this->SetXY($this->GetX(), $this->GetY());
        $this->Image(storage_path('app/images/logo.JPG'), null, null, 60, 10);


        $this->SetY($PoY + 3);
        $this->SetFont($this->fontFamily, 'B', $this->fontSize + 5);
        $this->MultiCell($this->paperWidth, $this->wordHeight, "Laporan Semua Data Tiket", 0, 'C');

        $this->Ln(10);
    }

    public function build()
    {
        $this->AliasNbPages();
        $this->AddPage();

        $CellCode = 0.1 * $this->paperWidth;
        $CellCreated = 0.1 * $this->paperWidth;
        $CellPelapor = 0.1 * $this->paperWidth;
        $CellCompany = 0.1 * $this->paperWidth;
        $CellSubject = 0.15 * $this->paperWidth;
        $CellDescription = 0.15 * $this->paperWidth;
        $CellPriority = 0.1 * $this->paperWidth;
        $CellStatus = 0.1 * $this->paperWidth;
        $CellUpdated = 0.1 * $this->paperWidth;

        $this->SetWidths(array($CellCode, $CellCreated, $CellUpdated, $CellPelapor, $CellCompany, $CellSubject, $CellDescription, $CellPriority, $CellStatus));
        $this->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
        $this->SetFont($this->fontFamily, 'B', $this->fontSize);

        $this->Row(array(
            "Kode",
            "Tanggal Dibuat",
            "Terakhir Diupdate",
            "Pelapor",
            "Perusahaan",
            "Subject",
            "Deskripsi",
            "Priority",
            "Status",
        ));

        $this->SetAligns(array('L', 'C', 'C', 'L', 'L', 'L', 'L', 'C', 'C'));
        $this->SetFont($this->fontFamily, '', $this->fontSize);

        foreach($this->datas as $data) {
            $this->Row(array(
                $data->getCode(),
                $data->getCreatedDate(),
                $data->getUpdatedDate(),
                $data->getCreated()->getFullname(),
                $data->getCustomer()->getFullname(),
                $data->getSubject(),
                $data->getDescString(),
                $data->getPriority()->getName(),
                $data->getStatus()->getName(),
            ));
        }
    }

    public function getFilename()
    {
        return 'print-out-all.pdf';
    }

    public function getDirectories($withFilename = true)
    {
        if(!is_dir(storage_path('app')))
            mkdir(storage_path('app'));

        if(!is_dir(storage_path('app/documents')))
            mkdir(storage_path('app/documents'));

        if(!is_dir(storage_path('app/documents/tickets')))
            mkdir(storage_path('app/documents/tickets'));

        return storage_path($withFilename ? 'app/documents/tickets/' . $this->getFilename() : 'app/documents/tickets');
    }

    public function response()
    {
        echo $this->Output();exit;
    }

    public function save()
    {
        $this->Output('F', $this->getDirectories());
    }
}

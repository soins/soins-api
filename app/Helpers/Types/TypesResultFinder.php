<?php


namespace App\Helpers\Types;

use App\Helpers\Collections\TypeCollection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;

class TypesResultFinder
{

    /* @var Collection */
    protected $codes;

    /* @var Collection|TypeCollection[] */
    protected $datas;

    /**
     * @param EloquentCollection|object $datas
     * */
    public function __construct($codes, $datas)
    {
        $this->codes = collect($codes);
        $this->datas = collect($datas->all());

        $this->datas = $this->datas->map(function($data) {
            return new TypeCollection($data);
        });
    }

    public function valid()
    {
        return $this->datas->count() > 0;
    }

    public function hasError()
    {
        return $this->datas->count() == 0;
    }

    /**
     * @param string|null $code
     * @param string $field
     * @return TypeCollection|bool
     * */
    public function get($code = null, $field = 'typecd')
    {
        if(is_null($code))
            $code = $this->codes->first();

        return $this->datas->filter(function($data) use ($code, $field) {
            return $data->$field == $code;
        })->first();
    }

    /**
     * @param string $parentCode
     * @return TypeCollection[]|mixed
     * */
    public function child($parentCode = null)
    {
        if(is_null($parentCode))
            $parentCode = $this->codes->first();

        return $this->datas->filter(function($data) use ($parentCode) {
            /* @var TypeCollection $data */
            return !is_null($data->parent()) && $data->parent()->getCode() == $parentCode;
        });
    }
}

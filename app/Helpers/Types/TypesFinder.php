<?php


namespace App\Helpers\Types;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Helpers\Collections\TypeCollection;
use App\Models\Masters\Types;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class TypesFinder
{

    static private $instance;

    static public function find() {
        if(is_null(self::$instance))
            self::$instance = new TypesFinder();

        return self::$instance;
    }

    /* @var Types */
    protected $types;

    public function __construct()
    {
        $this->types = new Types();
    }

    public function byCode($code)
    {
        $codes = is_array($code) ? $code : func_get_args();

        $types = $this->types->withJoin($this->types->defaultSelects)
            ->addSelect('descriptions')
            ->whereIn('typecd', $codes)
            ->get();

        return new TypesResultFinder($codes, $types);
    }

    public function byId($typeid)
    {
        $typeids = is_array($typeid) ? $typeid : func_get_args();

        $types = $this->types->withJoin($this->types->defaultSelects)
            ->addSelect('descriptions')
            ->whereIn('typeid', $typeids)
            ->get();

        return new TypesResultFinder($typeids, $types);
    }

    public function byParentCode($code)
    {
        $codes = is_array($code) ? $code : func_get_args();

        $types = $this->types->withJoin($this->types->defaultSelects)
            ->with([
                'parent' => function($query) {
                    Types::foreignSelect($query);
                }
            ])
            ->addSelect('masterid')
            ->whereHas('parent', function($query) use ($codes) {
                /* @var Relation $query */
                $query->whereIn('typecd', $codes);
            })
            ->get();

        return new TypesResultFinder($codes, $types);
    }

    /**
     * @param string $value
     * @param mixed $id
     * @param string $field
     * @return bool
     * @throws Exception
     * */
    public function check($value, $id = null, $field = TypesField::typecode)
    {
        list($datafield, $namefield) = explode(":", $field);

        $type = $this->types->withJoin()
            ->where($datafield, $value);

        if(!is_null($id))
            $type->where($this->types->getKeyName(), '!=', $id);

        if($type->count() > 0)
            throw new Exception(sprintf(DBMessage::DATA_EXIST, $namefield, $value), DBCode::AUTHORIZED_ERROR);

        return true;
    }

    /**
     * @param string $value
     * @param string $field
     * @return TypeCollection
     * @throws Exception
     * */
    public function search($value, $field = TypesField::typename)
    {
        list($datafield, $namefield) = explode(":", $field);

        $type = $this->types->withJoin()
            ->where(DB::raw("TRIM(LOWER($datafield))"), 'like', trim(strtolower($value)))
            ->first();
        return new TypeCollection($type);
    }
}

<?php

namespace App\Helpers\Users;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Helpers\Collections\UserCollection;
use App\Models\Masters\User;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;

class UsersFinder
{

    private static $instance;

    /**
     * @return UsersFinder
     * */
    static public function init()
    {
        if(is_null(self::$instance))
            self::$instance = new UsersFinder();

        return self::$instance;
    }

    /* @var User|Relation */
    protected $user;

    public function __construct()
    {
        $this->user = new User();
    }

    /**
     * @param string $value
     * @param string $field
     * @return bool
     * @throws Exception
     * */
    public function check($value, $field = UserField::username)
    {
        list($datafield, $namefield) = explode(":", $field);

        $user = $this->user->withJoin()
            ->where($datafield, $value)
            ->get();

        if($user->count() > 0)
            throw new Exception(sprintf(DBMessage::DATA_EXIST, $namefield, $value), DBCode::AUTHORIZED_ERROR);

        return true;
    }

    /**
     * @param int $id
     * @return UserCollection
     * @throws Exception
     * */
    public function byId($id)
    {
        $user = $this->user->withJoin($this->user->defaultSelects)
            ->find($id);

        if(is_null($user))
            throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

        return new UserCollection($user);
    }
}

<?php

namespace App\Helpers\Vendor;

use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Helpers\Collections\VendorCollection;
use App\Models\Masters\Vendor;
use Illuminate\Support\Facades\DB;

class VendorFinder
{

    static private $instance;

    static public function find() {
        if(is_null(self::$instance))
            self::$instance = new VendorFinder();

        return self::$instance;
    }

    /* @var Vendor() */
    protected $vendor;

    public function __construct()
    {
        $this->vendor = new Vendor();
    }

    /**
     * @param string $value
     * @param string $field
     * @return VendorCollection
     * @throws \Exception
     * */
    public function search($value, $field = VendorField::name)
    {
        list($datafield, $namefield) = explode(":", $field);

        $type = $this->vendor->withJoin()
            ->where(DB::raw("TRIM(LOWER($datafield))"), 'like', trim(strtolower($value)))
            ->first();

        return new VendorCollection($type);
    }
}

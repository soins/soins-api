<?php


namespace App\Helpers;


use App\Helpers\Types\TypesFinder;
use App\Models\Masters\Files;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;
use Storage;

/**
 * @property Files|Relation file
 */
class FileUpload
{

    /**
     * @var UploadedFile|UploadedFile[]
     * */
    private $files;

    private $directory;

    private $transtype;

    private $userid;

    private $refid;

    private $tempFile = array();

    private $uploaded = array();

    /**
     * @var TypesFinder
     */
    private $typeFinder;

    public function __construct($userid, $transtype, $refid)
    {
        $this->file = new Files();
        $this->typeFinder = new TypesFinder();

        $this->userid = $userid;
        $this->transtype = $transtype;
        $this->refid = $refid;
    }

    public function uploadFileTo($files, $directory)
    {
        $this->files = is_array($files) ? $files : array($files);
        $this->directory = trim($directory, "\/\\") . "/";

        return $this;
    }

    public function save($config = null)
    {
        $type = TypesFinder::find()->byCode($this->transtype);

        foreach($this->files as $index => $file) {

            $storage = Storage::createLocalDriver(array(
                'driver' => 'local',
                'root' => storage_path(),
            ));

            $manager = new ImageManager(array('drive', 'gd'));
            $image = $manager->make($file->getRealPath());

            if(!is_dir($this->directory))
                $storage->makeDirectory($this->directory);

            $filename = $file->getClientOriginalName();
            if($config != null) {
                $_config = call_user_func_array($config, array($file, $index));

                if (!is_object($config))
                    throw new Exception("Return config file upload must be an object");

                if (!property_exists($_config, 'filename'))
                    throw new Exception("Undefined filename, custom save must returned filename");

                $filename = $_config->filename;

                if(file_exists($this->directory.$filename))
                    throw new Exception(sprintf("Failed to upload file. File %s already exist", $filename));

                $extension = $file->getClientOriginalExtension();
                if (property_exists($_config, 'extension'))
                    $extension = $_config->extension;

                $compression = 50;
                if (property_exists($_config, 'compression'))
                    $compression = $_config->compression;

                $image->save(storage_path($this->directory . $_config->filename), $compression, $extension);
            } else {
                $image->save(storage_path($this->directory . $file->getClientOriginalName()), 50, $file->getClientOriginalExtension());
            }

            $this->tempFile[] = $this->directory.$filename;

            $mimeType = $storage->mimeType($this->directory.$filename);
            $size = $storage->size($this->directory.$filename);

            $file = $this->file
                ->create([
                    'refid' => $this->refid,
                    'directories' => $this->directory,
                    'transtypeid' => $type->get()->getId(),
                    'filename' => $filename,
                    'mimetype' => $mimeType,
                    'filesize' => $size,
                    'createdby' => $this->userid,
                    'updatedby' => $this->userid,
                    'isactive' => TRUE,
                ]);

            unset($this->tempFile[$index]);

            $this->uploaded[] = $file;
        }
    }

    public function update($condif = null)
    {

    }

    public function delete($files, $config = null)
    {
        foreach ($files as $file) {
            $row = $this->file->find($file->fileid);

            if(file_exists(storage_path($file->directory.$file->filename))) {
                unlink(storage_path($this->directory.$file->filename));
            }

            $row->delete();
        }
    }

    public function rollback()
    {
        foreach($this->tempFile as $file) {
            unlink(storage_path($file));
        }
    }
}

<?php

use App\Helpers\Types\TypesFinder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\File;

if(!function_exists('image_api')) {

    function image_api($directories, $filename, $size = 'medium') {
        return sprintf(url("images/%s/%s/%s"),
            $size,
            str_replace("/", "-", $directories),
            $filename
        );
    }
}

if(!function_exists('rawSearch')) {

    function rawSearch($value) {
        return DB::raw("TRIM(LOWER($value))");
    }
}

if(!function_exists('strLike')) {

    function strLike($value) {
        return "%$value%";
    }
}

if(!function_exists('imageNotFound')) {
    function imageNotFound() {
        return sprintf("%s/images/medium/assets/not-found.jpg", url());
    }
}

if(!function_exists('imageUrlField')) {
    function imageUrlField($alias = 'imageUrl', $size = 'medium') {
        return sprintf("CONCAT('%s/images/$size/', REPLACE(directories, '/', '-'), '/', filename) as $alias", url());
    }
}

if(!function_exists('Base64ToFile')) {

    /**
     * @param string|string[]
     * @param string|null
     * @return UploadedFile|UploadedFile[]
     * */
    function Base64ToFile($data, $filename = null) {
        $datas = (is_array($data)) ? $data : array($data);
        $filenames = (is_array($filename)) ? $filename : array($filename);

        $files = array();
        foreach($datas as $i => $base64File) {
            $name = $filenames[$i] ?? Str::uuid()->toString();
            $tmpFilePath = sys_get_temp_dir() . '/' . $name;
            file_put_contents($tmpFilePath, base64_decode($base64File));

            $tmpFile = new File($tmpFilePath);

            $uploadFile = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType()
            );

            $files[] = $uploadFile;
        }

        return $files;
    }
}

if(!function_exists('str_arr_replace')) {
    function str_arr_replace($string, $array = array(), $prefix = '${', $suffix = '}') {
        foreach($array as $find => $replace) {
            $string = str_replace($prefix.$find.$suffix, $replace, $string);
        }
        return $string;
    }
}

if(!function_exists('findTypesInArray')) {
    function findTypesInArray($types, $array, $result = null) {
        $string = '';
        foreach($array as $value) {
            if($value->type->typecd == $types) {
                $string = is_null($result) ? $value->typevalname : $value->$result;
                break;
            }
        }
        return $string;
    }
}

if(!function_exists('findInArray')) {
    function findInArray($array, $callback) {
        $founded = null;
        foreach($array as $data) {
            if(call_user_func($callback, $data)) {
                $founded = $data;
                break;
            }
        }
        return $founded;
    }
}

if(!function_exists('terbilang')) {
    function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
    }
}

if(!function_exists('terbilang')) {
    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }
        return $hasil;
    }
}

if(!function_exists('calendarDays')) {
    function calendarDays($days) {
        if($days > 365)
            return number_format(round($days/365), 2);
        else if($days > 30)
            return number_format(round($days/30), 2);
        else if($days > 1)
            return number_format(round($days/7), 2);
        else
            return $days;
    }
}

if(!function_exists('calendarUnit')) {
    function calendarUnit($days) {
        if($days < 7)
            return 'hari';
        else if($days < 30)
            return 'minggu';
        else if($days < 365)
            return 'bulan';
        else
            return 'tahun';
    }
}

if(!function_exists('LocaleDateTime'))
{
    function LocaleDateTime($value, $timezone = ' +7 hours') {
        return date('Y-m-d H:i:s', strtotime($value.$timezone));
    }
}

if(!function_exists('IndDayName')) {
    function IndDayName($date) {
        $arrShortDaysInd = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );

        return $arrShortDaysInd[date('D', strtotime($date))];
    }
}

if(!function_exists('IndDateFormat')) {
    function IndDateFormat($date, $format) {
        $arrShortDaysInd = array(
            'Sun' => 'Minnggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );

        $arrLongDaysInd = array(
            'Sunday' => 'Minggu',
            'Monday' => 'Senin',
            'Tuesday' => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday' => 'Kamis',
            'Friday' => 'Jumat',
            'Saturday' => 'Sabtu',
        );

        $arrShortMonthInd = array(
            'Aug' => 'Agu',
            'Oct' => 'Okt',
            'Dec' => 'Des'
        );

        $arrLongMonthInd = array(
            'January' => 'Januari',
            'February' => 'Februari',
            'March' => 'Maret',
            'June' => 'Juni',
            'July' => 'Juli',
            'August' => 'Agustus',
            'October' => 'Oktober',
            'November' => 'November',
            'December' => 'Desember',
        );

        $newdate = date($format, strtotime($date));
        foreach($arrShortDaysInd as $index => $value)
            $newdate = str_replace($index, $value, $newdate);

        foreach($arrLongDaysInd as $index => $value)
            $newdate = str_replace($index, $value, $newdate);

        foreach($arrShortMonthInd as $index => $value)
            $newdate = str_replace($index, $value, $newdate);

        foreach($arrLongMonthInd as $index => $value)
            $newdate = str_replace($index, $value, $newdate);

        return $newdate;
    }
}

if(!function_exists('convertData')) {
    function convertData($data) {
        return json_decode(json_encode($data));
    }
}

if(!function_exists('createStrVar')) {
    function createStrVar($var, $object) {
        return sprintf("%s->%s", $var, $object);
    }
}

if(!function_exists('createStrVarArr')) {
    function createStrVarArr($var, $index) {
        return sprintf("%s[%s]", $var, $index);
    }
}

if(!function_exists('convertObjToStrVar')) {
    function convertObjToStrVar($object, $var = '$data') {
        $array = array();
        foreach($object as $field => $value) {
            if(is_string($value)
                || is_numeric($value)
                || is_bool($value))
                $array[createStrVar($var, $field)] = $value;

            if(is_array($value))
                $array[createStrVar($var, $field)] = convertArrToStrVar($value, createStrVar($var, $field));

            if(is_object($value))
                $array = array_merge($array, convertObjToStrVar($value, createStrVar($var, $field)));
        }
        return $array;
    }
}

if(!function_exists('convertArrToStrVar')) {
    function convertArrToStrVar($array, $var = '$data') {
        $newarray = array();
        foreach($array as $index => $object) {
            $newarray[] = convertObjToStrVar($object, createStrVarArr($var, $index));
        }
        return $newarray;
    }
}

if(!function_exists('code')) {
    function code($code) {
        return sprintf("%s;", $code);
    }
}

if(!function_exists('variable')) {
    function variable($code, $variable, $emptyValue = null) {
        if(!empty($code))
            $code = !is_null($emptyValue) ? "(!empty($code)) ? $code : '$emptyValue'" : $code;
        else
            $code = "'$emptyValue'";
        return sprintf("$variable = %s;", $code);
    }
}

if(!function_exists('codeEcho')) {
    function codeEcho($code) {
        return sprintf("echo %s;", $code);
    }
}

if(!function_exists('arraySearchChild')) {
    function arraySearchChild($array, $needle, $obj) {
        $indexArr = array();
        foreach($array as $index => $value) {
            if($value->$obj == $needle)
                $indexArr[] = $index;
        }
        return $indexArr;
    }
}

if(!function_exists('sortMenu')) {
    function sortMenu($menus)
    {
        foreach($menus as $imenu => $menu) {
            $indexChild = arraySearchChild($menus,$menu->menuid, 'masterid');
            if(count($indexChild) > 0) {
                $children = array();
                foreach($indexChild as $index) {
                    $children[] = $menus[$index];
                    unset($menus[$index]);
                }
                $menus[$imenu]->children = sortMenu($children);
            }
        }
        usort($menus, function($a, $b) {
            return $a->seq > $b->seq;
        });

        return $menus;
    }
}

if(!function_exists('currencyToString')) {

    function currencyToString($currency)
    {
        $string = str_replace(',', '', $currency);
        $string = str_replace('Rp', '', $string);

        return intval($string);
    }
}

if(!function_exists('allTypesByParent')) {

    function allTypesByParent($typeid)
    {
        $type = new TypesFinder();
        return $type->byParentCode($typeid);
    }
}

if(!function_exists('IDR'))
{
    function IDR($amount, $decimal = 2, $symbol = 'Rp. ', $decimalSeparator = '.', $thousanSeparator = ',')
    {
        return sprintf("%s%s", $symbol, number_format($amount, $decimal, $decimalSeparator, $thousanSeparator));
    }
}

if(!function_exists('dbIDR')) {

    function dbIDR($idr, $default = 0)
    {
        if($idr == '')
            return $default;
        return str_replace(',', '', $idr);
    }
}

if(!function_exists('dbDate')) {

    function dbDate($date)
    {
        $date = str_replace("/", "-", $date);
        $newDate = date('Y-m-d', strtotime($date));

        return $newDate != '1970-01-01' ? $newDate : null;
    }
}

if(!function_exists('dbDateTime')) {

    function dbDateTime($date, $format = 'Y-m-d H:i:s')
    {
        $date = str_replace("/", "-", $date);
        return date($format, strtotime($date));
    }
}

if(!function_exists('printArray')) {

    function printArray($array)
    {
        echo '<pre>';print_r($array);echo '</pre>';
    }
}

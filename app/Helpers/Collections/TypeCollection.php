<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class TypeCollection extends Collection
{

    public function getId()
    {
        return $this->typeid;
    }

    public function getName()
    {
        return $this->typename;
    }

    public function getCode()
    {
        return $this->typecd;
    }

    public function getSequence()
    {
        return $this->typeseq;
    }

    public function getDesc()
    {
        return $this->descriptions;
    }

    public function getDescFormated(array $formated)
    {
        $description = '';
        foreach($formated as $key => $value)
            $description .= str_replace($key, $value, $this->getDesc());

        return $description;
    }

    public function getJsonDesc($value)
    {
        $datas = json_decode($this->getDesc());
        return !empty($datas->$value) ? $datas->$value : null;
    }

    /**
     * @return TypeCollection
     * */
    public function parent()
    {
        if(!is_null($this->parent))
            return new TypeCollection($this->parent);

        return null;
    }
}

<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class CustomerCollection extends Collection
{

    public function getId()
    {
        return $this->customerid;
    }

    public function getName()
    {
        return $this->customername;
    }

    public function getPrefix()
    {
        return $this->customerprefix;
    }

    public function getFullname()
    {
        return $this->getPrefix() . " " . $this->getName();
    }

    public function getPhone()
    {
        return $this->customerphone;
    }

    public function getAddress()
    {
        return $this->customeraddres;
    }

    public function getPostalCode()
    {
        return $this->postalcode;
    }

    public function getReferalCode()
    {
        return $this->referalcode;
    }

    public function getLatitude()
    {
        return $this->customerlatitude;
    }

    public function getLongitude()
    {
        return $this->customerlongitude;
    }

    public function city()
    {
        if(!is_null($this->city))
            return new CityCollection($this->city);

        return null;
    }
}

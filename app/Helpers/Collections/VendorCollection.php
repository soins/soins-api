<?php

namespace App\Helpers\Collections;

use App\Helpers\Collection;
use App\Models\Masters\Vendor;
use Illuminate\Database\Eloquent\Relations\Relation;

class VendorCollection extends Collection
{

    static public function create($values)
    {
        /* @var Relation|Vendor $vendor */
        $vendor = new Vendor();
        $create = $vendor->create($values);
        return new VendorCollection($create);
    }

    public function getId()
    {
        return $this->vendorid;
    }

    public function getName()
    {
        return $this->vendornm;
    }

    public function getDesc()
    {
        return $this->description;
    }
}

<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class SubdistrictCollection extends Collection
{

    public function getId()
    {
        return $this->subdistrictid;
    }

    public function getName()
    {
        return $this->subdistrictname;
    }

    public function city()
    {
        if(!is_null($this->city))
            return new CityCollection($this->city);

        return null;
    }
}

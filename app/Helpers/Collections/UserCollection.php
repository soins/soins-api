<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class UserCollection extends Collection
{

    public function getId()
    {
        return $this->userid;
    }

    public function getUniqueName()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->userpassword;
    }

    public function getFullname()
    {
        return $this->userfullname;
    }

    public function getEmail()
    {
        return $this->useremail;
    }

    public function getPhone()
    {
        return $this->userphone;
    }

    public function getDeviceId()
    {
        return $this->userdeviceid;
    }

    public function getFcmToken()
    {
        return $this->fcmtoken;
    }
}

<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class CountryCollection extends Collection
{

    public function getId()
    {
        return $this->countryid;
    }

    public function getName()
    {
        return $this->countryname;
    }
}

<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class CityCollection extends Collection
{

    public function getId()
    {
        return $this->cityid;
    }

    public function getName()
    {
        return $this->cityname;
    }

    public function province()
    {
        if(!is_null($this->province))
            return new ProvinceCollection($this->province);

        return null;
    }
}

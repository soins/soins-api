<?php


namespace App\Helpers\Collections;

use App\Helpers\Collection;

class TicketCollection extends Collection
{

    public function getId()
    {
        return $this->ticketid;
    }

    public function getCode()
    {
        return $this->ticketcd;
    }

    public function getDesc()
    {
        return $this->description;
    }

    public function getSubject()
    {
        return $this->tickettitle;
    }

    public function getCreatedDate()
    {
        return date('d F H', strtotime($this->createddate));
    }

    public function getUpdatedDate()
    {
        return date('d F H', strtotime($this->updateddate));
    }

    public function getDescString()
    {
        $string = "";
        foreach(json_decode($this->getDesc()) as $desc) {
            $string .= $desc->insert;
        }

        return $string;
    }

    /**
     * @return CustomerCollection
     * */
    public function getCustomer()
    {
        if(!is_null($this->customer))
            return new CustomerCollection($this->customer);

        return null;
    }

    /**
     * @return TypeCollection
     * */
    public function getPriority()
    {
        if(!is_null($this->priority))
            return new TypeCollection($this->priority);

        return null;
    }

    /**
     * @return TypeCollection
     * */
    public function getStatus()
    {
        if(!is_null($this->status))
            return new TypeCollection($this->status);

        return null;
    }


    /**
     * @return UserCollection
     * */
    public function getCreated()
    {
        if(!is_null($this->datacreated))
            return new UserCollection($this->datacreated);

        return null;
    }
}

<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class VillageCollection extends Collection
{

    public function getId()
    {
        return $this->villageid;
    }

    public function getName()
    {
        return $this->villagename;
    }

    public function subdistrict()
    {
        if(!is_null($this->subdistrict))
            return new SubdistrictCollection($this->subdistrict);

        return null;
    }
}

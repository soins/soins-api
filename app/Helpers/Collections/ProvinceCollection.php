<?php


namespace App\Helpers\Collections;


use App\Helpers\Collection;

class ProvinceCollection extends Collection
{

    public function getId()
    {
        return $this->provinceid;
    }

    public function getName()
    {
        return $this->provincename;
    }

    public function country()
    {
        if(!is_null($this->country))
            return new CountryCollection($this->country);

        return null;
    }
}

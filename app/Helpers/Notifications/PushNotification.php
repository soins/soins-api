<?php


namespace App\Helpers\Notifications;


use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Helpers\Users\UsersFinder;
//use App\Models\Chatting;
use App\Models\Masters\Notification;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;

class PushNotification
{

    private $pushUrl = "https://fcm.googleapis.com/fcm/send";
    private $authorization = "AAAAU1-VSew:APA91bG92WFPvp3vqmVgkjyFdf0lvgP8Vbxc6YdVmoyeITTs-mvlLSrkPp1Le-OQeibHJ8UPsjftfEOggLzEQp5ISFHZvSUF3Kyl7WbM07snZks_yiBajsCmotvmLEnwmXxMi6CvO4-X";

    private $userid;

    /**
     * @var TypesFinder
     */
    private $typefinder;

    /**
     * @var UsersFinder
     */
    private $userfinder;

    private $notificationType;

    private $refId;

    private $title;

    private $body;

    private $icon;

    private $clickAction;

    private $datas = array();

    private $headers = array();

    /**
     * @var Notification|Relation
     */
    private $notification;

    public function __construct($userid)
    {
        $this->userid = $userid;

        $this->typefinder = new TypesFinder();
        $this->userfinder = new UsersFinder();

        $this->notification = new Notification();

//        $this->message = new Chatting();
    }

    public function setNotificationType($notificationType, $refId)
    {
        $this->notificationType = $notificationType;
        $this->refId = $refId;
    }

    public function setNotification($title, $body, $icon = null, $clickAction = null)
    {
        $this->title = $title;
        $this->body = $body;
        $this->icon = $icon;
        $this->clickAction = $clickAction;
    }

    public function setNotificationData($datas)
    {
        $this->datas = $datas;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @param string|array|null $usersId
     * @return array
     * @throws Exception
     */
    public function pushTo($usersId, $saveNotification = false)
    {
        $typeFinder = TypesFinder::find()->byCode($this->notificationType);

        $users = is_array($usersId) ? $usersId : array($usersId);

        $results = array();
        foreach($users as $userId) {
            $userFinder = UsersFinder::init()->byId($usersId);

            $notificationid = 0;
            if($saveNotification) {
                $notification = new NotificationsCollection(
                    $this->notification->create([
                        'refid' => $this->refId,
                        'notificationtypeid' => $typeFinder->get()->getId(),
                        'notificationtoid' => $userId,
                        'notificationtitle' => $this->title,
                        'description' => $this->body,
                        'createdby' => $this->userid,
                        'updatedby' => $this->userid,
                    ])
                );
                $notificationid = $notification->notificationid;
            }

            $fields = array(
                'to' => $userFinder->getFcmToken(),
                'notification' => array(
                    'body' => $this->body,
                    'title' => $this->title,
                    'icon' => $this->icon,
                    'click_action' => $this->clickAction,
                ),
                'data' => array_merge($this->datas, array(
		    'id' => $notificationid,
		    'code' => $typeFinder->get()->getCode(),
		)),
            );

            $headers = array_merge($this->headers, array(
                "Authorization: key=$this->authorization",
                "Content-Type: application/json",
            ));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->pushUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $results[] = curl_exec($ch);;
        }

        return $results;
    }


//    public function messageTo($userId, $bpid)
//    {
//
////        $users = is_array($usersId) ? $usersId : func_get_args();
//
//        $results = array();
////        foreach($users as $userId) {
//            $userFinder = $this->userfinder->find($userId);
//            $userFinderme = $this->userfinder->find($this->userid);
//
//            $message = $this->message->create([
//                "messagetext" => $this->body,
//                "senderid" => $this->userid,
//                "receiverid" => $userId,
//                "createdby" => $this->userid,
//                "updatedby" => $this->userid,
//                "isactive" => TRUE,
//                "isread" => FALSE,
//                "bpid" => $bpid
//            ]);
//
//            $fields = array(
//                'to' => $userFinder->fcmtoken,
//                'notification' => array(
//                    'body' => $this->body,
//                    'title' => $userFinderme->userfullname,
//                    'icon' => $this->icon,
//                    'click_action' => $this->clickAction,
//                ),
//                'data' => array_merge($this->datas, array(
//                    'id' => $message->messageid,
//                    'code' => "MSG",
//                )),
//            );
//
//            $headers = array_merge($this->headers, array(
//                "Authorization: key=$this->authorization",
//                "Content-Type: application/json",
//            ));
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $this->pushUrl);
//            curl_setopt($ch, CURLOPT_POST, true);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//
//            $results[] = curl_exec($ch);;
////        }
//
//        return $results;
//    }
}

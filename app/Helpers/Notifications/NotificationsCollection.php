<?php


namespace App\Helpers\Notifications;


class NotificationsCollection
{

    public $notificationid;
    public $refid;
    public $notificationtypeid;
    public $notificationtoid;
    public $notificationtitle;
    public $description;
    public $isviewed;

    public function __construct($rawResults)
    {
        foreach(json_decode(json_encode($rawResults)) as $field => $value)
            $this->$field = $value;
    }
}

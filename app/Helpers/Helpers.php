<?php


namespace App\Helpers;


class Helpers
{

    static function randomStr($length = 10, $capitalize = false) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $capitalize ? strtoupper($randomString) : $randomString;
    }

    static function imageUrlField($alias = 'imageUrl', $size = 'medium') {
        return sprintf("CONCAT('%s/images/$size/', REPLACE(directories, '/', '-'), '/', filename) as $alias", url());
    }

    static function imageUrl($directories, $filename, $size) {
        return sprintf("%s/images/%s/%s/%s", url(), $size, str_replace('/', '-', $directories), $filename);
    }
}

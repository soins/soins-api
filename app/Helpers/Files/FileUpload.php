<?php


namespace App\Helpers\Files;


use App\Helpers\Collections\TypeCollection;
use App\Models\Masters\Files;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;

class FileUpload
{

    /* @var Files|Relation */
    protected $mfiles;

    /* @var UploadedFile|UploadedFile[]*/
    protected $files;

    protected $directory;

    /* @var TypeCollection */
    protected $transtype;

    protected $userid;

    protected $refid;

    private $tempfile = array();

    private $uploaded = array();

    public function __construct($userid,TypeCollection $transtype, $refid)
    {
        $this->mfiles = new Files();

        $this->userid = $userid;
        $this->transtype = $transtype;
        $this->refid = $refid;
    }

    public function uploadFileTo($files, $directory)
    {
        $this->files = is_array($files) ? $files : array($files);
        $this->directory = 'app/'.trim($directory, "\/\\") . "/";

        return $this;
    }

    /**
     * @param null $config
     * @throws Exception
     * */
    public function save($config = null)
    {
        $type = $this->transtype;

        foreach($this->files as $index => $file) {

            $manager = new ImageManager(array('drive', 'gd'));
            $image = $manager->make($file->getRealPath());

            if(!is_dir(storage_path($this->directory)))
                mkdir(storage_path($this->directory), 777, true);

            $filename = $file->getClientOriginalName();
            if($config != null) {
                $_config = call_user_func_array($config, array($file, $index));

                if (!is_object($config))
                    throw new Exception("Return config file upload must be an object");

                if (!property_exists($_config, 'filename'))
                    throw new Exception("Undefined filename, custom save must returned filename");

                $filename = $_config->filename;

                if(file_exists($this->directory.$filename))
                    throw new Exception(sprintf("Failed to upload file. File %s already exist", $filename));

                $extension = $file->getClientOriginalExtension();
                if (property_exists($_config, 'extension'))
                    $extension = $_config->extension;

                $compression = 50;
                if (property_exists($_config, 'compression'))
                    $compression = $_config->compression;

                $image->save(storage_path($this->directory . $_config->filename), $compression, $extension);
            } else {
                $image->save(storage_path($this->directory . $file->getClientOriginalName()), 50, $file->getClientOriginalExtension());
            }

            $this->tempfile[] = $this->directory.$filename;

            $mimeType = $file->getClientMimeType();
            $size = $file->getSize();

            $file = $this->mfiles
                ->create([
                    'refid' => $this->refid,
                    'directories' => $this->directory,
                    'transtypeid' => $type->getId(),
                    'filename' => $filename,
                    'mimetype' => $mimeType,
                    'filesize' => $size,
                    'createdby' => $this->userid,
                    'updatedby' => $this->userid,
                    'isactive' => TRUE,
                ]);

            unset($this->tempfile[$index]);

            $this->uploaded[] = $file;
        }
    }

    /**
     * @param null $config
     * @throws Exception
     * */
    public function singleUpdate($config = null)
    {
        $type = $this->transtype;
        $file = collect($this->files)->first();

        $mfile = $this->mfiles->where('refid', $this->refid)
            ->where('transtypeid', $this->transtype->getId())
            ->first();

        $manager = new ImageManager(array('drive', 'gd'));
        $image = $manager->make($file->getRealPath());

        if(!is_dir(storage_path($this->directory)))
            mkdir(storage_path($this->directory), 777, true);

        $filename = $file->getClientOriginalName();
        if($config != null) {
            $_config = call_user_func_array($config, array($file));

            if (!is_object($config))
                throw new Exception("Return config file upload must be an object");

            if (!property_exists($_config, 'filename'))
                throw new Exception("Undefined filename, custom save must returned filename");

            $filename = $_config->filename;

            if(file_exists($this->directory.$filename))
                throw new Exception(sprintf("Failed to upload file. File %s already exist", $filename));

            $extension = $file->getClientOriginalExtension();
            if (property_exists($_config, 'extension'))
                $extension = $_config->extension;

            $compression = 50;
            if (property_exists($_config, 'compression'))
                $compression = $_config->compression;

            $image->save(storage_path($this->directory . $_config->filename), $compression, $extension);
        } else {
            $image->save(storage_path($this->directory . $file->getClientOriginalName()), 50, $file->getClientOriginalExtension());
        }

        $this->tempfile[] = $this->directory.$filename;

        $mimeType = $file->getClientMimeType();
        $size = $file->getSize();


        if(is_null($mfile)) {
            $file = $this->mfiles
                ->create([
                    'refid' => $this->refid,
                    'directories' => $this->directory,
                    'transtypeid' => $type->getId(),
                    'filename' => $filename,
                    'mimetype' => $mimeType,
                    'filesize' => $size,
                    'createdby' => $this->userid,
                    'updatedby' => $this->userid,
                    'isactive' => TRUE,
                ]);
        } else {
            $this->mfiles->where('refid', $this->refid)
                ->where('transtypeid', $this->transtype->getId())
                ->update([
                    'directories' => $this->directory,
                    'filename' => $filename,
                    'mimetype' => $mimeType,
                    'filesize' => $size,
                    'updatedby' => $this->userid,
                ]);

            unlink(storage_path($mfile->directories.$mfile->filename));
        }

        unset($this->tempfile[0]);

        $this->uploaded[] = $file;
    }

    public function getUploaded()
    {
        return $this->uploaded;
    }

    public function rollback()
    {
        foreach($this->uploaded as $file) {
            unlink(storage_path($file));
        }

        foreach($this->tempfile as $file) {
            unlink(storage_path($file));
        }
    }
}

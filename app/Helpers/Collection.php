<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

class Collection
{
    /**
     * @param Model $data
     * */
    public function __construct($data = null)
    {
        if(is_array($data))
            $data = (object) $data;

        else if(method_exists($data, 'toArray'))
            $data = $data->toArray();

        if(!is_null($data)) {
            foreach ($data as $key => $db)
                $this->$key = $db;
        }

    }

    public function __get($arguments)
    {
        $var = strtolower(str_replace('get', '', $arguments));
        return !empty($this->$var) ? $this->$var : null;
    }
}

<?php


namespace App\Mail;


use App\Documents\PDF\PrintOutAllTicket;
use App\Models\Masters\User;
use App\Models\Tickets\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class TicketAll extends Mailable
{
    use Queueable, SerializesModels;

    /* @var object */
    protected $params;

    /* @var Request */
    protected $request;

    /* @var Ticket|Relation */
    protected $ticket;

    /* @var PrintOutAllTicket */
    protected $document;

    public function __construct($request, $params = array())
    {
        $this->request = $request;
        $this->params = collect($params);

        $this->ticket = new Ticket();
    }

    protected function generateDocument()
    {
        $bpid = $this->request->get('bpid');

        $tickets = $this->ticket->withJoin($this->ticket->defaultSelects)
            ->with([
                'datacreated' => function($query) use ($bpid) {
                    User::history($query, $bpid);
                }
            ])
            ->addSelect($this->ticket->history)
            ->where('bpid', $bpid);

        if($this->request->has('customerid'))
            $tickets->where('customerid', $this->request->get('customerid'));

        if($this->request->has('statusid'))
            $tickets->where('statusid', $this->request->get('statusid'));

        if($this->request->has('startdate') && !$this->request->has('enddate'))
            $tickets->where(DB::raw("TO_CHAR(createddate, 'YYYY-MM-DD')"), $this->request->get('startdate'));

        else if($this->request->has('startdate') && $this->request->has('enddate'))
            $tickets->whereBetween(DB::raw("TO_CHAR(createddate, 'YYYY-MM-DD')"), array($this->request->get('startdate'), $this->request->get('enddate')));

        $document = new PrintOutAllTicket();
        $document->setDatas($tickets->get());
        $document->build();

        $document->save();

        $this->document = $document;
    }

    public function build()
    {
        $this->generateDocument();

        $subject = 'Laporan Semua Tiket';
        if($this->request->has('startdate') && !$this->$this->request->has('enddate'))
            $subject = sprintf("Laporan Tiket Periode %s", date('d F Y', strtotime($this->request->get('startdate'))));

        else if($this->request->has('startdate') && $this->request->has('enddate'))
            $subject = sprintf("Laporan Tiket Periode %s - %s", date('d F Y', strtotime($this->request->get('startdate'))), date('d F Y', strtotime($this->request->get('enddate'))));

        $this->params->put('subject', $subject);

        return $this->from('no-reply@hdsii.hyperdatasystem.com', 'PT. Access')
            ->subject("Laporan Tiket")
            ->attach($this->document->getDirectories(), [
                'as' => $this->document->getFilename(),
            ])
            ->view('emails.ticketall', $this->params->toArray());
    }
}

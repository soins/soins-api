<?php


namespace App\Constants;


class DBMessage
{

    const SUCCESS = "Process telah berhasil";
    const SUCCESS_DELETED = "Data berhasil dihapus";
    const SUCCESS_ADD = "Data berhasil ditambahkan";
    const SUCCESS_EDIT = "Data berhasil diupdate";
    const SUCCESS_REPORTED = "Proses reporting berhasil";
    const SUCCESS_UPLOADED = "Data berhasil diupload";

    const ERROR_CORRUPT_DATA = "Data tidak ditemukan";
    const UNDEFINED_DATA_TYPE = "Data tipe kode %s tidak ditemukan";
    const DATA_EXIST = "%s %s sudah ada";

    const API_SERVER_ERROR_MESSAGE = 'Terjadi kesalahan pada server';

    const AUTH_LOGIN_FAILED = "Nama pengguna atau kata sandi tidak ditemukan";

    const REGISTER_FAILED = "Username Exist";
    const WRONG_PASSWORD = "Password is incorrect";
    const AUTH_REFERAL_INVALID = "Kode referal %s tidak ditemukan";
    const AUTH_REGISTER_SUCCESS = "Registrasi berhasil";
    const AUTH_UNAUTHORIZED = "Anda tidak memiliki akses";
    const AUTH_WRONG_PASSWORD = "Kata sandi salah";
}

<?php


namespace App\Constants;


class DBTypes
{

    const menu = 'MENU';
    const menuWeb = 'MWEB';
    const menuApps = 'MAPPS';

    const role = 'ROLE';
    const roleSuperuser = 'ROLESPA';
    const roleCustomer = 'ROLCUS1';
    const roleAdministrator = 'ROLEADM';
    const roleTechnician = 'ROLETEC';
    const roleEmployee = 'ROLEEMP';

    const menuAccess = 'MACC';
    const menuAccessView = 'MACCV';
    const menuAccessCreate = 'MACCA';
    const menuAccessUpdate = 'MACCU';
    const menuAccessDelete = 'MACCD';

    const businessPartner = 'BPTYPE';

    const priority = 'PRIO';
    const priorityLow = 'PRIOL';
    const priorityMedium = 'PRIOM';
    const priorityHigh = 'PRIOH';
    const priorityCritical = 'PRIOC';

    const ticket = 'TICK';

    const statusTicket = 'STSTICK';
    const statusTicketWaiting = 'STSTICKOW';
    const statusTicketOpen = 'STSTICKOP';
    const statusTicketWaitingTechnician = 'STSTICKOWT';
    const statusTicketInProgress = 'STSTICKIP';
    const statusTicketPending = 'STSTICKPD';
    const statusTicketResolved = 'STSTICKRS';
    const statusTicketClose = 'STSTICKCS';

    const progressTicket = 'PRGS';
    const progressTicketCreated = 'PRGSC';
    const progressTicketWaitingTechnician = 'PRGSWT';
    const progressTicketConfirmed = 'PRGSCF';
    const progressTicketConfirmedTech = 'PRGSCFT';
    const progressTicketOnProgress = 'PRGSPG';
    const progressTicketComplete = 'PRGSCP';

    const files = 'FILES';
    const filesTicket = 'FTICK';
    const filesTicketReply = 'FTICKR';
    const filesProfile = 'FLPROF';
    const filesTicketProgress = 'FTICKPRGS';

    const customerType = 'CUSTP';

    const assetType = 'Info';
    const custtypeindividu = 'CUS2';

    const usertypepersonal = 'USRTP1';
    const usertypetechnisian = 'USRTP2';

    const statusSchedule = "SCH_STS";
    const statusScheduleCreate = "SCH_STS01";
    const statusScheduleOnProgress = "SCH_STS02";
    const statusScheduleFinished = "SCH_STS03";
    const statusScheduleCanceled = "SCH_STS04";

    const assetCategory = 'ASSCT';

    const brand = 'BRAND';

    const assetInformation = 'ASSINF';
    const assetInformationWatt = 'ASSINFWT';

    const ticketType = 'TKTP';
    const ticketTypeService = 'TKTPS';
    const ticketTypeMaintenance = 'TKTPM';

    const scheduleType = 'SCHTP';
    const scheduleTypeService = 'SCHTPS';
    const scheduleTypeMaintenance = 'SCHTPM';

    const toward = 'TOWARD';
    const towardUser = 'TWUSER';
    const towardCompany = 'TWCOM';

    const typescheduleservice = 'SRVC';

    const notificationType = "NTF";
    const notificationTypeScheduleRecurrent = 'NTFSCR';
    const notificationTypeScheduling = 'NTFSCN';

    const detailProduct = 'DTPRD';
    const purchase = 'PRCH';

    const depreciation = 'DEPRE';

    const mappingType = 'MAPTP';
    const mappingTypeDepartment = 'MAPTPDEPT';

    const sparePartBrand = 'SPRBR';

    const sparePartCategory = 'SPCAT';

    const sparePartSubCategory = 'SPSCAT';

    const sparePartType = 'SPTP';

    const sparePartPriority = 'SPPP';
}

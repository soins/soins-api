<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'images'], function() use ($router) {
    $router->get('/', function() use ($router) {
        return "Images API : " . $router->app->version();
    });
    $router->get('/{size}/{directories}/{filename}', 'ImagesController@index');
});

$router->group(['prefix' => 'print'], function() use ($router) {

    $router->get('ticket-all', ['uses' => 'PrintController@ticketAll']);
});

$router->group(['prefix' => 'auth'], function() use ($router) {

    $router->post('login', ['uses' => 'AuthController@login']);
    $router->post('loginTechnician', ['uses' => 'AuthController@loginTechnician']);
    $router->post('register', ['uses' => 'AuthController@register']);
    $router->post('check', ['middleware' => 'auth', 'uses' => 'AuthController@check']);
    $router->post('check/{field}', ['uses' => 'AuthController@checkData']);
});

$router->get('fetch', ['uses' => 'AppController@fetch']);
$router->get('sendemail', ['uses' => 'AppController@sendEmail']);

$router->group(['middleware' => 'auth'], function() use ($router) {
    $router->get('update', ['uses' => 'AppController@update']);
    $router->get('dashboard-ticket', ['uses' => 'AppController@dashboardTicket']);
    $router->get('notifications', ['uses' => 'AppController@notifications']);
    $router->get('read-notifications', ['uses' => 'AppController@readNotification']);

    $router->group(['namespace' => 'Masters'], function() use ($router) {

        $router->group(['prefix' => 'types'], function() use ($router) {

            $router->get('select', ['uses' => 'TypesController@select']);
            $router->post('datatables', ['uses' => 'TypesController@datatables']);
            $router->get('find/{value}', ['uses' => 'TypesController@find']);

            $router->post('', ['uses' => 'TypesController@store']);
            $router->get('{id}', ['uses' => 'TypesController@show']);
            $router->put('{id}', ['uses' => 'TypesController@update']);
            $router->delete('{id}', ['uses' => 'TypesController@destroy']);
        });

        $router->group(['prefix' => 'menu'], function() use ($router) {
            $router->get('access', ['uses' => 'MenuController@access']);
            $router->get('select', ['uses' => 'MenUController@select']);
            $router->post('datatables', ['uses' => 'MenuController@datatables']);

            $router->post('', ['uses' => 'MenuController@store']);
            $router->get('{id}', ['uses' => 'MenuController@show']);
            $router->put('{id}', ['uses' => 'MenuController@update']);
            $router->delete('{id}', ['uses' => 'MenuController@destroy']);
        });

        $router->group(['prefix' => 'businesspartner'], function() use ($router) {

            $router->get('select', ['uses' => 'BusinessPartnerController@select']);
            $router->post('datatables', ['uses' => 'BusinessPartnerController@datatables']);

            $router->post('', ['uses' => 'BusinessPartnerController@store']);
            $router->get('{id}', ['uses' => 'BusinessPartnerController@show']);
            $router->put('{id}', ['uses' => 'BusinessPartnerController@update']);
            $router->delete('{id}', ['uses' => 'BusinessPartnerController@destroy']);
        });

        $router->group(['prefix' => 'bptype'], function() use ($router) {

            $router->get('select', ['uses' => 'BpTypeController@select']);
            $router->post('datatables', ['uses' => 'BpTypeController@datatables']);

            $router->post('', ['uses' => 'BpTypeController@store']);
            $router->get('{id}', ['uses' => 'BpTypeController@show']);
            $router->put('{id}', ['uses' => 'BpTypeController@update']);
            $router->delete('{id}', ['uses' => 'BpTypeController@destroy']);
        });

        $router->group(['prefix' => 'bpmenu'], function() use ($router) {

            $router->get('load', ['uses' => 'BpMenuController@load']);
            $router->post('save', ['uses' => 'BpMenuController@save']);
        });

        $router->group(['prefix' => 'bpvendor'], function() use ($router) {
            $router->get('select', ['uses' => 'BpVendorController@select']);
            $router->post('datatables', ['uses' => 'BpVendorController@datatables']);

            $router->post('', ['uses' => 'BpVendorController@store']);
            $router->get('{id}', ['uses' => 'BpVendorController@show']);
            $router->post('{id}', ['uses' => 'BpVendorController@update']);
            $router->delete('{id}', ['uses' => 'BpVendorController@destroy']);
        });

        $router->group(['prefix' => 'bpcustomer'], function() use ($router) {

            $router->get('select', ['uses' => 'BpCustomerController@select']);
            $router->post('datatables', ['uses' => 'BpCustomerController@datatables']);

            $router->post('', ['uses' => 'BpCustomerController@store']);
            $router->get('{id}', ['uses' => 'BpCustomerController@show']);
            $router->put('{id}', ['uses' => 'BpCustomerController@update']);
            $router->delete('{id}', ['uses' => 'BpCustomerController@destroy']);
        });

        $router->group(['prefix' => 'bpuser'], function() use ($router) {

            $router->get('select', ['uses' => 'BpUserController@select']);
            $router->post('datatables', ['uses' => 'BpUserController@datatables']);

            $router->post('', ['uses' => 'BpUserController@store']);
            $router->get('{id}', ['uses' => 'BpUserController@show']);
            $router->put('{id}', ['uses' => 'BpUserController@update']);
            $router->delete('{id}', ['uses' => 'BpUserController@destroy']);
        });

        $router->group(['prefix' => 'customer'], function() use ($router) {

            $router->post('search', ['uses' => 'CustomerController@search']);
            $router->post('datatables', ['uses' => 'CustomerController@datatables']);

            $router->post('', ['uses' => 'CustomerController@store']);
            $router->get('{id}', ['uses' => 'CustomerController@show']);
            $router->put('{id}', ['uses' => 'CustomerController@update']);
            $router->delete('{id}', ['uses' => 'CustomerController@destroy']);
        });

        $router->group(['prefix' => 'branch'], function() use ($router) {

            $router->get('select', ['uses' => 'BranchController@select']);
            $router->post('datatables', ['uses' => 'BranchController@datatables']);

            $router->post('', ['uses' => 'BranchController@store']);
            $router->get('{id}', ['uses' => 'BranchController@show']);
            $router->put('{id}', ['uses' => 'BranchController@update']);
            $router->delete('{id}', ['uses' => 'BranchController@destroy']);
        });

        $router->group(['prefix' => 'departement'], function() use ($router) {

            $router->get('select', ['uses' => 'DepartementController@select']);
            $router->post('datatables', ['uses' => 'DepartementController@datatables']);

            $router->post('', ['uses' => 'DepartementController@store']);
            $router->get('{id}', ['uses' => 'DepartementController@show']);
            $router->put('{id}', ['uses' => 'DepartementController@update']);
            $router->delete('{id}', ['uses' => 'DepartementController@destroy']);
        });

        $router->group(['prefix' => 'location'], function() use ($router) {

            $router->get('select', ['uses' => 'LocationController@select']);
            $router->post('datatables', ['uses' => 'LocationController@datatables']);

            $router->post('', ['uses' => 'LocationController@store']);
            $router->get('{id}', ['uses' => 'LocationController@show']);
            $router->put('{id}', ['uses' => 'LocationController@update']);
            $router->delete('{id}', ['uses' => 'LocationController@destroy']);
        });

        $router->group(['prefix' => 'assets'], function() use ($router) {

            $router->get('select', ['uses' => 'AssetsController@select']);
            $router->post('datatables', ['uses' => 'AssetsController@datatables']);
            $router->post('check/{field}', ['uses' => 'AssetsController@check']);
            $router->post('save-service', ['uses' => 'AssetsController@saveService']);
            $router->get('show-service', ['uses' => 'AssetsController@showService']);

            $router->post('', ['uses' => 'AssetsController@store']);
            $router->get('{id}', ['uses' => 'AssetsController@show']);
            $router->put('{id}', ['uses' => 'AssetsController@update']);
            $router->delete('{id}', ['uses' => 'AssetsController@destroy']);
        });

        $router->group(['prefix' => 'assetsdt'], function() use ($router) {

            $router->get('select', ['uses' => 'AssetsDetailController@select']);
            $router->post('datatables', ['uses' => 'AssetsDetailController@datatables']);

            $router->post('', ['uses' => 'AssetsDetailController@store']);
            $router->get('{id}', ['uses' => 'AssetsDetailController@show']);
            $router->put('{id}', ['uses' => 'AssetsDetailController@update']);
            $router->delete('{id}', ['uses' => 'AssetsDetailController@destroy']);
        });

        $router->group(['prefix' => 'vendor'], function() use ($router) {
            $router->get('select', ['uses' => 'VendorController@select']);
            $router->post('datatables', ['uses' => 'VendorController@datatables']);

            $router->post('', ['uses' => 'VendorController@store']);
            $router->get('{id}', ['uses' => 'VendorController@show']);
            $router->put('{id}', ['uses' => 'VendorController@update']);
            $router->delete('{id}', ['uses' => 'VendorController@destroy']);
        });

        $router->group(['prefix' => 'sparepart'], function() use ($router) {
            $router->get('select', ['uses' => 'SparePartController@select']);
            $router->post('datatables', ['uses' => 'SparePartController@datatables']);

            $router->post('', ['uses' => 'SparePartController@store']);
            $router->get('{id}', ['uses' => 'SparePartController@show']);
            $router->put('{id}', ['uses' => 'SparePartController@update']);
            $router->delete('{id}', ['uses' => 'SparePartController@destroy']);
        });

        $router->group(['prefix' => 'spareparttype'], function() use ($router) {
            $router->get('select', ['uses' => 'SparePartTypeController@select']);
            $router->post('datatables', ['uses' => 'SparePartTypeController@datatables']);

            $router->post('', ['uses' => 'SparePartTypeController@store']);
            $router->get('{id}', ['uses' => 'SparePartTypeController@show']);
            $router->put('{id}', ['uses' => 'SparePartTypeController@update']);
            $router->delete('{id}', ['uses' => 'SparePartTypeController@destroy']);
        });

        $router->group(['prefix' => 'sparepartmapping'], function() use ($router) {

            $router->post('datatables', ['uses' => 'SparePartMappingController@datatables']);
            $router->post('', ['uses' => 'SparePartMappingController@store']);
            $router->delete('{id}', ['uses' => 'SparePartMappingController@destroy']);
        });

        $router->group(['prefix' => 'recurrenttemplate'], function() use ($router) {
            $router->post('datatables', ['uses' => 'RecurrentTemplateController@datatables']);

            $router->post('', ['uses' => 'RecurrentTemplateController@store']);
            $router->get('{id}', ['uses' => 'RecurrentTemplateController@show']);
            $router->put('{id}', ['uses' => 'RecurrentTemplateController@update']);
            $router->delete('{id}', ['uses' => 'RecurrentTemplateController@destroy']);
        });

        $router->group(['prefix' => 'job'], function() use ($router) {
            $router->get('select', ['uses' => 'JobsController@select']);
            $router->post('datatables', ['uses' => 'JobsController@datatables']);

            $router->post('', ['uses' => 'JobsController@store']);
            $router->get('{id}', ['uses' => 'JobsController@show']);
            $router->put('{id}', ['uses' => 'JobsController@update']);
            $router->delete('{id}', ['uses' => 'JobsController@destroy']);
        });

        $router->group(['prefix' => 'servicetemplate'], function() use ($router) {

            $router->get('select', ['uses' => 'ServiceTemplateController@select']);
            $router->post('datatables', ['uses' => 'ServiceTemplateController@datatables']);

            $router->post('', ['uses' => 'ServiceTemplateController@store']);
            $router->get('{id}', ['uses' => 'ServiceTemplateController@show']);
            $router->put('{id}', ['uses' => 'ServiceTemplateController@update']);
            $router->delete('{id}', ['uses' => 'ServiceTemplateController@destroy']);
        });

        $router->group(['prefix' => 'servicetemplatejobs'], function() use ($router) {

            $router->get('', ['uses' => 'ServiceTemplateJobsController@all']);
            $router->post('', ['uses' => 'ServiceTemplateJobsController@store']);
            $router->get('{id}', ['uses' => 'ServiceTemplateJobsController@show']);
            $router->put('{id}', ['uses' => 'ServiceTemplateJobsController@update']);
            $router->delete('{id}', ['uses' => 'ServiceTemplateJobsControllerz@destroy']);
        });

        $router->group(['prefix' => 'jobssp'], function() use ($router) {
            $router->post('datatables', ['uses' => 'JobsSparePartController@datatables']);

            $router->get('', ['uses' => 'JobsSparePartController@index']);
            $router->post('', ['uses' => 'JobsSparePartController@store']);
            $router->get('{id}', ['uses' => 'JobsSparePartController@show']);
            $router->put('{id}', ['uses' => 'JobsSparePartController@update']);
            $router->delete('{id}', ['uses' => 'JobsSparePartController@destroy']);
        });
    });

    $router->group(['namespace' => 'Addresses'], function() use ($router) {

        $router->group(['prefix' => 'country'], function() use ($router) {

            $router->get('select', ['uses' => 'CountryController@select']);
            $router->post('datatables', ['uses' => 'CountryController@datatables']);

            $router->post('', ['uses' => 'CountryController@store']);
            $router->get('{id}', ['uses' => 'CountryController@show']);
            $router->put('{id}', ['uses' => 'CountryController@update']);
            $router->delete('{id}', ['uses' => 'CountryController@destroy']);
        });

        $router->group(['prefix' => 'province'], function() use ($router) {

            $router->get('select', ['uses' => 'ProvinceController@select']);
            $router->post('datatables', ['uses' => 'ProvinceController@datatables']);

            $router->post('', ['uses' => 'ProvinceController@store']);
            $router->get('{id}', ['uses' => 'ProvinceController@show']);
            $router->put('{id}', ['uses' => 'ProvinceController@update']);
            $router->delete('{id}', ['uses' => 'ProvinceController@destroy']);
        });

        $router->group(['prefix' => 'city'], function() use ($router) {

            $router->get('select', ['uses' => 'CityController@select']);
            $router->post('datatables', ['uses' => 'CityController@datatables']);

            $router->post('', ['uses' => 'CityController@store']);
            $router->get('{id}', ['uses' => 'CityController@show']);
            $router->put('{id}', ['uses' => 'CityController@update']);
            $router->delete('{id}', ['uses' => 'CityController@destroy']);
        });

        $router->group(['prefix' => 'subdistrict'], function() use ($router) {

            $router->get('select', ['uses' => 'SubdistrictController@select']);
            $router->post('datatables', ['uses' => 'SubdistrictController@datatables']);

            $router->post('', ['uses' => 'SubdistrictController@store']);
            $router->get('{id}', ['uses' => 'SubdistrictController@show']);
            $router->put('{id}', ['uses' => 'SubdistrictController@update']);
            $router->delete('{id}', ['uses' => 'SubdistrictController@destroy']);
        });

        $router->group(['prefix' => 'village'], function() use ($router) {

            $router->get('select', ['uses' => 'VillageController@select']);
            $router->post('datatables', ['uses' => 'VillageController@datatables']);

            $router->post('', ['uses' => 'VillageController@store']);
            $router->get('{id}', ['uses' => 'VillageController@show']);
            $router->put('{id}', ['uses' => 'VillageController@update']);
            $router->delete('{id}', ['uses' => 'VillageController@destroy']);
        });
    });

    $router->group(['namespace' => 'Security'],function() use ($router) {

        $router->group(['prefix' => 'users'], function() use ($router) {

            $router->get('select', ['uses' => 'UserController@select']);
            $router->post('datatables', ['uses' => 'UserController@datatables']);
            $router->post('check/{field}', ['uses' => 'UserController@check']);
            $router->post('change-password', ['uses' => 'UserController@changePassword']);
            $router->post('change-profile', ['uses' => 'UserController@changeProfile']);

            $router->post('', ['uses' => 'UserController@store']);
            $router->get('{id}', ['uses' => 'UserController@show']);
            $router->put('{id}', ['uses' => 'UserController@update']);
            $router->delete('{id}', ['uses' => 'UserController@destroy']);
        });

        $router->group(['prefix' => 'usersdt'], function() use ($router) {

            $router->get('select', ['uses' => 'UserDetailController@select']);
            $router->post('datatables', ['uses' => 'UserDetailController@datatables']);

            $router->post('', ['uses' => 'UserDetailController@store']);
            $router->get('{id}', ['uses' => 'UserDetailController@show']);
            $router->put('{id}', ['uses' => 'UserDetailController@update']);
            $router->delete('{id}', ['uses' => 'UserDetailController@destroy']);
        });
    });

    $router->group(['namespace' => 'Settings'], function() use ($router) {

        $router->group(['prefix' => 'role'], function() use ($router) {

            $router->get('access', ['uses' => 'RoleController@access']);
            $router->post('access', ['uses' => 'RoleController@save']);
        });

        $router->group(['prefix' => 'mappingtype'], function() use ($router) {
            $router->post('datatables', ['uses' => 'MappingTypeController@datatables']);

            $router->post('', ['uses' => 'MappingTypeController@store']);
            $router->get('find', ['uses' => 'MappingTypeController@find']);
        });
    });

    $router->group(['prefix' => 'ticket', 'namespace' => 'Tickets'], function() use ($router) {

        $router->group(['prefix' => 'progress'], function() use ($router) {
            $router->get('load', ['uses' => 'TicketProgressController@load']);
            $router->post('', ['uses' => 'TicketProgressController@store']);
        });

        $router->group(['prefix' => 'assets'], function() use ($router) {
            $router->get('select', ['uses' => 'TicketAssetController@select']);
            $router->post('datatables', ['uses' => 'TicketAssetController@datatables']);
            $router->post('', ['uses' => 'TicketAssetController@store']);
            $router->delete('{id}', ['uses' => 'TicketAssetController@destroy']);
            $router->get('sparepart', ['uses' => 'TicketAssetController@sparePart']);
        });

        $router->get('loaddetails', ['uses' => 'TicketController@loadDetails']);
        $router->post('reply', ['uses' => 'TicketController@reply']);
        $router->post('datatables', ['uses' => 'TicketController@datatables']);
        $router->post('schedule', ['uses' => 'TicketController@schedule']);
        $router->post('confirm', ['uses' => 'TicketController@confirm']);

        $router->post('', ['uses' => 'TicketController@store']);
        $router->get('/{ticketcd}', ['uses' => 'TicketController@show']);
        $router->put('/{id}', ['uses' => 'TicketController@update']);
        $router->delete('/{id}', ['uses' => 'TicketController@destroy']);
    });

    $router->group(['prefix' => 'notification', 'namespace' => 'Transactions'], function() use ($router) {

        $router->post('create-schedule', ['uses' => 'NotificationController@createSchedule']);
    });

    $router->group(['prefix' => 'schedule', 'namespace' => 'Schedules'], function() use ($router) {

        $router->get('{id}', ['uses' => 'ScheduleController@show']);
    });

    $router->group(['namespace' => 'Apps'], function() use ($router) {

        $router->group(['prefix' => 'menu'], function() use ($router) {
            $router->post('select', ['uses' => 'Ctrl_Menu@select']);
        });

        $router->group(['prefix' => 'user'], function() use ($router) {
            $router->post('select', ['uses' => 'Ctrl_User@select']);
            $router->post('edit', ['uses' => 'Ctrl_User@edit']);
            $router->post('changePassword', ['uses' => 'Ctrl_User@changePassword']);
            $router->post('updateFCMToken', ['uses' => 'Ctrl_User@updateFCMToken']);
        });

        $router->group(['prefix' => 'schedule'], function() use ($router) {
            $router->post('select', ['uses' => 'Ctrl_Schedule@select']);
            $router->post('selectToday', ['uses' => 'Ctrl_Schedule@selectToday']);
            $router->post('selectComingNext', ['uses' => 'Ctrl_Schedule@selectComingNext']);
            $router->post('selectLate', ['uses' => 'Ctrl_Schedule@selectLate']);
            $router->post('saveEndSchedule', ['uses' => 'Ctrl_Schedule@saveEndSchedule']);
        });

        $router->group(['prefix' => 'ticket'], function() use ($router) {
            $router->post('selectbyid', ['uses' => 'Ctrl_Ticket@select']);
            $router->post('selectTicketAsset', ['uses' => 'Ctrl_Ticket@selectTicketAsset']);
            $router->post('saveTicketProgress', ['uses' => 'Ctrl_Ticket@saveTicketProgress']);
            $router->post('saveTicketProgressConfirm', ['uses' => 'Ctrl_Ticket@saveTicketProgressConfirm']);
            $router->post('selectTicketDetail', ['uses' => 'Ctrl_Ticket@selectTicketDetail']);
            $router->post('saveTicketDetail', ['uses' => 'Ctrl_Ticket@saveTicketDetail']);
            $router->post('selectByAssign', ['uses' => 'Ctrl_Ticket@selectByAssign']);
            $router->post('selectByDept', ['uses' => 'Ctrl_Ticket@selectByDept']);
        });

        $router->group(['prefix' => 'notification'], function() use ($router) {
            $router->post('select', ['uses' => 'Ctrl_Notification@select']);
            $router->post('selectCount', ['uses' => 'Ctrl_Notification@selectCount']);
            $router->post('updateRead', ['uses' => 'Ctrl_Notification@updateRead']);
        });
    });
});

$router->group(['namespace' => 'Apps'], function() use ($router) {

    $router->group(['prefix' => 'user'], function() use ($router) {
        $router->post('register', ['uses' => 'Ctrl_User@register']);
        $router->post('registerTechnician', ['uses' => 'Ctrl_User@registerTechnician']);
    });
});

$router->group(['namespace' => 'Masters'], function() use ($router) {

    $router->group(['prefix' => 'select'], function() use ($router) {

        $router->post('type', ['uses' => 'TypesController@select']);
    });
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrscheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trschedule', function (Blueprint $table) {
            $table->id('scheduleid');
            $table->bigInteger('bpid');
            $table->bigInteger('refid')->nullable();
            $table->bigInteger('actualid')->nullable();
            $table->bigInteger('subjectid');
            $table->bigInteger('customerid');
            $table->string('scheduletitle', 50);
            $table->date('scheduledatefrom');
            $table->date('scheduledateto');
            $table->date('actualdatefrom')->nullable();
            $table->date('actualdateto')->nullable();
            $table->bigInteger('statusid');
            $table->bigInteger('towardtypeid');
            $table->bigInteger('assigntoid');

            $table->bigInteger('createdby')->nullable();
            $table->timestamp('createddate')->useCurrent();
            $table->bigInteger('updatedby')->nullable();
            $table->timestamp('updateddate')->useCurrent();
            $table->boolean('isactive')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trschedule');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrticketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trticket', function (Blueprint $table) {
            $table->id('ticketid');
            $table->date('duedate')->nullable();
            $table->bigInteger('priorityid')->nullable();
            $table->bigInteger('tickettypeid');
            $table->bigInteger('customerid');
            $table->string('ticketcd', 100);
            $table->string('tickettitle', 100);
            $table->text('description');
            $table->bigInteger('statusid');
            $table->bigInteger('bpid');
            $table->bigInteger('deptfromid');
            $table->bigInteger('depttoid');
            $table->bigInteger('branchfromid');
            $table->bigInteger('branchtoid');

            $table->bigInteger('createdby')->nullable();
            $table->timestamp('createddate')->useCurrent();
            $table->bigInteger('updatedby')->nullable();
            $table->timestamp('updateddate')->useCurrent();
            $table->boolean('isactive')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trticket');
    }
}

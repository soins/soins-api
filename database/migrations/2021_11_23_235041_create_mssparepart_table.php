<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMssparepartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mssparepart', function (Blueprint $table) {
            $table->id('sparepartid');
            $table->bigInteger('bpid');
            $table->bigInteger('branchid');
            $table->bigInteger('deptid');
            $table->bigInteger('brandid');
            $table->bigInteger('sparepartcategoryid');
            $table->bigInteger('sparepartsubcategoryid');
            $table->bigInteger('spareparttypeid');
            $table->string('sparepartname');
            $table->text('description')->nullable();

            $table->bigInteger('createdby')->nullable();
            $table->timestamp('createddate')->useCurrent();
            $table->bigInteger('updatedby')->nullable();
            $table->timestamp('updateddate')->useCurrent();
            $table->boolean('isactive')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mssparepart');
    }
}

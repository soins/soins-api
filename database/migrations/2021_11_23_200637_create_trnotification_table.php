<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrnotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trnotification', function (Blueprint $table) {
            $table->id('notificationid');
            $table->bigInteger('refid');
            $table->bigInteger('actualid')->nullable();
            $table->bigInteger('notificationtypeid');
            $table->bigInteger('notificationtoid');
            $table->string('notificationtitle', 100);
            $table->text('description');
            $table->boolean('isviewed')->default(false);
            $table->boolean('ishandled')->default(false);

            $table->bigInteger('createdby')->nullable();
            $table->timestamp('createddate')->useCurrent();
            $table->bigInteger('updatedby')->nullable();
            $table->timestamp('updateddate')->useCurrent();
            $table->boolean('isactive')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trnotification');
    }
}

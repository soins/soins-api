<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstemplateserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mstemplateservice', function (Blueprint $table) {
            $table->id("templateserviceid");
            $table->bigInteger("assetcategoryid");
            $table->bigInteger("assetsubcategoryid");
            $table->bigInteger("assetbrandid");
            $table->bigInteger("assettypeid");
            $table->string("servicenm", 100);
            $table->text("payload");
            $table->bigInteger('bpid');
            $table->bigInteger('deptid')->nullable();

            $table->bigInteger('createdby')->nullable();
            $table->timestamp('createddate')->useCurrent();
            $table->bigInteger('updatedby')->nullable();
            $table->timestamp('updateddate')->useCurrent();
            $table->boolean('isactive')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mstemplateservice');
    }
}

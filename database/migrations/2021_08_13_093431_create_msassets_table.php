<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsassetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msassets', function (Blueprint $table) {
            $table->id('assetid');
            $table->string('assetno', 50)->nullable();
            $table->bigInteger('bpid');
            $table->bigInteger('assetcategoryid');
            $table->bigInteger('assetsubcategoryid');
            $table->bigInteger('merkid');
            $table->bigInteger('assettypeid');
            $table->bigInteger('branchid');
            $table->bigInteger('deptid');
            $table->bigInteger('locationid')->nullable();
            $table->bigInteger('usedbyid')->nullable();
            $table->bigInteger('usedbranchid')->nullable();
            $table->bigInteger('useddeptid')->nullable();
            $table->string('assetname', 100);
            $table->double('purchaseprice', 18, 2)->nullable();
            $table->date('purchasedate')->nullable();
            $table->date('warrantyexpdate')->nullable();
            $table->bigInteger('purchasetypeid')->nullable();
            $table->bigInteger('depreciationtypeid')->nullable();
            $table->bigInteger('vendorid')->nullable();
            $table->text('schedulepattern')->nullable();
            $table->text('recurrentpattern')->nullable();

            $table->bigInteger('createdby')->nullable();
            $table->timestamp('createddate')->useCurrent();
            $table->bigInteger('updatedby')->nullable();
            $table->timestamp('updateddate')->useCurrent();
            $table->boolean('isactive')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msassets');
    }
}

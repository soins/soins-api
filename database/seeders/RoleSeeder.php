<?php

namespace Database\Seeders;

use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Menu;
use App\Models\Masters\Role;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{

    /* @var Role|Relation */
    protected $role;

    /* @var Menu|Relation */
    protected $menu;

    /* @var BusinessPartner|Relation */
    protected $bp;

    public function __construct()
    {
        $this->role = new Role();
        $this->menu = new Menu();
        $this->bp = new BusinessPartner();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminid = 1;
        $now = date('Y-m-d H:i:s');

        $bps = $this->bp->get();
        $menus = $this->menu->get();
        $types = TypesFinder::find()->byParentCode(DBTypes::menuAccess);
        $usertype = TypesFinder::find()->byCode(DBTypes::roleSuperuser);

        if($usertype->valid() && $types->valid()) {
            foreach($bps->all() as $bp) {

                $inserts = array();
                foreach($menus->all() as $menu) {
                    foreach ($types->child() as $access) {
                        $inserts[] = [
                            'usertypeid' => $usertype->get()->getId(),
                            'menuid' => $menu->menuid,
                            'bpid' => $bp->bpid,
                            'accessid' => $access->getId(),
                            'createdby' => $adminid,
                            'createddate' => $now,
                            'updatedby' => $adminid,
                            'updateddate' => $now,
                        ];
                    }
                }

                $this->role->insert($inserts);
            }
        }
    }
}

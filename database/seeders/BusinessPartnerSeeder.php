<?php

namespace Database\Seeders;

use App\Constants\DBTypes;
use App\Helpers\Collections\TypeCollection;
use App\Helpers\Types\TypesFinder;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\BusinessPartnerCustomer;
use App\Models\Masters\BusinessPartnerMenu;
use App\Models\Masters\BusinessPartnerType;
use App\Models\Masters\Customer;
use App\Models\Masters\Menu;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class BusinessPartnerSeeder extends Seeder
{

    /* @var BusinessPartner|Relation */
    protected $businesspartner;

    /* @var BusinessPartnerType|Relation */
    protected $bptype;

    /* @var BusinessPartnerMenu|Relation */
    protected $bpmenu;

    /* @var BusinessPartnerCustomer|Relation */
    protected $bpcustomer;

    /* @var Menu|Relation */
    protected $menu;

    /* @var Customer|Relation */
    protected $customer;

    public function __construct()
    {
        $this->businesspartner = new BusinessPartner();

        $this->bptype = new BusinessPartnerType();
        $this->bpmenu = new BusinessPartnerMenu();
        $this->bpcustomer = new BusinessPartnerCustomer();

        $this->menu = new Menu();
        $this->customer = new Customer();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminid = 1;
        $now = date('Y-m-d H:i:s');

        $find = TypesFinder::find()->byParentCode(DBTypes::businessPartner);
        if($find->valid()) {

            /* @var TypeCollection $bptype */
            $bptype = $find->child()->first();

            $bp = $this->businesspartner->create([
                'bpname' => 'PT. Default Company',
                'bptypeid' => $bptype->getId(),
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $find = TypesFinder::find()->byParentCode([
                DBTypes::menu,
                DBTypes::role,
                DBTypes::priority,
                DBTypes::statusTicket,
                DBTypes::progressTicket,
                DBTypes::brand,
                DBTypes::assetCategory,
                DBTypes::assetInformation,
                DBTypes::purchase,
                DBTypes::depreciation,
                DBTypes::ticketType,
                DBTypes::progressTicket,
                DBTypes::scheduleType,
                DBTypes::toward,
            ]);
            if($find->valid()) {
                $inserts = array();
                foreach($find->child(DBTypes::menu) as $menu)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $menu->parent()->getId(), 'typevalue' => $menu->getId(), 'typeseq' => $menu->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach($find->child(DBTypes::role) as $role)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $role->parent()->getId(), 'typevalue' => $role->getId(), 'typeseq' => $role->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach($find->child(DBTypes::priority) as $priority)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $priority->parent()->getId(), 'typevalue' => $priority->getId(), 'typeseq' => $priority->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::statusTicket) as $statusTicket)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $statusTicket->parent()->getId(), 'typevalue' => $statusTicket->getId(), 'typeseq' => $statusTicket->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::progressTicket) as $progressTicket)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $progressTicket->parent()->getId(), 'typevalue' => $progressTicket->getId(), 'typeseq' => $progressTicket->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::assetType) as $assetType)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $assetType->parent()->getId(), 'typevalue' => $assetType->getId(), 'typeseq' => $assetType->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::assetInformation) as $assetInformation)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $assetInformation->parent()->getId(), 'typevalue' => $assetInformation->getId(), 'typeseq' => $assetInformation->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::ticketType) as $ticketType)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $ticketType->parent()->getId(), 'typevalue' => $ticketType->getId(), 'typeseq' => $ticketType->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::scheduleType) as $scheduleType)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $scheduleType->parent()->getId(), 'typevalue' => $scheduleType->getId(), 'typeseq' => $scheduleType->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::toward) as $toward)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $toward->parent()->getId(), 'typevalue' => $toward->getId(), 'typeseq' => $toward->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::brand) as $brand)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $brand->parent()->getId(), 'typevalue' => $brand->getId(), 'typeseq' => $brand->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::assetCategory) as $category)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $category->parent()->getId(), 'typevalue' => $category->getId(), 'typeseq' => $category->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::assetInformation) as $inforation)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $inforation->parent()->getId(), 'typevalue' => $inforation->getId(), 'typeseq' => $inforation->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::purchase) as $purchase)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $purchase->parent()->getId(), 'typevalue' => $purchase->getId(), 'typeseq' => $purchase->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::depreciation) as $depreciation)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $depreciation->parent()->getId(), 'typevalue' => $depreciation->getId(), 'typeseq' => $depreciation->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);

                $inserts = array();
                foreach ($find->child(DBTypes::progressTicket) as $progressTicket)
                    $inserts[] = ['bpid' => $bp->bpid, 'typeid' => $progressTicket->parent()->getId(), 'typevalue' => $progressTicket->getId(), 'typeseq' => $progressTicket->getSequence(), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

                $this->bptype->insert($inserts);
            }

            $customers = $this->customer->get();

            $inserts = array();
            foreach ($customers->all() as $customer)
                $inserts[] = [
                    'bpid' => $bp->bpid,
                    'customerid' => $customer->customerid,
                    'customername' => $customer->customername,
                    'customerphone' => $customer->customerphone,
                    'customeraddress' => $customer->customeraddress,
                    'customerpic' => 'Pak Harto',
                    'createdby' => $adminid,
                    'createddate' => $now,
                    'updatedby' => $adminid,
                    'updateddate' => $now,
                ];

            $this->bpcustomer->insert($inserts);

            $menus = $this->menu->get();

            $insers = array();
            foreach($menus->all() as $menu)
                $insers[] = ['bpid' => $bp->bpid, 'menuid' => $menu->menuid, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];

            $this->bpmenu->insert($insers);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Addresses\City;
use App\Models\Addresses\Country;
use App\Models\Addresses\Province;
use App\Models\Addresses\Subdistrict;
use App\Models\Addresses\Village;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{

    /* @var Country|Relation */
    protected $country;

    /* @var Province|Relation */
    protected $province;

    /* @var City|Relation */
    protected $city;

    /* @var Subdistrict|Relation */
    protected $subdistrict;

    /* @var Village|Relation */
    protected $village;


    public function __construct()
    {
        $this->country = new Country();
        $this->province = new Province();
        $this->city = new City();
        $this->subdistrict = new Subdistrict();
        $this->village = new Village();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminid = 1;
        $now = date('Y-m-d H:i:s');

        $country = $this->country->create([
            'countryname' => 'Indonesia',
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $province = $this->province->create([
            'provincename' => 'Jawa Timur',
            'countryid' => $country->countryid,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $city = $this->city->create([
            'cityname' => 'Kota Batu',
            'provinceid' => $province->provinceid,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $subdistrict = $this->subdistrict->create([
            'subdistrictname' => 'Batu',
            'cityid' => $city->cityid,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->village->create([
            'villagename' => 'Ngaglik',
            'subdistrictid' => $subdistrict->subdistrictid,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);
    }
}

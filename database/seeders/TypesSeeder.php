<?php

namespace Database\Seeders;

use App\Constants\DBTypes;
use App\Models\Masters\Types;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class TypesSeeder extends Seeder
{

    /* @var Types|Relation */
    protected $types;

    public function __construct()
    {
        $this->types = new Types();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminid = 1;
        $now = date('Y-m-d H:i:s');

        $type = $this->types->create([
            'typecd' => DBTypes::role,
            'typename' => 'Role',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::roleSuperuser, 'typename' => 'SuperAdmin', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::roleCustomer, 'typename' => 'Customer', 'typeseq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::roleAdministrator, 'typename' => 'Administrator', 'typeseq' => 30, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::roleTechnician, 'typename' => 'Technician', 'typeseq' => 40, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::roleEmployee, 'typename' => 'Employee', 'typeseq' => 50, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
           'typecd' => DBTypes::menu,
           'typename' => 'Menu Type',
           'typeseq' => 10,
           'createdby' => $adminid,
           'createddate' => $now,
           'updatedby' => $adminid,
           'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::menuWeb, 'typename' => 'Web Application', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::menuApps, 'typename' => 'Apps Application', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::menuAccess,
            'typename' => 'Privileges',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::menuAccessView, 'typename' => 'View', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::menuAccessCreate, 'typename' => 'Create', 'typeseq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::menuAccessUpdate, 'typename' => 'Edit', 'typeseq' => 30, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::menuAccessDelete, 'typename' => 'Delete', 'typeseq' => 40, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::businessPartner,
            'typename' => 'Business Partner Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typename' => 'Manufaktur', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Konstruksi', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Transportasi', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Komunikasi', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Finansial', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Perdagangan', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::priority,
            'typename' => 'Priority',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::priorityLow, 'typename' => 'Low', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::priorityMedium, 'typename' => 'Medium', 'typeseq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::priorityHigh, 'typename' => 'High', 'typeseq' => 30, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::priorityCritical, 'typename' => 'Critical', 'typeseq' => 40, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::statusTicket,
            'typename' => 'Status Ticket',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusTicketWaiting, 'typename' => 'Waiting Confirmation', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusTicketOpen, 'typename' => 'Open', 'typeseq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusTicketWaitingTechnician, 'typename' => 'Waiting Technician', 'typeseq' => 30, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusTicketPending, 'typename' => 'Pending', 'typeseq' => 40, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusTicketResolved, 'typename' => 'Resolved', 'typeseq' => 40, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusTicketClose, 'typename' => 'Close', 'typeseq' => 50, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::progressTicket,
            'typename' => 'Progress Ticket',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::progressTicketCreated, 'typename' => 'Created', 'typeseq' => 10, 'descriptions' => 'Ticket has been created by {userfullname}', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::progressTicketConfirmed, 'typename' => 'Confirmed by Admin', 'typeseq' => 20, 'descriptions' => 'Ticket has been confirmed by {userfullname}', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::progressTicketWaitingTechnician, 'typename' => 'Waiting Technician', 'typeseq' => 30, 'descriptions' => 'Technician {technician} has been scheduled, waiting for technician confirmation', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::progressTicketOnProgress, 'typename' => 'On Progress', 'typeseq' => 40, 'descriptions' => 'Ticket on progress', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::progressTicketComplete, 'typename' => 'Done', 'typeseq' => 50, 'descriptions' => 'Ticket has been complete', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::files,
            'typename' => 'Data Files',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::filesTicket, 'typename' => 'File Ticket', 'typeseq' => 10, 'descriptions' => json_encode(['path' => 'ticket', 'description' => 'File untuk data detail tiket']), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::filesTicketReply, 'typename' => 'File Ticket Reply', 'typeseq' => 10, 'descriptions' => json_encode(['path' => 'ticket', 'description' => 'File untuk data detail tiket']), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::filesProfile, 'typename' => 'File Profile', 'typeseq' => 10, 'descriptions' => json_encode(['path' => 'profile', 'description' => 'File untuk data detail tiket']), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::filesTicketProgress, 'typename' => 'File Progress', 'typeseq' => 10, 'descriptions' => json_encode(['path' => 'profile', 'description' => 'File untuk data detail tiket']), 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::customerType,
            'typename' => 'Customer Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typename' => 'Otomotif', 'descriptions' => 'Customer bergerak dibidang otomotif', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Food & Beverages', 'descriptions' => 'Customer bergerak dibidang makanan dan minuman', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Jasa', 'description' => 'descriptions bergerak dibidang Jasa', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::assetCategory,
            'typename' => 'Asset Category',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typename' => 'Mesin', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Alat Elektronik', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Kendaraan', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Bangunan', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Surat Berharga', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::brand,
            'typename' => 'Brand',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $lenovo = $this->types->create([
            'typename' => 'Lenovo',
            'masterid' => $type->typeid,
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $lenovo->typeid, 'typename' => 'Lenovo Y150', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $lenovo = $this->types->create([
            'typename' => 'LG',
            'masterid' => $type->typeid,
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $lenovo->typeid, 'typename' => 'Monitor LG MK24600', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $this->types->create([
            'typecd' => DBTypes::assetInformation,
            'typename' => 'Informasi Asset',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::ticketType,
            'typename' => 'Ticket Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::ticketTypeService, 'typename' => 'Service', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::ticketTypeMaintenance, 'typename' => 'Maintenance', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::scheduleType,
            'typename' => 'Schedule Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::scheduleTypeService, 'typename' => 'Service', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::scheduleTypeMaintenance, 'typename' => 'Maintenance', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::toward,
            'typename' => 'Toward',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::towardUser, 'typename' => 'Technician', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::mappingType,
            'typename' => 'Mapping Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::mappingTypeDepartment, 'typename' => 'Mapping Type Department', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::statusSchedule,
            'typename' => 'Status Schedule',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusScheduleCreate, 'typename' => 'Created', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusScheduleOnProgress, 'typename' => 'On Progress', 'typeseq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusScheduleFinished, 'typename' => 'Finished', 'typeseq' => 30, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::statusScheduleCanceled, 'typename' => 'Canceled', 'typeseq' => 40, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::notificationType,
            'typename' => 'Notification Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typecd' => DBTypes::notificationTypeScheduleRecurrent, 'typename' => 'Remainder Schedule', 'descriptions' => 'Asset {assetname} required for scheduling maintenance', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typecd' => DBTypes::notificationTypeScheduling, 'typename' => 'Scheduling Notification', 'descriptions' => 'Asset {assetname} has been scheduling on {date}', 'typeseq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::purchase,
            'typename' => 'Purchasing Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typename' => 'Owned', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Rented', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Leased', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Subscription', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::depreciation,
            'typename' => 'Depreciation Type',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $this->types->insert([
            ['masterid' => $type->typeid, 'typename' => 'Declining Balance', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Double Declining Balance', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Straight Line', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ['masterid' => $type->typeid, 'typename' => 'Sum of The Years Digit', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
        ]);

        $type = $this->types->create([
            'typecd' => DBTypes::sparePartBrand,
            'typename' => 'Spare Part Brand',
            'typeseq' => 10,
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);
    }
}

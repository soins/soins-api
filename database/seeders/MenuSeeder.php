<?php

namespace Database\Seeders;

use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Models\Masters\Menu;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{

    /* @var Menu|Relation */
    protected $menu;

    public function __construct()
    {
        $this->menu = new Menu();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $diff = 10;
        $sequence = 0;
        $adminid = 1;
        $now = date('Y-m-d H:i:s');

        $find = TypesFinder::find()->byCode(DBTypes::menuWeb);
        if($find->valid()) {
            $menutypeid = $find->get()->getId();

            $menu = $this->menu->create([
                'menutypeid' => $menutypeid,
                'menunm' => 'Ticket',
                'route' => '/-ticket',
                'seq' => $sequence += $diff,
                'icon' => 'Icons.confirmation_num_rounded',
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $this->menu->insert([
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'All Ticket', 'route' => '/ticket', 'seq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'History Ticket', 'route' => '/tickethistory', 'seq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ]);

            $menu = $this->menu->create([
                'menutypeid' => $menutypeid,
                'menunm' => 'Task',
                'route' => '/-mytask',
                'seq' => $sequence += $diff,
                'icon' => 'Icons.task_rounded',
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $this->menu->insert([
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'All Task', 'route' => '/mytask', 'seq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'History Task', 'route' => '/historytask', 'seq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ]);

            $menu = $this->menu->create([
                'menutypeid' => $menutypeid,
                'menunm' => 'Master',
                'route' => '/-master',
                'seq' => $sequence += $diff,
                'icon' => 'Icons.dns_rounded',
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $masterSequence = 10;
            $this->menu->insert([
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Menu', 'route' => '/menu', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Location', 'route' => '/branches', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Department', 'route' => '/departement', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Location of Assets', 'route' => '/location', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Assets', 'route' => '/assets', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Assets Inquiry', 'route' => '/inquiry', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Brand', 'route' => '/datatype/brand', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Assets Type', 'route' => '/assettype', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Assets Category', 'route' => '/datatype/assct', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Assets Sub Category', 'route' => '/subcategory', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Assets Information', 'route' => '/datatype/assinf', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Spare Part', 'route' => '/sparepart', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Spare Part Type', 'route' => '/spareparttype', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Purchase Type', 'route' => '/datatype/prch', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Depreciation Type', 'route' => '/datatype/depre', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Data Type', 'route' => '/types', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Customers', 'route' => '/customer', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Dev-Customers', 'route' => '/customers', 'seq' => $masterSequence += $diff, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ]);

            $businesspartner = $this->menu->create([
                'masterid' => $menu->menuid,
                'menutypeid' => $menutypeid,
                'menunm' => 'Business Partner',
                'route' => '/-businesspartner',
                'seq' => $masterSequence += $diff,
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $this->menu->insert([
                ['masterid' => $businesspartner->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'All', 'route' => '/businesspartner', 'seq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $businesspartner->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Configure Type', 'route' => '/bptype', 'seq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $businesspartner->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Configure Menu', 'route' => '/bpmenu', 'seq' => 30, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ]);

            $security = $this->menu->create([
                'masterid' => $menu->menuid,
                'menutypeid' => $menutypeid,
                'menunm' => 'Security',
                'route' => '/-security',
                'seq' => $masterSequence += $diff,
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $this->menu->insert([
                ['masterid' => $security->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Users', 'route' => '/bpuser', 'seq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $security->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Dev-Users', 'route' => '/users', 'seq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ]);

            $addresses = $this->menu->create([
                'masterid' => $menu->menuid,
                'menutypeid' => $menutypeid,
                'menunm' => 'Addresses',
                'route' => '/-addresses',
                'seq' => $masterSequence += $diff,
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $this->menu->insert([
                ['masterid' => $addresses->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Country', 'route' => '/country', 'seq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $addresses->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Province', 'route' => '/province', 'seq' => 20, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $addresses->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'City', 'route' => '/city', 'seq' => 30, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $addresses->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Sub District', 'route' => '/subdistrict', 'seq' => 40, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $addresses->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Village', 'route' => '/village', 'seq' => 50, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ]);

            $menu = $this->menu->create([
                'menutypeid' => $menutypeid,
                'menunm' => 'Pengaturan',
                'route' => '/-settings',
                'seq' => $sequence += $diff,
                'icon' => 'Icons.settings',
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $this->menu->insert([
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Privilege', 'route' => '/previlege', 'seq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ['masterid' => $menu->menuid, 'menutypeid' => $menutypeid, 'menunm' => 'Dev-Privilege', 'route' => '/previleges', 'seq' => 10, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
            ]);
        }
    }
}

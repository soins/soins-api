<?php

namespace Database\Seeders;

use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    /* @var User|Relation */
    protected $user;

    /* @var UserDetail|Relation */
    protected $userdetail;

    /* @var BusinessPartner|Relation */
    protected $bp;

    public function __construct()
    {
        $this->user = new User();
        $this->userdetail = new UserDetail();

        $this->bp = new BusinessPartner();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminid = 1;
        $now = date('Y-m-d H:i:s');

        $user = $this->user->create([
            'username' => 'developer',
            'userpassword' => Hash::make('passwordstr12'),
            'userfullname' => 'Developer Application',
            'useremail' => 'admin@developer.application',
            'userphone' => '12345678890',
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $bps = $this->bp->get();
        $usertype = TypesFinder::find()->byCode(DBTypes::roleSuperuser, DBTypes::roleAdministrator, DBTypes::roleCustomer);

        $inserts = array();
        foreach($bps->all() as $bp) {
            $inserts[] = ['userid' => $user->userid, 'usertypeid' => $usertype->get()->getId(), 'bpid' => $bp->bpid, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];
        }

        $this->userdetail->insert($inserts);

        $user = $this->user->create([
            'username' => 'administrator',
            'userpassword' => Hash::make('admin123'),
            'userfullname' => 'Administrator',
            'useremail' => 'admin@exmaple.com',
            'userphone' => '12345678890',
            'createdby' => $adminid,
            'createddate' => $now,
            'updatedby' => $adminid,
            'updateddate' => $now,
        ]);

        $inserts = array();
        foreach($bps->all() as $bp) {
            $inserts[] = ['userid' => $user->userid, 'usertypeid' => $usertype->get(DBTypes::roleAdministrator)->getId(), 'bpid' => $bp->bpid, 'branchid' => 1, 'deptid' => 1, 'relationid' => 1, 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now];
        }

        $this->userdetail->insert($inserts);
    }
}

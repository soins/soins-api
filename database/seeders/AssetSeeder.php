<?php

namespace Database\Seeders;

use App\Constants\DBTypes;
use App\Helpers\Types\TypesFinder;
use App\Models\Masters\Assets;
use App\Models\Masters\AssetsDetail;
use App\Models\Masters\Branch;
use App\Models\Masters\BusinessPartner;
use App\Models\Masters\Departement;
use App\Models\Masters\Location;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{

    /* @var BusinessPartner|Relation */
    protected $businesspartner;

    /* @var Branch|Relation */
    protected $branch;

    /* @var Departement|Relation */
    protected $departement;

    /* @var Location|Relation */
    protected $location;

    /* @var Assets|Relation */
    protected $assets;

    /* @var AssetsDetail|Relation */
    protected $assetsdt;

    public function __construct()
    {
        $this->branch = new Branch();
        $this->departement = new Departement();
        $this->location = new Location();
        $this->assets = new Assets();
        $this->assetsdt = new AssetsDetail();
        $this->businesspartner = new BusinessPartner();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminid = 1;
        $now = date('Y-m-d H:i:s');
        $businespartners = $this->businesspartner->get();

        $assetTypes = TypesFinder::find()->byParentCode(DBTypes::assetType);
        $assetInformationWatt = TypesFinder::find()->byCode(DBTypes::assetInformationWatt);

        foreach ($businespartners as $businespartner)
        {
            $branch = $this->branch->create([
                'branchname' => 'Pusat',
                'branchaddress' => 'Jl Abdul Gani GG III',
                'branchphone' => '089636848641',
                'branchpic' => 'Pak Alfon',
                'bpid' => $businespartner->bpid,
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $departement = $this->departement->create([
                'deptname' => 'Departement IT',
                'branchid' => $branch->branchid,
                'bpid' => $businespartner->bpid,
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            $location = $this->location->create([
                'locationname' => 'Gudang IT',
                'bpid' => $businespartner->bpid,
                'branchid' => $branch->branchid,
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);

            foreach($assetTypes->child(DBTypes::assetType) as $i => $assetType)
            {
                $no = $i + 1;
                $asset = $this->assets->create([
                    'assetno' => "ASS00$no",
                    'bpid' => $businespartner->bpid,
                    'branchid' => $branch->branchid,
                    'deptid' => $departement->deptid,
                    'locationid' => $location->locationid,
                    'assettypeid' => $assetType->getId(),
                    'assetname' => "AC $no PK",
                    'createdby' => $adminid,
                    'createddate' => $now,
                    'updatedby' => $adminid,
                    'updateddate' => $now,
                ]);

                $this->assetsdt->insert([
                    ['assetid' => $asset->assetid, 'typeid' => $assetInformationWatt->get()->getId(), 'typevalue' => '150', 'createdby' => $adminid, 'createddate' => $now, 'updatedby' => $adminid, 'updateddate' => $now],
                ]);
            }
        }
    }
}

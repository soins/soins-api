<?php

namespace Database\Seeders;

use App\Constants\DBTypes;
use App\Helpers\Collections\TypeCollection;
use App\Helpers\Types\TypesFinder;
use App\Models\Masters\Customer;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{

    /* @var Customer|Relation */
    protected $customer;

    public function __construct()
    {
        $this->customer = new Customer();
    }

    private function _randomStr($length = 10, $capitalize = false) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $capitalize ? strtoupper($randomString) : $randomString;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminid = 1;
        $now = date('Y-m-d H:i:s');

        $find = TypesFinder::find()->byParentCode(DBTypes::businessPartner);
        if($find->valid()) {

            /* @var TypeCollection $bptype */
            $bptype = $find->child()->first();

            $this->customer->create([
                'customerprefix' => 'PT.',
                'customername' => 'Autochem Industry',
                'customerphone' => '911',
                'customeraddress' => 'Gedung Berca Indonesia Lantai 1 Room 403, Jl. Palmerah Utara No. 14, Palmerah, RT.1/RW.5, Palmerah, Kec. Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11480',
                'customertypeid' => $bptype->getId(),
                'referalcode' => $this->_randomStr(6, true),
                'createdby' => $adminid,
                'createddate' => $now,
                'updatedby' => $adminid,
                'updateddate' => $now,
            ]);
        }
    }
}

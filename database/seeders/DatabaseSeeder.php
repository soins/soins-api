<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TypesSeeder::class,
            MenuSeeder::class,
            CustomerSeeder::class,
            BusinessPartnerSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            AddressSeeder::class,
            AssetSeeder::class
        ]);
    }
}
